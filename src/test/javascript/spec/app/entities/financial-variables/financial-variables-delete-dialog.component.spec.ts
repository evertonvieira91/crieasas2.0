/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { CrieAsasTestModule } from '../../../test.module';
import { FinancialVariablesDeleteDialogComponent } from 'app/entities/financial-variables/financial-variables-delete-dialog.component';
import { FinancialVariablesService } from 'app/entities/financial-variables/financial-variables.service';

describe('Component Tests', () => {
    describe('FinancialVariables Management Delete Component', () => {
        let comp: FinancialVariablesDeleteDialogComponent;
        let fixture: ComponentFixture<FinancialVariablesDeleteDialogComponent>;
        let service: FinancialVariablesService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [CrieAsasTestModule],
                declarations: [FinancialVariablesDeleteDialogComponent]
            })
                .overrideTemplate(FinancialVariablesDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(FinancialVariablesDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(FinancialVariablesService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it(
                'Should call delete service on confirmDelete',
                inject(
                    [],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });
});
