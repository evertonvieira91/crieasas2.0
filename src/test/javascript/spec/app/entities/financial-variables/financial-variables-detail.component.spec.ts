/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { CrieAsasTestModule } from '../../../test.module';
import { FinancialVariablesDetailComponent } from 'app/entities/financial-variables/financial-variables-detail.component';
import { FinancialVariables } from 'app/shared/model/financial-variables.model';

describe('Component Tests', () => {
    describe('FinancialVariables Management Detail Component', () => {
        let comp: FinancialVariablesDetailComponent;
        let fixture: ComponentFixture<FinancialVariablesDetailComponent>;
        const route = ({ data: of({ financialVariables: new FinancialVariables(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [CrieAsasTestModule],
                declarations: [FinancialVariablesDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(FinancialVariablesDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(FinancialVariablesDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.financialVariables).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
