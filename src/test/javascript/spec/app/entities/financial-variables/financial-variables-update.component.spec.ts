/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { CrieAsasTestModule } from '../../../test.module';
import { FinancialVariablesUpdateComponent } from 'app/entities/financial-variables/financial-variables-update.component';
import { FinancialVariablesService } from 'app/entities/financial-variables/financial-variables.service';
import { FinancialVariables } from 'app/shared/model/financial-variables.model';

describe('Component Tests', () => {
    describe('FinancialVariables Management Update Component', () => {
        let comp: FinancialVariablesUpdateComponent;
        let fixture: ComponentFixture<FinancialVariablesUpdateComponent>;
        let service: FinancialVariablesService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [CrieAsasTestModule],
                declarations: [FinancialVariablesUpdateComponent]
            })
                .overrideTemplate(FinancialVariablesUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(FinancialVariablesUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(FinancialVariablesService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new FinancialVariables(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.financialVariables = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new FinancialVariables();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.financialVariables = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
