/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { CrieAsasTestModule } from '../../../test.module';
import { SubDomainDeleteDialogComponent } from 'app/entities/sub-domain/sub-domain-delete-dialog.component';
import { SubDomainService } from 'app/entities/sub-domain/sub-domain.service';

describe('Component Tests', () => {
    describe('SubDomain Management Delete Component', () => {
        let comp: SubDomainDeleteDialogComponent;
        let fixture: ComponentFixture<SubDomainDeleteDialogComponent>;
        let service: SubDomainService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [CrieAsasTestModule],
                declarations: [SubDomainDeleteDialogComponent]
            })
                .overrideTemplate(SubDomainDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(SubDomainDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SubDomainService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it(
                'Should call delete service on confirmDelete',
                inject(
                    [],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });
});
