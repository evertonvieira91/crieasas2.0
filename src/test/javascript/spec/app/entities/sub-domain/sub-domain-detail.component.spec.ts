/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { CrieAsasTestModule } from '../../../test.module';
import { SubDomainDetailComponent } from 'app/entities/sub-domain/sub-domain-detail.component';
import { SubDomain } from 'app/shared/model/sub-domain.model';

describe('Component Tests', () => {
    describe('SubDomain Management Detail Component', () => {
        let comp: SubDomainDetailComponent;
        let fixture: ComponentFixture<SubDomainDetailComponent>;
        const route = ({ data: of({ subDomain: new SubDomain(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [CrieAsasTestModule],
                declarations: [SubDomainDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(SubDomainDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(SubDomainDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.subDomain).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
