/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { CrieAsasTestModule } from '../../../test.module';
import { SubDomainUpdateComponent } from 'app/entities/sub-domain/sub-domain-update.component';
import { SubDomainService } from 'app/entities/sub-domain/sub-domain.service';
import { SubDomain } from 'app/shared/model/sub-domain.model';

describe('Component Tests', () => {
    describe('SubDomain Management Update Component', () => {
        let comp: SubDomainUpdateComponent;
        let fixture: ComponentFixture<SubDomainUpdateComponent>;
        let service: SubDomainService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [CrieAsasTestModule],
                declarations: [SubDomainUpdateComponent]
            })
                .overrideTemplate(SubDomainUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(SubDomainUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SubDomainService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new SubDomain(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.subDomain = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new SubDomain();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.subDomain = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
