/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { CrieAsasTestModule } from '../../../test.module';
import { SellersToSellersDetailComponent } from 'app/entities/sellers-to-sellers/sellers-to-sellers-detail.component';
import { SellersToSellers } from 'app/shared/model/sellers-to-sellers.model';

describe('Component Tests', () => {
    describe('SellersToSellers Management Detail Component', () => {
        let comp: SellersToSellersDetailComponent;
        let fixture: ComponentFixture<SellersToSellersDetailComponent>;
        const route = ({ data: of({ sellersToSellers: new SellersToSellers(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [CrieAsasTestModule],
                declarations: [SellersToSellersDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(SellersToSellersDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(SellersToSellersDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.sellersToSellers).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
