/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { CrieAsasTestModule } from '../../../test.module';
import { SellersToSellersDeleteDialogComponent } from 'app/entities/sellers-to-sellers/sellers-to-sellers-delete-dialog.component';
import { SellersToSellersService } from 'app/entities/sellers-to-sellers/sellers-to-sellers.service';

describe('Component Tests', () => {
    describe('SellersToSellers Management Delete Component', () => {
        let comp: SellersToSellersDeleteDialogComponent;
        let fixture: ComponentFixture<SellersToSellersDeleteDialogComponent>;
        let service: SellersToSellersService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [CrieAsasTestModule],
                declarations: [SellersToSellersDeleteDialogComponent]
            })
                .overrideTemplate(SellersToSellersDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(SellersToSellersDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SellersToSellersService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it(
                'Should call delete service on confirmDelete',
                inject(
                    [],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });
});
