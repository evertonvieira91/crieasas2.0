/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { CrieAsasTestModule } from '../../../test.module';
import { SellersToSellersUpdateComponent } from 'app/entities/sellers-to-sellers/sellers-to-sellers-update.component';
import { SellersToSellersService } from 'app/entities/sellers-to-sellers/sellers-to-sellers.service';
import { SellersToSellers } from 'app/shared/model/sellers-to-sellers.model';

describe('Component Tests', () => {
    describe('SellersToSellers Management Update Component', () => {
        let comp: SellersToSellersUpdateComponent;
        let fixture: ComponentFixture<SellersToSellersUpdateComponent>;
        let service: SellersToSellersService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [CrieAsasTestModule],
                declarations: [SellersToSellersUpdateComponent]
            })
                .overrideTemplate(SellersToSellersUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(SellersToSellersUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SellersToSellersService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new SellersToSellers(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.sellersToSellers = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new SellersToSellers();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.sellersToSellers = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
