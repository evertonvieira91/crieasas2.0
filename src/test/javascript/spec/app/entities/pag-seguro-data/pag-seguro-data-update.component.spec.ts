/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { CrieAsasTestModule } from '../../../test.module';
import { PagSeguroDataUpdateComponent } from 'app/entities/pag-seguro-data/pag-seguro-data-update.component';
import { PagSeguroDataService } from 'app/entities/pag-seguro-data/pag-seguro-data.service';
import { PagSeguroData } from 'app/shared/model/pag-seguro-data.model';

describe('Component Tests', () => {
    describe('PagSeguroData Management Update Component', () => {
        let comp: PagSeguroDataUpdateComponent;
        let fixture: ComponentFixture<PagSeguroDataUpdateComponent>;
        let service: PagSeguroDataService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [CrieAsasTestModule],
                declarations: [PagSeguroDataUpdateComponent]
            })
                .overrideTemplate(PagSeguroDataUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(PagSeguroDataUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PagSeguroDataService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new PagSeguroData(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.pagSeguroData = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new PagSeguroData();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.pagSeguroData = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
