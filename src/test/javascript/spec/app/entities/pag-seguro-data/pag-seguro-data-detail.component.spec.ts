/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { CrieAsasTestModule } from '../../../test.module';
import { PagSeguroDataDetailComponent } from 'app/entities/pag-seguro-data/pag-seguro-data-detail.component';
import { PagSeguroData } from 'app/shared/model/pag-seguro-data.model';

describe('Component Tests', () => {
    describe('PagSeguroData Management Detail Component', () => {
        let comp: PagSeguroDataDetailComponent;
        let fixture: ComponentFixture<PagSeguroDataDetailComponent>;
        const route = ({ data: of({ pagSeguroData: new PagSeguroData(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [CrieAsasTestModule],
                declarations: [PagSeguroDataDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(PagSeguroDataDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(PagSeguroDataDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.pagSeguroData).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
