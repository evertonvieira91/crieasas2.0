package br.com.crieasas.app.web.rest;

import br.com.crieasas.app.CrieAsasApp;

import br.com.crieasas.app.domain.PagSeguroData;
import br.com.crieasas.app.repository.PagSeguroDataRepository;
import br.com.crieasas.app.service.PagSeguroDataService;
import br.com.crieasas.app.service.dto.PagSeguroDataDTO;
import br.com.crieasas.app.service.mapper.PagSeguroDataMapper;
import br.com.crieasas.app.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static br.com.crieasas.app.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PagSeguroDataResource REST controller.
 *
 * @see PagSeguroDataResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = CrieAsasApp.class)
public class PagSeguroDataResourceIntTest {

    private static final String DEFAULT_SELLER_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_SELLER_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_SELLER_TOKEN = "AAAAAAAAAA";
    private static final String UPDATED_SELLER_TOKEN = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL_SANDBOX = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL_SANDBOX = "BBBBBBBBBB";

    private static final String DEFAULT_PASS_SANDBOX = "AAAAAAAAAA";
    private static final String UPDATED_PASS_SANDBOX = "BBBBBBBBBB";

    private static final String DEFAULT_APP_ID = "AAAAAAAAAA";
    private static final String UPDATED_APP_ID = "BBBBBBBBBB";

    private static final String DEFAULT_APP_KEY = "AAAAAAAAAA";
    private static final String UPDATED_APP_KEY = "BBBBBBBBBB";

    @Autowired
    private PagSeguroDataRepository pagSeguroDataRepository;


    @Autowired
    private PagSeguroDataMapper pagSeguroDataMapper;
    

    @Autowired
    private PagSeguroDataService pagSeguroDataService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPagSeguroDataMockMvc;

    private PagSeguroData pagSeguroData;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PagSeguroDataResource pagSeguroDataResource = new PagSeguroDataResource(pagSeguroDataService);
        this.restPagSeguroDataMockMvc = MockMvcBuilders.standaloneSetup(pagSeguroDataResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PagSeguroData createEntity(EntityManager em) {
        PagSeguroData pagSeguroData = new PagSeguroData()
            .sellerEmail(DEFAULT_SELLER_EMAIL)
            .sellerToken(DEFAULT_SELLER_TOKEN)
            .emailSandbox(DEFAULT_EMAIL_SANDBOX)
            .passSandbox(DEFAULT_PASS_SANDBOX)
            .appId(DEFAULT_APP_ID)
            .appKey(DEFAULT_APP_KEY);
        return pagSeguroData;
    }

    @Before
    public void initTest() {
        pagSeguroData = createEntity(em);
    }

    @Test
    @Transactional
    public void createPagSeguroData() throws Exception {
        int databaseSizeBeforeCreate = pagSeguroDataRepository.findAll().size();

        // Create the PagSeguroData
        PagSeguroDataDTO pagSeguroDataDTO = pagSeguroDataMapper.toDto(pagSeguroData);
        restPagSeguroDataMockMvc.perform(post("/api/pag-seguro-data")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pagSeguroDataDTO)))
            .andExpect(status().isCreated());

        // Validate the PagSeguroData in the database
        List<PagSeguroData> pagSeguroDataList = pagSeguroDataRepository.findAll();
        assertThat(pagSeguroDataList).hasSize(databaseSizeBeforeCreate + 1);
        PagSeguroData testPagSeguroData = pagSeguroDataList.get(pagSeguroDataList.size() - 1);
        assertThat(testPagSeguroData.getSellerEmail()).isEqualTo(DEFAULT_SELLER_EMAIL);
        assertThat(testPagSeguroData.getSellerToken()).isEqualTo(DEFAULT_SELLER_TOKEN);
        assertThat(testPagSeguroData.getEmailSandbox()).isEqualTo(DEFAULT_EMAIL_SANDBOX);
        assertThat(testPagSeguroData.getPassSandbox()).isEqualTo(DEFAULT_PASS_SANDBOX);
        assertThat(testPagSeguroData.getAppId()).isEqualTo(DEFAULT_APP_ID);
        assertThat(testPagSeguroData.getAppKey()).isEqualTo(DEFAULT_APP_KEY);
    }

    @Test
    @Transactional
    public void createPagSeguroDataWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = pagSeguroDataRepository.findAll().size();

        // Create the PagSeguroData with an existing ID
        pagSeguroData.setId(1L);
        PagSeguroDataDTO pagSeguroDataDTO = pagSeguroDataMapper.toDto(pagSeguroData);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPagSeguroDataMockMvc.perform(post("/api/pag-seguro-data")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pagSeguroDataDTO)))
            .andExpect(status().isBadRequest());

        // Validate the PagSeguroData in the database
        List<PagSeguroData> pagSeguroDataList = pagSeguroDataRepository.findAll();
        assertThat(pagSeguroDataList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllPagSeguroData() throws Exception {
        // Initialize the database
        pagSeguroDataRepository.saveAndFlush(pagSeguroData);

        // Get all the pagSeguroDataList
        restPagSeguroDataMockMvc.perform(get("/api/pag-seguro-data?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(pagSeguroData.getId().intValue())))
            .andExpect(jsonPath("$.[*].sellerEmail").value(hasItem(DEFAULT_SELLER_EMAIL.toString())))
            .andExpect(jsonPath("$.[*].sellerToken").value(hasItem(DEFAULT_SELLER_TOKEN.toString())))
            .andExpect(jsonPath("$.[*].emailSandbox").value(hasItem(DEFAULT_EMAIL_SANDBOX.toString())))
            .andExpect(jsonPath("$.[*].passSandbox").value(hasItem(DEFAULT_PASS_SANDBOX.toString())))
            .andExpect(jsonPath("$.[*].appId").value(hasItem(DEFAULT_APP_ID.toString())))
            .andExpect(jsonPath("$.[*].appKey").value(hasItem(DEFAULT_APP_KEY.toString())));
    }
    

    @Test
    @Transactional
    public void getPagSeguroData() throws Exception {
        // Initialize the database
        pagSeguroDataRepository.saveAndFlush(pagSeguroData);

        // Get the pagSeguroData
        restPagSeguroDataMockMvc.perform(get("/api/pag-seguro-data/{id}", pagSeguroData.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(pagSeguroData.getId().intValue()))
            .andExpect(jsonPath("$.sellerEmail").value(DEFAULT_SELLER_EMAIL.toString()))
            .andExpect(jsonPath("$.sellerToken").value(DEFAULT_SELLER_TOKEN.toString()))
            .andExpect(jsonPath("$.emailSandbox").value(DEFAULT_EMAIL_SANDBOX.toString()))
            .andExpect(jsonPath("$.passSandbox").value(DEFAULT_PASS_SANDBOX.toString()))
            .andExpect(jsonPath("$.appId").value(DEFAULT_APP_ID.toString()))
            .andExpect(jsonPath("$.appKey").value(DEFAULT_APP_KEY.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingPagSeguroData() throws Exception {
        // Get the pagSeguroData
        restPagSeguroDataMockMvc.perform(get("/api/pag-seguro-data/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePagSeguroData() throws Exception {
        // Initialize the database
        pagSeguroDataRepository.saveAndFlush(pagSeguroData);

        int databaseSizeBeforeUpdate = pagSeguroDataRepository.findAll().size();

        // Update the pagSeguroData
        PagSeguroData updatedPagSeguroData = pagSeguroDataRepository.findById(pagSeguroData.getId()).get();
        // Disconnect from session so that the updates on updatedPagSeguroData are not directly saved in db
        em.detach(updatedPagSeguroData);
        updatedPagSeguroData
            .sellerEmail(UPDATED_SELLER_EMAIL)
            .sellerToken(UPDATED_SELLER_TOKEN)
            .emailSandbox(UPDATED_EMAIL_SANDBOX)
            .passSandbox(UPDATED_PASS_SANDBOX)
            .appId(UPDATED_APP_ID)
            .appKey(UPDATED_APP_KEY);
        PagSeguroDataDTO pagSeguroDataDTO = pagSeguroDataMapper.toDto(updatedPagSeguroData);

        restPagSeguroDataMockMvc.perform(put("/api/pag-seguro-data")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pagSeguroDataDTO)))
            .andExpect(status().isOk());

        // Validate the PagSeguroData in the database
        List<PagSeguroData> pagSeguroDataList = pagSeguroDataRepository.findAll();
        assertThat(pagSeguroDataList).hasSize(databaseSizeBeforeUpdate);
        PagSeguroData testPagSeguroData = pagSeguroDataList.get(pagSeguroDataList.size() - 1);
        assertThat(testPagSeguroData.getSellerEmail()).isEqualTo(UPDATED_SELLER_EMAIL);
        assertThat(testPagSeguroData.getSellerToken()).isEqualTo(UPDATED_SELLER_TOKEN);
        assertThat(testPagSeguroData.getEmailSandbox()).isEqualTo(UPDATED_EMAIL_SANDBOX);
        assertThat(testPagSeguroData.getPassSandbox()).isEqualTo(UPDATED_PASS_SANDBOX);
        assertThat(testPagSeguroData.getAppId()).isEqualTo(UPDATED_APP_ID);
        assertThat(testPagSeguroData.getAppKey()).isEqualTo(UPDATED_APP_KEY);
    }

    @Test
    @Transactional
    public void updateNonExistingPagSeguroData() throws Exception {
        int databaseSizeBeforeUpdate = pagSeguroDataRepository.findAll().size();

        // Create the PagSeguroData
        PagSeguroDataDTO pagSeguroDataDTO = pagSeguroDataMapper.toDto(pagSeguroData);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPagSeguroDataMockMvc.perform(put("/api/pag-seguro-data")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pagSeguroDataDTO)))
            .andExpect(status().isBadRequest());

        // Validate the PagSeguroData in the database
        List<PagSeguroData> pagSeguroDataList = pagSeguroDataRepository.findAll();
        assertThat(pagSeguroDataList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePagSeguroData() throws Exception {
        // Initialize the database
        pagSeguroDataRepository.saveAndFlush(pagSeguroData);

        int databaseSizeBeforeDelete = pagSeguroDataRepository.findAll().size();

        // Get the pagSeguroData
        restPagSeguroDataMockMvc.perform(delete("/api/pag-seguro-data/{id}", pagSeguroData.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<PagSeguroData> pagSeguroDataList = pagSeguroDataRepository.findAll();
        assertThat(pagSeguroDataList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PagSeguroData.class);
        PagSeguroData pagSeguroData1 = new PagSeguroData();
        pagSeguroData1.setId(1L);
        PagSeguroData pagSeguroData2 = new PagSeguroData();
        pagSeguroData2.setId(pagSeguroData1.getId());
        assertThat(pagSeguroData1).isEqualTo(pagSeguroData2);
        pagSeguroData2.setId(2L);
        assertThat(pagSeguroData1).isNotEqualTo(pagSeguroData2);
        pagSeguroData1.setId(null);
        assertThat(pagSeguroData1).isNotEqualTo(pagSeguroData2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PagSeguroDataDTO.class);
        PagSeguroDataDTO pagSeguroDataDTO1 = new PagSeguroDataDTO();
        pagSeguroDataDTO1.setId(1L);
        PagSeguroDataDTO pagSeguroDataDTO2 = new PagSeguroDataDTO();
        assertThat(pagSeguroDataDTO1).isNotEqualTo(pagSeguroDataDTO2);
        pagSeguroDataDTO2.setId(pagSeguroDataDTO1.getId());
        assertThat(pagSeguroDataDTO1).isEqualTo(pagSeguroDataDTO2);
        pagSeguroDataDTO2.setId(2L);
        assertThat(pagSeguroDataDTO1).isNotEqualTo(pagSeguroDataDTO2);
        pagSeguroDataDTO1.setId(null);
        assertThat(pagSeguroDataDTO1).isNotEqualTo(pagSeguroDataDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(pagSeguroDataMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(pagSeguroDataMapper.fromId(null)).isNull();
    }
}
