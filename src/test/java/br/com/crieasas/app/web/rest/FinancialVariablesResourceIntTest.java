package br.com.crieasas.app.web.rest;

import br.com.crieasas.app.CrieAsasApp;

import br.com.crieasas.app.domain.FinancialVariables;
import br.com.crieasas.app.repository.FinancialVariablesRepository;
import br.com.crieasas.app.service.FinancialVariablesService;
import br.com.crieasas.app.service.dto.FinancialVariablesDTO;
import br.com.crieasas.app.service.mapper.FinancialVariablesMapper;
import br.com.crieasas.app.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;


import static br.com.crieasas.app.web.rest.TestUtil.sameInstant;
import static br.com.crieasas.app.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the FinancialVariablesResource REST controller.
 *
 * @see FinancialVariablesResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = CrieAsasApp.class)
public class FinancialVariablesResourceIntTest {

    private static final ZonedDateTime DEFAULT_CLOSING_DAY = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CLOSING_DAY = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_BILLING_DAY = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_BILLING_DAY = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Float DEFAULT_PERCENTAGE_LEVEL_1 = 1F;
    private static final Float UPDATED_PERCENTAGE_LEVEL_1 = 2F;

    private static final Float DEFAULT_PERCENTAGE_LEVEL_2 = 1F;
    private static final Float UPDATED_PERCENTAGE_LEVEL_2 = 2F;

    private static final Float DEFAULT_PERCENTAGE_LEVEL_3 = 1F;
    private static final Float UPDATED_PERCENTAGE_LEVEL_3 = 2F;

    private static final Float DEFAULT_PERCENTAGE_LEVEL_4 = 1F;
    private static final Float UPDATED_PERCENTAGE_LEVEL_4 = 2F;

    private static final Float DEFAULT_PERCENTAGE_LEVEL_5 = 1F;
    private static final Float UPDATED_PERCENTAGE_LEVEL_5 = 2F;

    private static final Float DEFAULT_VALUE_ADMIN_FEE = 1F;
    private static final Float UPDATED_VALUE_ADMIN_FEE = 2F;

    private static final Float DEFAULT_PERCENTAGE_BONUS = 1F;
    private static final Float UPDATED_PERCENTAGE_BONUS = 2F;

    @Autowired
    private FinancialVariablesRepository financialVariablesRepository;


    @Autowired
    private FinancialVariablesMapper financialVariablesMapper;
    

    @Autowired
    private FinancialVariablesService financialVariablesService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restFinancialVariablesMockMvc;

    private FinancialVariables financialVariables;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final FinancialVariablesResource financialVariablesResource = new FinancialVariablesResource(financialVariablesService);
        this.restFinancialVariablesMockMvc = MockMvcBuilders.standaloneSetup(financialVariablesResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FinancialVariables createEntity(EntityManager em) {
        FinancialVariables financialVariables = new FinancialVariables()
            .closingDay(DEFAULT_CLOSING_DAY)
            .billingDay(DEFAULT_BILLING_DAY)
            .percentageLevel1(DEFAULT_PERCENTAGE_LEVEL_1)
            .percentageLevel2(DEFAULT_PERCENTAGE_LEVEL_2)
            .percentageLevel3(DEFAULT_PERCENTAGE_LEVEL_3)
            .percentageLevel4(DEFAULT_PERCENTAGE_LEVEL_4)
            .percentageLevel5(DEFAULT_PERCENTAGE_LEVEL_5)
            .valueAdminFee(DEFAULT_VALUE_ADMIN_FEE)
            .percentageBonus(DEFAULT_PERCENTAGE_BONUS);
        return financialVariables;
    }

    @Before
    public void initTest() {
        financialVariables = createEntity(em);
    }

    @Test
    @Transactional
    public void createFinancialVariables() throws Exception {
        int databaseSizeBeforeCreate = financialVariablesRepository.findAll().size();

        // Create the FinancialVariables
        FinancialVariablesDTO financialVariablesDTO = financialVariablesMapper.toDto(financialVariables);
        restFinancialVariablesMockMvc.perform(post("/api/financial-variables")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(financialVariablesDTO)))
            .andExpect(status().isCreated());

        // Validate the FinancialVariables in the database
        List<FinancialVariables> financialVariablesList = financialVariablesRepository.findAll();
        assertThat(financialVariablesList).hasSize(databaseSizeBeforeCreate + 1);
        FinancialVariables testFinancialVariables = financialVariablesList.get(financialVariablesList.size() - 1);
        assertThat(testFinancialVariables.getClosingDay()).isEqualTo(DEFAULT_CLOSING_DAY);
        assertThat(testFinancialVariables.getBillingDay()).isEqualTo(DEFAULT_BILLING_DAY);
        assertThat(testFinancialVariables.getPercentageLevel1()).isEqualTo(DEFAULT_PERCENTAGE_LEVEL_1);
        assertThat(testFinancialVariables.getPercentageLevel2()).isEqualTo(DEFAULT_PERCENTAGE_LEVEL_2);
        assertThat(testFinancialVariables.getPercentageLevel3()).isEqualTo(DEFAULT_PERCENTAGE_LEVEL_3);
        assertThat(testFinancialVariables.getPercentageLevel4()).isEqualTo(DEFAULT_PERCENTAGE_LEVEL_4);
        assertThat(testFinancialVariables.getPercentageLevel5()).isEqualTo(DEFAULT_PERCENTAGE_LEVEL_5);
        assertThat(testFinancialVariables.getValueAdminFee()).isEqualTo(DEFAULT_VALUE_ADMIN_FEE);
        assertThat(testFinancialVariables.getPercentageBonus()).isEqualTo(DEFAULT_PERCENTAGE_BONUS);
    }

    @Test
    @Transactional
    public void createFinancialVariablesWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = financialVariablesRepository.findAll().size();

        // Create the FinancialVariables with an existing ID
        financialVariables.setId(1L);
        FinancialVariablesDTO financialVariablesDTO = financialVariablesMapper.toDto(financialVariables);

        // An entity with an existing ID cannot be created, so this API call must fail
        restFinancialVariablesMockMvc.perform(post("/api/financial-variables")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(financialVariablesDTO)))
            .andExpect(status().isBadRequest());

        // Validate the FinancialVariables in the database
        List<FinancialVariables> financialVariablesList = financialVariablesRepository.findAll();
        assertThat(financialVariablesList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllFinancialVariables() throws Exception {
        // Initialize the database
        financialVariablesRepository.saveAndFlush(financialVariables);

        // Get all the financialVariablesList
        restFinancialVariablesMockMvc.perform(get("/api/financial-variables?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(financialVariables.getId().intValue())))
            .andExpect(jsonPath("$.[*].closingDay").value(hasItem(sameInstant(DEFAULT_CLOSING_DAY))))
            .andExpect(jsonPath("$.[*].billingDay").value(hasItem(sameInstant(DEFAULT_BILLING_DAY))))
            .andExpect(jsonPath("$.[*].percentageLevel1").value(hasItem(DEFAULT_PERCENTAGE_LEVEL_1.doubleValue())))
            .andExpect(jsonPath("$.[*].percentageLevel2").value(hasItem(DEFAULT_PERCENTAGE_LEVEL_2.doubleValue())))
            .andExpect(jsonPath("$.[*].percentageLevel3").value(hasItem(DEFAULT_PERCENTAGE_LEVEL_3.doubleValue())))
            .andExpect(jsonPath("$.[*].percentageLevel4").value(hasItem(DEFAULT_PERCENTAGE_LEVEL_4.doubleValue())))
            .andExpect(jsonPath("$.[*].percentageLevel5").value(hasItem(DEFAULT_PERCENTAGE_LEVEL_5.doubleValue())))
            .andExpect(jsonPath("$.[*].valueAdminFee").value(hasItem(DEFAULT_VALUE_ADMIN_FEE.doubleValue())))
            .andExpect(jsonPath("$.[*].percentageBonus").value(hasItem(DEFAULT_PERCENTAGE_BONUS.doubleValue())));
    }
    

    @Test
    @Transactional
    public void getFinancialVariables() throws Exception {
        // Initialize the database
        financialVariablesRepository.saveAndFlush(financialVariables);

        // Get the financialVariables
        restFinancialVariablesMockMvc.perform(get("/api/financial-variables/{id}", financialVariables.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(financialVariables.getId().intValue()))
            .andExpect(jsonPath("$.closingDay").value(sameInstant(DEFAULT_CLOSING_DAY)))
            .andExpect(jsonPath("$.billingDay").value(sameInstant(DEFAULT_BILLING_DAY)))
            .andExpect(jsonPath("$.percentageLevel1").value(DEFAULT_PERCENTAGE_LEVEL_1.doubleValue()))
            .andExpect(jsonPath("$.percentageLevel2").value(DEFAULT_PERCENTAGE_LEVEL_2.doubleValue()))
            .andExpect(jsonPath("$.percentageLevel3").value(DEFAULT_PERCENTAGE_LEVEL_3.doubleValue()))
            .andExpect(jsonPath("$.percentageLevel4").value(DEFAULT_PERCENTAGE_LEVEL_4.doubleValue()))
            .andExpect(jsonPath("$.percentageLevel5").value(DEFAULT_PERCENTAGE_LEVEL_5.doubleValue()))
            .andExpect(jsonPath("$.valueAdminFee").value(DEFAULT_VALUE_ADMIN_FEE.doubleValue()))
            .andExpect(jsonPath("$.percentageBonus").value(DEFAULT_PERCENTAGE_BONUS.doubleValue()));
    }
    @Test
    @Transactional
    public void getNonExistingFinancialVariables() throws Exception {
        // Get the financialVariables
        restFinancialVariablesMockMvc.perform(get("/api/financial-variables/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFinancialVariables() throws Exception {
        // Initialize the database
        financialVariablesRepository.saveAndFlush(financialVariables);

        int databaseSizeBeforeUpdate = financialVariablesRepository.findAll().size();

        // Update the financialVariables
        FinancialVariables updatedFinancialVariables = financialVariablesRepository.findById(financialVariables.getId()).get();
        // Disconnect from session so that the updates on updatedFinancialVariables are not directly saved in db
        em.detach(updatedFinancialVariables);
        updatedFinancialVariables
            .closingDay(UPDATED_CLOSING_DAY)
            .billingDay(UPDATED_BILLING_DAY)
            .percentageLevel1(UPDATED_PERCENTAGE_LEVEL_1)
            .percentageLevel2(UPDATED_PERCENTAGE_LEVEL_2)
            .percentageLevel3(UPDATED_PERCENTAGE_LEVEL_3)
            .percentageLevel4(UPDATED_PERCENTAGE_LEVEL_4)
            .percentageLevel5(UPDATED_PERCENTAGE_LEVEL_5)
            .valueAdminFee(UPDATED_VALUE_ADMIN_FEE)
            .percentageBonus(UPDATED_PERCENTAGE_BONUS);
        FinancialVariablesDTO financialVariablesDTO = financialVariablesMapper.toDto(updatedFinancialVariables);

        restFinancialVariablesMockMvc.perform(put("/api/financial-variables")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(financialVariablesDTO)))
            .andExpect(status().isOk());

        // Validate the FinancialVariables in the database
        List<FinancialVariables> financialVariablesList = financialVariablesRepository.findAll();
        assertThat(financialVariablesList).hasSize(databaseSizeBeforeUpdate);
        FinancialVariables testFinancialVariables = financialVariablesList.get(financialVariablesList.size() - 1);
        assertThat(testFinancialVariables.getClosingDay()).isEqualTo(UPDATED_CLOSING_DAY);
        assertThat(testFinancialVariables.getBillingDay()).isEqualTo(UPDATED_BILLING_DAY);
        assertThat(testFinancialVariables.getPercentageLevel1()).isEqualTo(UPDATED_PERCENTAGE_LEVEL_1);
        assertThat(testFinancialVariables.getPercentageLevel2()).isEqualTo(UPDATED_PERCENTAGE_LEVEL_2);
        assertThat(testFinancialVariables.getPercentageLevel3()).isEqualTo(UPDATED_PERCENTAGE_LEVEL_3);
        assertThat(testFinancialVariables.getPercentageLevel4()).isEqualTo(UPDATED_PERCENTAGE_LEVEL_4);
        assertThat(testFinancialVariables.getPercentageLevel5()).isEqualTo(UPDATED_PERCENTAGE_LEVEL_5);
        assertThat(testFinancialVariables.getValueAdminFee()).isEqualTo(UPDATED_VALUE_ADMIN_FEE);
        assertThat(testFinancialVariables.getPercentageBonus()).isEqualTo(UPDATED_PERCENTAGE_BONUS);
    }

    @Test
    @Transactional
    public void updateNonExistingFinancialVariables() throws Exception {
        int databaseSizeBeforeUpdate = financialVariablesRepository.findAll().size();

        // Create the FinancialVariables
        FinancialVariablesDTO financialVariablesDTO = financialVariablesMapper.toDto(financialVariables);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restFinancialVariablesMockMvc.perform(put("/api/financial-variables")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(financialVariablesDTO)))
            .andExpect(status().isBadRequest());

        // Validate the FinancialVariables in the database
        List<FinancialVariables> financialVariablesList = financialVariablesRepository.findAll();
        assertThat(financialVariablesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteFinancialVariables() throws Exception {
        // Initialize the database
        financialVariablesRepository.saveAndFlush(financialVariables);

        int databaseSizeBeforeDelete = financialVariablesRepository.findAll().size();

        // Get the financialVariables
        restFinancialVariablesMockMvc.perform(delete("/api/financial-variables/{id}", financialVariables.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<FinancialVariables> financialVariablesList = financialVariablesRepository.findAll();
        assertThat(financialVariablesList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(FinancialVariables.class);
        FinancialVariables financialVariables1 = new FinancialVariables();
        financialVariables1.setId(1L);
        FinancialVariables financialVariables2 = new FinancialVariables();
        financialVariables2.setId(financialVariables1.getId());
        assertThat(financialVariables1).isEqualTo(financialVariables2);
        financialVariables2.setId(2L);
        assertThat(financialVariables1).isNotEqualTo(financialVariables2);
        financialVariables1.setId(null);
        assertThat(financialVariables1).isNotEqualTo(financialVariables2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(FinancialVariablesDTO.class);
        FinancialVariablesDTO financialVariablesDTO1 = new FinancialVariablesDTO();
        financialVariablesDTO1.setId(1L);
        FinancialVariablesDTO financialVariablesDTO2 = new FinancialVariablesDTO();
        assertThat(financialVariablesDTO1).isNotEqualTo(financialVariablesDTO2);
        financialVariablesDTO2.setId(financialVariablesDTO1.getId());
        assertThat(financialVariablesDTO1).isEqualTo(financialVariablesDTO2);
        financialVariablesDTO2.setId(2L);
        assertThat(financialVariablesDTO1).isNotEqualTo(financialVariablesDTO2);
        financialVariablesDTO1.setId(null);
        assertThat(financialVariablesDTO1).isNotEqualTo(financialVariablesDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(financialVariablesMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(financialVariablesMapper.fromId(null)).isNull();
    }
}
