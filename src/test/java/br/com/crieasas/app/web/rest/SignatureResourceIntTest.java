package br.com.crieasas.app.web.rest;

import br.com.crieasas.app.CrieAsasApp;

import br.com.crieasas.app.domain.Signature;
import br.com.crieasas.app.repository.SignatureRepository;
import br.com.crieasas.app.service.SignatureService;
import br.com.crieasas.app.service.dto.SignatureDTO;
import br.com.crieasas.app.service.mapper.SignatureMapper;
import br.com.crieasas.app.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;


import static br.com.crieasas.app.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import br.com.crieasas.app.domain.enumeration.StatusSignature;
/**
 * Test class for the SignatureResource REST controller.
 *
 * @see SignatureResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = CrieAsasApp.class)
public class SignatureResourceIntTest {

    private static final String DEFAULT_SIGNATURE_HASH = "AAAAAAAAAA";
    private static final String UPDATED_SIGNATURE_HASH = "BBBBBBBBBB";

    private static final Instant DEFAULT_DATE_BEGIN = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE_BEGIN = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final StatusSignature DEFAULT_STATUS_SIGNATURE = StatusSignature.ACTIVE;
    private static final StatusSignature UPDATED_STATUS_SIGNATURE = StatusSignature.CANCELED;

    @Autowired
    private SignatureRepository signatureRepository;
    @Mock
    private SignatureRepository signatureRepositoryMock;

    @Autowired
    private SignatureMapper signatureMapper;
    
    @Mock
    private SignatureService signatureServiceMock;

    @Autowired
    private SignatureService signatureService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restSignatureMockMvc;

    private Signature signature;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SignatureResource signatureResource = new SignatureResource(signatureService);
        this.restSignatureMockMvc = MockMvcBuilders.standaloneSetup(signatureResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Signature createEntity(EntityManager em) {
        Signature signature = new Signature()
            .signatureHash(DEFAULT_SIGNATURE_HASH)
            .dateBegin(DEFAULT_DATE_BEGIN)
            .statusSignature(DEFAULT_STATUS_SIGNATURE);
        return signature;
    }

    @Before
    public void initTest() {
        signature = createEntity(em);
    }

    @Test
    @Transactional
    public void createSignature() throws Exception {
        int databaseSizeBeforeCreate = signatureRepository.findAll().size();

        // Create the Signature
        SignatureDTO signatureDTO = signatureMapper.toDto(signature);
        restSignatureMockMvc.perform(post("/api/signatures")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(signatureDTO)))
            .andExpect(status().isCreated());

        // Validate the Signature in the database
        List<Signature> signatureList = signatureRepository.findAll();
        assertThat(signatureList).hasSize(databaseSizeBeforeCreate + 1);
        Signature testSignature = signatureList.get(signatureList.size() - 1);
        assertThat(testSignature.getSignatureHash()).isEqualTo(DEFAULT_SIGNATURE_HASH);
        assertThat(testSignature.getDateBegin()).isEqualTo(DEFAULT_DATE_BEGIN);
        assertThat(testSignature.getStatusSignature()).isEqualTo(DEFAULT_STATUS_SIGNATURE);
    }

    @Test
    @Transactional
    public void createSignatureWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = signatureRepository.findAll().size();

        // Create the Signature with an existing ID
        signature.setId(1L);
        SignatureDTO signatureDTO = signatureMapper.toDto(signature);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSignatureMockMvc.perform(post("/api/signatures")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(signatureDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Signature in the database
        List<Signature> signatureList = signatureRepository.findAll();
        assertThat(signatureList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllSignatures() throws Exception {
        // Initialize the database
        signatureRepository.saveAndFlush(signature);

        // Get all the signatureList
        restSignatureMockMvc.perform(get("/api/signatures?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(signature.getId().intValue())))
            .andExpect(jsonPath("$.[*].signatureHash").value(hasItem(DEFAULT_SIGNATURE_HASH.toString())))
            .andExpect(jsonPath("$.[*].dateBegin").value(hasItem(DEFAULT_DATE_BEGIN.toString())))
            .andExpect(jsonPath("$.[*].statusSignature").value(hasItem(DEFAULT_STATUS_SIGNATURE.toString())));
    }
    
    public void getAllSignaturesWithEagerRelationshipsIsEnabled() throws Exception {
        SignatureResource signatureResource = new SignatureResource(signatureServiceMock);
        when(signatureServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        MockMvc restSignatureMockMvc = MockMvcBuilders.standaloneSetup(signatureResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restSignatureMockMvc.perform(get("/api/signatures?eagerload=true"))
        .andExpect(status().isOk());

        verify(signatureServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    public void getAllSignaturesWithEagerRelationshipsIsNotEnabled() throws Exception {
        SignatureResource signatureResource = new SignatureResource(signatureServiceMock);
            when(signatureServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));
            MockMvc restSignatureMockMvc = MockMvcBuilders.standaloneSetup(signatureResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restSignatureMockMvc.perform(get("/api/signatures?eagerload=true"))
        .andExpect(status().isOk());

            verify(signatureServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    public void getSignature() throws Exception {
        // Initialize the database
        signatureRepository.saveAndFlush(signature);

        // Get the signature
        restSignatureMockMvc.perform(get("/api/signatures/{id}", signature.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(signature.getId().intValue()))
            .andExpect(jsonPath("$.signatureHash").value(DEFAULT_SIGNATURE_HASH.toString()))
            .andExpect(jsonPath("$.dateBegin").value(DEFAULT_DATE_BEGIN.toString()))
            .andExpect(jsonPath("$.statusSignature").value(DEFAULT_STATUS_SIGNATURE.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingSignature() throws Exception {
        // Get the signature
        restSignatureMockMvc.perform(get("/api/signatures/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSignature() throws Exception {
        // Initialize the database
        signatureRepository.saveAndFlush(signature);

        int databaseSizeBeforeUpdate = signatureRepository.findAll().size();

        // Update the signature
        Signature updatedSignature = signatureRepository.findById(signature.getId()).get();
        // Disconnect from session so that the updates on updatedSignature are not directly saved in db
        em.detach(updatedSignature);
        updatedSignature
            .signatureHash(UPDATED_SIGNATURE_HASH)
            .dateBegin(UPDATED_DATE_BEGIN)
            .statusSignature(UPDATED_STATUS_SIGNATURE);
        SignatureDTO signatureDTO = signatureMapper.toDto(updatedSignature);

        restSignatureMockMvc.perform(put("/api/signatures")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(signatureDTO)))
            .andExpect(status().isOk());

        // Validate the Signature in the database
        List<Signature> signatureList = signatureRepository.findAll();
        assertThat(signatureList).hasSize(databaseSizeBeforeUpdate);
        Signature testSignature = signatureList.get(signatureList.size() - 1);
        assertThat(testSignature.getSignatureHash()).isEqualTo(UPDATED_SIGNATURE_HASH);
        assertThat(testSignature.getDateBegin()).isEqualTo(UPDATED_DATE_BEGIN);
        assertThat(testSignature.getStatusSignature()).isEqualTo(UPDATED_STATUS_SIGNATURE);
    }

    @Test
    @Transactional
    public void updateNonExistingSignature() throws Exception {
        int databaseSizeBeforeUpdate = signatureRepository.findAll().size();

        // Create the Signature
        SignatureDTO signatureDTO = signatureMapper.toDto(signature);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restSignatureMockMvc.perform(put("/api/signatures")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(signatureDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Signature in the database
        List<Signature> signatureList = signatureRepository.findAll();
        assertThat(signatureList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSignature() throws Exception {
        // Initialize the database
        signatureRepository.saveAndFlush(signature);

        int databaseSizeBeforeDelete = signatureRepository.findAll().size();

        // Get the signature
        restSignatureMockMvc.perform(delete("/api/signatures/{id}", signature.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Signature> signatureList = signatureRepository.findAll();
        assertThat(signatureList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Signature.class);
        Signature signature1 = new Signature();
        signature1.setId(1L);
        Signature signature2 = new Signature();
        signature2.setId(signature1.getId());
        assertThat(signature1).isEqualTo(signature2);
        signature2.setId(2L);
        assertThat(signature1).isNotEqualTo(signature2);
        signature1.setId(null);
        assertThat(signature1).isNotEqualTo(signature2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SignatureDTO.class);
        SignatureDTO signatureDTO1 = new SignatureDTO();
        signatureDTO1.setId(1L);
        SignatureDTO signatureDTO2 = new SignatureDTO();
        assertThat(signatureDTO1).isNotEqualTo(signatureDTO2);
        signatureDTO2.setId(signatureDTO1.getId());
        assertThat(signatureDTO1).isEqualTo(signatureDTO2);
        signatureDTO2.setId(2L);
        assertThat(signatureDTO1).isNotEqualTo(signatureDTO2);
        signatureDTO1.setId(null);
        assertThat(signatureDTO1).isNotEqualTo(signatureDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(signatureMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(signatureMapper.fromId(null)).isNull();
    }
}
