package br.com.crieasas.app.web.rest;

import br.com.crieasas.app.CrieAsasApp;
import br.com.crieasas.app.service.PagSeguroService;
import br.com.crieasas.app.service.PagSeguroTransaction;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;


import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
/**
 * Test class for the PagSeguroResource REST controller.
 *
 * @see PagSeguroResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = CrieAsasApp.class)
public class PagSeguroResourceIntTest {

    private MockMvc restMockMvc;

    @Autowired
    private PagSeguroService pagSeguroService;

    @Autowired
    private PagSeguroTransaction pagSeguroTransaction;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        PagSeguroResource pagSeguroResource = new PagSeguroResource(pagSeguroService,pagSeguroTransaction);
        restMockMvc = MockMvcBuilders
            .standaloneSetup(pagSeguroResource)
            .build();
    }

    /**
    * Test pagseguro 3
    */
    @Test
    public void testPagseguro() throws Exception {
        restMockMvc.perform(get("/api/pag-seguro/createPlan"))
            .andExpect(status().isOk());
    }


    @Test
    public void testCreateSession() throws Exception{
        restMockMvc.perform(put("/api/pag-seguro/createSession"))
            .andExpect(status().isOk());

    }

    @Test
    public void testRegisterPlan() throws Exception{
        restMockMvc.perform(get("/api/pag-seguro/planRegister"))
            .andExpect(status().isOk());

    }


    @Test
    public void testChangePay() throws Exception{
        restMockMvc.perform(get("/api/pag-seguro/changePaymentMethod"))
            .andExpect(status().isOk());

    }

    @Test
    public void testEditValuePlan() throws Exception{
        restMockMvc.perform(get("/api/pag-seguro/editValuePlan"))
            .andExpect(status().isOk());

    }

    @Test
    public void requestRefundPartial() throws Exception{
        restMockMvc.perform(get("/api/pag-seguro/requestRefundPartial"))
            .andExpect(status().isOk());

    }

    @Test
    public void requestRefundTotal() throws Exception{
        restMockMvc.perform(get("/api/pag-seguro/requestRefundTotal"))
            .andExpect(status().isOk());
    }


    @Test
    public void testDiscountPayment() throws Exception {
        restMockMvc.perform(put("/api/pag-seguro/discountpayment"))
            .andExpect(status().isOk());
    }

    @Test
    public void testListDirectPaymentOrders() throws Exception {
        restMockMvc.perform(get("/api/pag-seguro/listDirectPaymentOrders"))
            .andExpect(status().isOk());
    }

    @Test
    public void preAppovalRegister() throws Exception {
        restMockMvc.perform(get("/api/pag-seguro/planRegister"))
            .andExpect(status().isOk());
    }

    @Test
    public void searchPlanByAccessionCode() throws Exception {
        restMockMvc.perform(get("/api/pag-seguro/searchPlanByAccessionCode"))
            .andExpect(status().isOk());
    }

    @Test
    public void searchPlanByDateInterval() throws Exception {
        restMockMvc.perform(get("/api/pag-seguro/searchPlanByDateInterval"))
            .andExpect(status().isOk());
    }

    @Test
    public void searchPlanByDayInterval() throws Exception {
        restMockMvc.perform(get("/api/pag-seguro/searchPlanByDayInterval"))
            .andExpect(status().isOk());
    }

    @Test
    public void searchPlanByNotificationCode() throws Exception {
        restMockMvc.perform(get("/api/pag-seguro/searchPlanByNotificationCode"))
            .andExpect(status().isOk());
   }

    @Test
    public void searchTransactionByCode() throws Exception {
        restMockMvc.perform(get("/api/pag-seguro/searchTransactionByCode"))
            .andExpect(status().isOk());
    }


}
