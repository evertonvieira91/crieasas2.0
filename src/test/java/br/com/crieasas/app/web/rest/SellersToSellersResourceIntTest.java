package br.com.crieasas.app.web.rest;

import br.com.crieasas.app.CrieAsasApp;

import br.com.crieasas.app.domain.SellersToSellers;
import br.com.crieasas.app.repository.SellersToSellersRepository;
import br.com.crieasas.app.service.SellersToSellersService;
import br.com.crieasas.app.service.dto.SellersToSellersDTO;
import br.com.crieasas.app.service.mapper.SellersToSellersMapper;
import br.com.crieasas.app.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static br.com.crieasas.app.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SellersToSellersResource REST controller.
 *
 * @see SellersToSellersResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = CrieAsasApp.class)
public class SellersToSellersResourceIntTest {

    @Autowired
    private SellersToSellersRepository sellersToSellersRepository;


    @Autowired
    private SellersToSellersMapper sellersToSellersMapper;
    

    @Autowired
    private SellersToSellersService sellersToSellersService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restSellersToSellersMockMvc;

    private SellersToSellers sellersToSellers;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SellersToSellersResource sellersToSellersResource = new SellersToSellersResource(sellersToSellersService);
        this.restSellersToSellersMockMvc = MockMvcBuilders.standaloneSetup(sellersToSellersResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SellersToSellers createEntity(EntityManager em) {
        SellersToSellers sellersToSellers = new SellersToSellers();
        return sellersToSellers;
    }

    @Before
    public void initTest() {
        sellersToSellers = createEntity(em);
    }

    @Test
    @Transactional
    public void createSellersToSellers() throws Exception {
        int databaseSizeBeforeCreate = sellersToSellersRepository.findAll().size();

        // Create the SellersToSellers
        SellersToSellersDTO sellersToSellersDTO = sellersToSellersMapper.toDto(sellersToSellers);
        restSellersToSellersMockMvc.perform(post("/api/sellers-to-sellers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sellersToSellersDTO)))
            .andExpect(status().isCreated());

        // Validate the SellersToSellers in the database
        List<SellersToSellers> sellersToSellersList = sellersToSellersRepository.findAll();
        assertThat(sellersToSellersList).hasSize(databaseSizeBeforeCreate + 1);
        SellersToSellers testSellersToSellers = sellersToSellersList.get(sellersToSellersList.size() - 1);
    }

    @Test
    @Transactional
    public void createSellersToSellersWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = sellersToSellersRepository.findAll().size();

        // Create the SellersToSellers with an existing ID
        sellersToSellers.setId(1L);
        SellersToSellersDTO sellersToSellersDTO = sellersToSellersMapper.toDto(sellersToSellers);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSellersToSellersMockMvc.perform(post("/api/sellers-to-sellers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sellersToSellersDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SellersToSellers in the database
        List<SellersToSellers> sellersToSellersList = sellersToSellersRepository.findAll();
        assertThat(sellersToSellersList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllSellersToSellers() throws Exception {
        // Initialize the database
        sellersToSellersRepository.saveAndFlush(sellersToSellers);

        // Get all the sellersToSellersList
        restSellersToSellersMockMvc.perform(get("/api/sellers-to-sellers?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(sellersToSellers.getId().intValue())));
    }
    

    @Test
    @Transactional
    public void getSellersToSellers() throws Exception {
        // Initialize the database
        sellersToSellersRepository.saveAndFlush(sellersToSellers);

        // Get the sellersToSellers
        restSellersToSellersMockMvc.perform(get("/api/sellers-to-sellers/{id}", sellersToSellers.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(sellersToSellers.getId().intValue()));
    }
    @Test
    @Transactional
    public void getNonExistingSellersToSellers() throws Exception {
        // Get the sellersToSellers
        restSellersToSellersMockMvc.perform(get("/api/sellers-to-sellers/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSellersToSellers() throws Exception {
        // Initialize the database
        sellersToSellersRepository.saveAndFlush(sellersToSellers);

        int databaseSizeBeforeUpdate = sellersToSellersRepository.findAll().size();

        // Update the sellersToSellers
        SellersToSellers updatedSellersToSellers = sellersToSellersRepository.findById(sellersToSellers.getId()).get();
        // Disconnect from session so that the updates on updatedSellersToSellers are not directly saved in db
        em.detach(updatedSellersToSellers);
        SellersToSellersDTO sellersToSellersDTO = sellersToSellersMapper.toDto(updatedSellersToSellers);

        restSellersToSellersMockMvc.perform(put("/api/sellers-to-sellers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sellersToSellersDTO)))
            .andExpect(status().isOk());

        // Validate the SellersToSellers in the database
        List<SellersToSellers> sellersToSellersList = sellersToSellersRepository.findAll();
        assertThat(sellersToSellersList).hasSize(databaseSizeBeforeUpdate);
        SellersToSellers testSellersToSellers = sellersToSellersList.get(sellersToSellersList.size() - 1);
    }

    @Test
    @Transactional
    public void updateNonExistingSellersToSellers() throws Exception {
        int databaseSizeBeforeUpdate = sellersToSellersRepository.findAll().size();

        // Create the SellersToSellers
        SellersToSellersDTO sellersToSellersDTO = sellersToSellersMapper.toDto(sellersToSellers);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restSellersToSellersMockMvc.perform(put("/api/sellers-to-sellers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sellersToSellersDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SellersToSellers in the database
        List<SellersToSellers> sellersToSellersList = sellersToSellersRepository.findAll();
        assertThat(sellersToSellersList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSellersToSellers() throws Exception {
        // Initialize the database
        sellersToSellersRepository.saveAndFlush(sellersToSellers);

        int databaseSizeBeforeDelete = sellersToSellersRepository.findAll().size();

        // Get the sellersToSellers
        restSellersToSellersMockMvc.perform(delete("/api/sellers-to-sellers/{id}", sellersToSellers.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<SellersToSellers> sellersToSellersList = sellersToSellersRepository.findAll();
        assertThat(sellersToSellersList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SellersToSellers.class);
        SellersToSellers sellersToSellers1 = new SellersToSellers();
        sellersToSellers1.setId(1L);
        SellersToSellers sellersToSellers2 = new SellersToSellers();
        sellersToSellers2.setId(sellersToSellers1.getId());
        assertThat(sellersToSellers1).isEqualTo(sellersToSellers2);
        sellersToSellers2.setId(2L);
        assertThat(sellersToSellers1).isNotEqualTo(sellersToSellers2);
        sellersToSellers1.setId(null);
        assertThat(sellersToSellers1).isNotEqualTo(sellersToSellers2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SellersToSellersDTO.class);
        SellersToSellersDTO sellersToSellersDTO1 = new SellersToSellersDTO();
        sellersToSellersDTO1.setId(1L);
        SellersToSellersDTO sellersToSellersDTO2 = new SellersToSellersDTO();
        assertThat(sellersToSellersDTO1).isNotEqualTo(sellersToSellersDTO2);
        sellersToSellersDTO2.setId(sellersToSellersDTO1.getId());
        assertThat(sellersToSellersDTO1).isEqualTo(sellersToSellersDTO2);
        sellersToSellersDTO2.setId(2L);
        assertThat(sellersToSellersDTO1).isNotEqualTo(sellersToSellersDTO2);
        sellersToSellersDTO1.setId(null);
        assertThat(sellersToSellersDTO1).isNotEqualTo(sellersToSellersDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(sellersToSellersMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(sellersToSellersMapper.fromId(null)).isNull();
    }
}
