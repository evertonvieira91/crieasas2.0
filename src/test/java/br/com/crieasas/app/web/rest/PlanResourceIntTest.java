package br.com.crieasas.app.web.rest;

import br.com.crieasas.app.CrieAsasApp;

import br.com.crieasas.app.domain.Plan;
import br.com.crieasas.app.repository.PlanRepository;
import br.com.crieasas.app.service.PlanService;
import br.com.crieasas.app.service.dto.PlanDTO;
import br.com.crieasas.app.service.mapper.PlanMapper;
import br.com.crieasas.app.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static br.com.crieasas.app.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PlanResource REST controller.
 *
 * @see PlanResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = CrieAsasApp.class)
public class PlanResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DETAILS = "AAAAAAAAAA";
    private static final String UPDATED_DETAILS = "BBBBBBBBBB";

    private static final String DEFAULT_PG_PLAN_HASH = "AAAAAAAAAA";
    private static final String UPDATED_PG_PLAN_HASH = "BBBBBBBBBB";

    private static final String DEFAULT_PG_PLAN_CODE_REFERENCE = "AAAAAAAAAA";
    private static final String UPDATED_PG_PLAN_CODE_REFERENCE = "BBBBBBBBBB";

    private static final Float DEFAULT_VALUE = 1F;
    private static final Float UPDATED_VALUE = 2F;

    private static final String DEFAULT_BILLING_PERIOD = "AAAAAAAAAA";
    private static final String UPDATED_BILLING_PERIOD = "BBBBBBBBBB";

    private static final Float DEFAULT_MEMBERSHIP_FEE = 1F;
    private static final Float UPDATED_MEMBERSHIP_FEE = 2F;

    @Autowired
    private PlanRepository planRepository;


    @Autowired
    private PlanMapper planMapper;
    

    @Autowired
    private PlanService planService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPlanMockMvc;

    private Plan plan;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PlanResource planResource = new PlanResource(planService);
        this.restPlanMockMvc = MockMvcBuilders.standaloneSetup(planResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Plan createEntity(EntityManager em) {
        Plan plan = new Plan()
            .name(DEFAULT_NAME)
            .details(DEFAULT_DETAILS)
            .pgPlanHash(DEFAULT_PG_PLAN_HASH)
            .pgPlanCodeReference(DEFAULT_PG_PLAN_CODE_REFERENCE)
            .value(DEFAULT_VALUE)
            .billingPeriod(DEFAULT_BILLING_PERIOD)
            .membershipFee(DEFAULT_MEMBERSHIP_FEE);
        return plan;
    }

    @Before
    public void initTest() {
        plan = createEntity(em);
    }

    @Test
    @Transactional
    public void createPlan() throws Exception {
        int databaseSizeBeforeCreate = planRepository.findAll().size();

        // Create the Plan
        PlanDTO planDTO = planMapper.toDto(plan);
        restPlanMockMvc.perform(post("/api/plans")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(planDTO)))
            .andExpect(status().isCreated());

        // Validate the Plan in the database
        List<Plan> planList = planRepository.findAll();
        assertThat(planList).hasSize(databaseSizeBeforeCreate + 1);
        Plan testPlan = planList.get(planList.size() - 1);
        assertThat(testPlan.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testPlan.getDetails()).isEqualTo(DEFAULT_DETAILS);
        assertThat(testPlan.getPgPlanHash()).isEqualTo(DEFAULT_PG_PLAN_HASH);
        assertThat(testPlan.getPgPlanCodeReference()).isEqualTo(DEFAULT_PG_PLAN_CODE_REFERENCE);
        assertThat(testPlan.getValue()).isEqualTo(DEFAULT_VALUE);
        assertThat(testPlan.getBillingPeriod()).isEqualTo(DEFAULT_BILLING_PERIOD);
        assertThat(testPlan.getMembershipFee()).isEqualTo(DEFAULT_MEMBERSHIP_FEE);
    }

    @Test
    @Transactional
    public void createPlanWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = planRepository.findAll().size();

        // Create the Plan with an existing ID
        plan.setId(1L);
        PlanDTO planDTO = planMapper.toDto(plan);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPlanMockMvc.perform(post("/api/plans")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(planDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Plan in the database
        List<Plan> planList = planRepository.findAll();
        assertThat(planList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllPlans() throws Exception {
        // Initialize the database
        planRepository.saveAndFlush(plan);

        // Get all the planList
        restPlanMockMvc.perform(get("/api/plans?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(plan.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].details").value(hasItem(DEFAULT_DETAILS.toString())))
            .andExpect(jsonPath("$.[*].pgPlanHash").value(hasItem(DEFAULT_PG_PLAN_HASH.toString())))
            .andExpect(jsonPath("$.[*].pgPlanCodeReference").value(hasItem(DEFAULT_PG_PLAN_CODE_REFERENCE.toString())))
            .andExpect(jsonPath("$.[*].value").value(hasItem(DEFAULT_VALUE.doubleValue())))
            .andExpect(jsonPath("$.[*].billingPeriod").value(hasItem(DEFAULT_BILLING_PERIOD.toString())))
            .andExpect(jsonPath("$.[*].membershipFee").value(hasItem(DEFAULT_MEMBERSHIP_FEE.doubleValue())));
    }
    

    @Test
    @Transactional
    public void getPlan() throws Exception {
        // Initialize the database
        planRepository.saveAndFlush(plan);

        // Get the plan
        restPlanMockMvc.perform(get("/api/plans/{id}", plan.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(plan.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.details").value(DEFAULT_DETAILS.toString()))
            .andExpect(jsonPath("$.pgPlanHash").value(DEFAULT_PG_PLAN_HASH.toString()))
            .andExpect(jsonPath("$.pgPlanCodeReference").value(DEFAULT_PG_PLAN_CODE_REFERENCE.toString()))
            .andExpect(jsonPath("$.value").value(DEFAULT_VALUE.doubleValue()))
            .andExpect(jsonPath("$.billingPeriod").value(DEFAULT_BILLING_PERIOD.toString()))
            .andExpect(jsonPath("$.membershipFee").value(DEFAULT_MEMBERSHIP_FEE.doubleValue()));
    }
    @Test
    @Transactional
    public void getNonExistingPlan() throws Exception {
        // Get the plan
        restPlanMockMvc.perform(get("/api/plans/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePlan() throws Exception {
        // Initialize the database
        planRepository.saveAndFlush(plan);

        int databaseSizeBeforeUpdate = planRepository.findAll().size();

        // Update the plan
        Plan updatedPlan = planRepository.findById(plan.getId()).get();
        // Disconnect from session so that the updates on updatedPlan are not directly saved in db
        em.detach(updatedPlan);
        updatedPlan
            .name(UPDATED_NAME)
            .details(UPDATED_DETAILS)
            .pgPlanHash(UPDATED_PG_PLAN_HASH)
            .pgPlanCodeReference(UPDATED_PG_PLAN_CODE_REFERENCE)
            .value(UPDATED_VALUE)
            .billingPeriod(UPDATED_BILLING_PERIOD)
            .membershipFee(UPDATED_MEMBERSHIP_FEE);
        PlanDTO planDTO = planMapper.toDto(updatedPlan);

        restPlanMockMvc.perform(put("/api/plans")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(planDTO)))
            .andExpect(status().isOk());

        // Validate the Plan in the database
        List<Plan> planList = planRepository.findAll();
        assertThat(planList).hasSize(databaseSizeBeforeUpdate);
        Plan testPlan = planList.get(planList.size() - 1);
        assertThat(testPlan.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testPlan.getDetails()).isEqualTo(UPDATED_DETAILS);
        assertThat(testPlan.getPgPlanHash()).isEqualTo(UPDATED_PG_PLAN_HASH);
        assertThat(testPlan.getPgPlanCodeReference()).isEqualTo(UPDATED_PG_PLAN_CODE_REFERENCE);
        assertThat(testPlan.getValue()).isEqualTo(UPDATED_VALUE);
        assertThat(testPlan.getBillingPeriod()).isEqualTo(UPDATED_BILLING_PERIOD);
        assertThat(testPlan.getMembershipFee()).isEqualTo(UPDATED_MEMBERSHIP_FEE);
    }

    @Test
    @Transactional
    public void updateNonExistingPlan() throws Exception {
        int databaseSizeBeforeUpdate = planRepository.findAll().size();

        // Create the Plan
        PlanDTO planDTO = planMapper.toDto(plan);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPlanMockMvc.perform(put("/api/plans")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(planDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Plan in the database
        List<Plan> planList = planRepository.findAll();
        assertThat(planList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePlan() throws Exception {
        // Initialize the database
        planRepository.saveAndFlush(plan);

        int databaseSizeBeforeDelete = planRepository.findAll().size();

        // Get the plan
        restPlanMockMvc.perform(delete("/api/plans/{id}", plan.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Plan> planList = planRepository.findAll();
        assertThat(planList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Plan.class);
        Plan plan1 = new Plan();
        plan1.setId(1L);
        Plan plan2 = new Plan();
        plan2.setId(plan1.getId());
        assertThat(plan1).isEqualTo(plan2);
        plan2.setId(2L);
        assertThat(plan1).isNotEqualTo(plan2);
        plan1.setId(null);
        assertThat(plan1).isNotEqualTo(plan2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PlanDTO.class);
        PlanDTO planDTO1 = new PlanDTO();
        planDTO1.setId(1L);
        PlanDTO planDTO2 = new PlanDTO();
        assertThat(planDTO1).isNotEqualTo(planDTO2);
        planDTO2.setId(planDTO1.getId());
        assertThat(planDTO1).isEqualTo(planDTO2);
        planDTO2.setId(2L);
        assertThat(planDTO1).isNotEqualTo(planDTO2);
        planDTO1.setId(null);
        assertThat(planDTO1).isNotEqualTo(planDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(planMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(planMapper.fromId(null)).isNull();
    }
}
