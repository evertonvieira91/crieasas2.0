package br.com.crieasas.app.web.rest;

import br.com.crieasas.app.CrieAsasApp;

import br.com.crieasas.app.domain.SubDomain;
import br.com.crieasas.app.repository.SubDomainRepository;
import br.com.crieasas.app.service.SubDomainService;
import br.com.crieasas.app.service.dto.SubDomainDTO;
import br.com.crieasas.app.service.mapper.SubDomainMapper;
import br.com.crieasas.app.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static br.com.crieasas.app.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SubDomainResource REST controller.
 *
 * @see SubDomainResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = CrieAsasApp.class)
public class SubDomainResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private SubDomainRepository subDomainRepository;


    @Autowired
    private SubDomainMapper subDomainMapper;
    

    @Autowired
    private SubDomainService subDomainService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restSubDomainMockMvc;

    private SubDomain subDomain;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SubDomainResource subDomainResource = new SubDomainResource(subDomainService);
        this.restSubDomainMockMvc = MockMvcBuilders.standaloneSetup(subDomainResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SubDomain createEntity(EntityManager em) {
        SubDomain subDomain = new SubDomain()
            .name(DEFAULT_NAME);
        return subDomain;
    }

    @Before
    public void initTest() {
        subDomain = createEntity(em);
    }

    @Test
    @Transactional
    public void createSubDomain() throws Exception {
        int databaseSizeBeforeCreate = subDomainRepository.findAll().size();

        // Create the SubDomain
        SubDomainDTO subDomainDTO = subDomainMapper.toDto(subDomain);
        restSubDomainMockMvc.perform(post("/api/sub-domains")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subDomainDTO)))
            .andExpect(status().isCreated());

        // Validate the SubDomain in the database
        List<SubDomain> subDomainList = subDomainRepository.findAll();
        assertThat(subDomainList).hasSize(databaseSizeBeforeCreate + 1);
        SubDomain testSubDomain = subDomainList.get(subDomainList.size() - 1);
        assertThat(testSubDomain.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createSubDomainWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = subDomainRepository.findAll().size();

        // Create the SubDomain with an existing ID
        subDomain.setId(1L);
        SubDomainDTO subDomainDTO = subDomainMapper.toDto(subDomain);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSubDomainMockMvc.perform(post("/api/sub-domains")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subDomainDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SubDomain in the database
        List<SubDomain> subDomainList = subDomainRepository.findAll();
        assertThat(subDomainList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllSubDomains() throws Exception {
        // Initialize the database
        subDomainRepository.saveAndFlush(subDomain);

        // Get all the subDomainList
        restSubDomainMockMvc.perform(get("/api/sub-domains?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(subDomain.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }
    

    @Test
    @Transactional
    public void getSubDomain() throws Exception {
        // Initialize the database
        subDomainRepository.saveAndFlush(subDomain);

        // Get the subDomain
        restSubDomainMockMvc.perform(get("/api/sub-domains/{id}", subDomain.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(subDomain.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingSubDomain() throws Exception {
        // Get the subDomain
        restSubDomainMockMvc.perform(get("/api/sub-domains/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSubDomain() throws Exception {
        // Initialize the database
        subDomainRepository.saveAndFlush(subDomain);

        int databaseSizeBeforeUpdate = subDomainRepository.findAll().size();

        // Update the subDomain
        SubDomain updatedSubDomain = subDomainRepository.findById(subDomain.getId()).get();
        // Disconnect from session so that the updates on updatedSubDomain are not directly saved in db
        em.detach(updatedSubDomain);
        updatedSubDomain
            .name(UPDATED_NAME);
        SubDomainDTO subDomainDTO = subDomainMapper.toDto(updatedSubDomain);

        restSubDomainMockMvc.perform(put("/api/sub-domains")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subDomainDTO)))
            .andExpect(status().isOk());

        // Validate the SubDomain in the database
        List<SubDomain> subDomainList = subDomainRepository.findAll();
        assertThat(subDomainList).hasSize(databaseSizeBeforeUpdate);
        SubDomain testSubDomain = subDomainList.get(subDomainList.size() - 1);
        assertThat(testSubDomain.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingSubDomain() throws Exception {
        int databaseSizeBeforeUpdate = subDomainRepository.findAll().size();

        // Create the SubDomain
        SubDomainDTO subDomainDTO = subDomainMapper.toDto(subDomain);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restSubDomainMockMvc.perform(put("/api/sub-domains")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subDomainDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SubDomain in the database
        List<SubDomain> subDomainList = subDomainRepository.findAll();
        assertThat(subDomainList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSubDomain() throws Exception {
        // Initialize the database
        subDomainRepository.saveAndFlush(subDomain);

        int databaseSizeBeforeDelete = subDomainRepository.findAll().size();

        // Get the subDomain
        restSubDomainMockMvc.perform(delete("/api/sub-domains/{id}", subDomain.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<SubDomain> subDomainList = subDomainRepository.findAll();
        assertThat(subDomainList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SubDomain.class);
        SubDomain subDomain1 = new SubDomain();
        subDomain1.setId(1L);
        SubDomain subDomain2 = new SubDomain();
        subDomain2.setId(subDomain1.getId());
        assertThat(subDomain1).isEqualTo(subDomain2);
        subDomain2.setId(2L);
        assertThat(subDomain1).isNotEqualTo(subDomain2);
        subDomain1.setId(null);
        assertThat(subDomain1).isNotEqualTo(subDomain2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SubDomainDTO.class);
        SubDomainDTO subDomainDTO1 = new SubDomainDTO();
        subDomainDTO1.setId(1L);
        SubDomainDTO subDomainDTO2 = new SubDomainDTO();
        assertThat(subDomainDTO1).isNotEqualTo(subDomainDTO2);
        subDomainDTO2.setId(subDomainDTO1.getId());
        assertThat(subDomainDTO1).isEqualTo(subDomainDTO2);
        subDomainDTO2.setId(2L);
        assertThat(subDomainDTO1).isNotEqualTo(subDomainDTO2);
        subDomainDTO1.setId(null);
        assertThat(subDomainDTO1).isNotEqualTo(subDomainDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(subDomainMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(subDomainMapper.fromId(null)).isNull();
    }
}
