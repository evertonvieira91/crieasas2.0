package br.com.crieasas.app.service;

import br.com.crieasas.app.service.dto.PagSeguroDTO;
import br.com.uol.pagseguro.api.PagSeguro;
import br.com.uol.pagseguro.api.PagSeguroEnv;
import br.com.uol.pagseguro.api.common.domain.PreApprovalRequest;
import br.com.uol.pagseguro.api.common.domain.ShippingType;
import br.com.uol.pagseguro.api.common.domain.builder.*;
import br.com.uol.pagseguro.api.common.domain.enums.*;
import br.com.uol.pagseguro.api.credential.Credential;
import br.com.uol.pagseguro.api.direct.preapproval.*;
import br.com.uol.pagseguro.api.http.JSEHttpClient;
import br.com.uol.pagseguro.api.preapproval.PreApprovalRegistrationBuilder;
import br.com.uol.pagseguro.api.preapproval.RegisteredPreApproval;
import br.com.uol.pagseguro.api.preapproval.cancel.CancelledPreApproval;
import br.com.uol.pagseguro.api.preapproval.cancel.PreApprovalCancellationBuilder;
import br.com.uol.pagseguro.api.session.CreatedSession;
import br.com.uol.pagseguro.api.utils.logging.SimpleLoggerFactory;
import org.junit.Test;

import javax.xml.bind.DatatypeConverter;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by testando on 08/08/18.
 */
public class PagSeguroServiceTest {


    private static final String SELLER_TOKEN = PagSeguroDTO.getSellerToken();
    private static final String SELLER_EMAIL = PagSeguroDTO.getSellerEmail();
    private static final String codePlan = PagSeguroDTO.getCodePlan();
    private static final String codeSignature = PagSeguroDTO.getCodeSignature();

    private static final String token = "";

    @Test
    public void criaPlanoRecorrente() throws Throwable {

        try {

            final PagSeguro pagSeguro = PagSeguro
                .instance(new SimpleLoggerFactory(), new JSEHttpClient(),
                    Credential.sellerCredential(SELLER_EMAIL, SELLER_TOKEN), PagSeguroEnv.SANDBOX);

            RegisteredDirectPreApprovalRequest registeredPreApproval = pagSeguro.directPreApprovals().register(
                new DirectPreApprovalRequestRegistrationBuilder()
                    .withRedirectURL("http://www.seusite.com.br/assinatura-concluidaa")
                    .withReference("XXXXXX")
                    .withReviewURL("http://lojamodelo.com.br/revisarr")
                    .withMaxUses(500)

                    .withPreApproval(new PreApprovalRequestBuilder()
                        .withName("Assinatura da Revista Fictícia")
                        .withCharge(Charge.AUTO)
                        .withPeriod(Period.MONTHLY)
                        .withDetails("Assinatura mensal de Revista")
                        .withAmountPerPayment(new BigDecimal(100.00))
                        .withExpiration(new ExpirationBuilder()
                            .withValue(1)
                            .withUnit(Unit.YEARS))
                    )

            );
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
            System.out.println(sdf.format(registeredPreApproval.getPreApprovalDate()));
            System.out.println(registeredPreApproval.getPreApprovalCode());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //cria uma sessão de pagamento para retornar o token necessario para o front
    //seller session (CreateDirectPreApprovalSellerSession.java)
    @Test
    public void criaSessaoPagamento() throws Throwable {

        final PagSeguro pagSeguro = PagSeguro
            .instance(new SimpleLoggerFactory(), new JSEHttpClient(), Credential.sellerCredential(SELLER_EMAIL,
                SELLER_TOKEN), PagSeguroEnv.SANDBOX);

        try {

            // Criacao de sessao de seller
            CreatedSession createdSession = pagSeguro.sessions().createDirectPreApproval();
            System.out.println(createdSession.getId());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void preAppovalRegister() throws Throwable {
        try {

            final PagSeguro pagSeguro = PagSeguro
                .instance(new SimpleLoggerFactory(), new JSEHttpClient(),
                    Credential.sellerCredential(SELLER_EMAIL, SELLER_TOKEN), PagSeguroEnv.SANDBOX);

            //Aderindo ao plano 595529D6D0D05EE444DAFFA3FAB9B381
            AccededDirectPreApproval accededDirectPreApproval = pagSeguro.directPreApprovals().accede(
                new DirectPreApprovalAccessionBuilder()
                    .withPlan(codePlan)
                    .withReference("XXXXXX")
                    .withSender(new SenderBuilder()
                        .withName("Everton")
                        .withEmail("senderemail@sandbox.pagseguro.com.br")
                        .withIp("1.1.1.1")
                        .withHash("HASHHERE")
                        .withPhone(new PhoneBuilder()
                            .withAreaCode("99")
                            .withNumber("99999999")
                        )
                        .withAddress(new AddressBuilder()
                            .withStreet("Av. PagSeguro")
                            .withNumber("9999")
                            .withComplement("99o andar")
                            .withDistrict("Jardim Internet")
                            .withCity("Cidade Exemplo")
                            .withState(State.SP)
                            .withCountry("BRA")
                            .withPostalCode("99999999")
                        )
                        .addDocument(new DocumentBuilder()
                            .withType(DocumentType.CPF)
                            .withValue("99999999999")
                        )
                    )

                    .withPaymentMethod(new PreApprovalPaymentMethodBuilder()
                        .withType(PreApprovalPaymentMethodType.CREDITCARD)
                        .withCreditCard(new PreApprovalCreditCardBuilder()
                            .withToken(token)
                            .withHolder(new PreApprovalHolderBuilder()
                                .withName("JOSÉ COMPRADOR")
                                .withBirthDate(new SimpleDateFormat("dd/MM/yyyy").parse("20/12/1990"))
                                .addDocument(new DocumentBuilder()
                                    .withType(DocumentType.CPF)
                                    .withValue("99999999999")
                                )
                                .withPhone(new PhoneBuilder()
                                    .withAreaCode("99")
                                    .withNumber("99999999")
                                )
                                .withBillingAddress(new AddressBuilder()
                                    .withStreet("Av. PagSeguro")
                                    .withNumber("9999")
                                    .withComplement("99o andar")
                                    .withDistrict("Jardim Internet")
                                    .withCity("Cidade Exemplo")
                                    .withState(State.SP)
                                    .withCountry("BRA")
                                    .withPostalCode("99999999")
                                )
                            )
                        )
                    )
            );

            System.out.println(accededDirectPreApproval.getPreApprovalCode());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void changePaymentMethodPreApproval() {
        try {
            final PagSeguro pagSeguro = PagSeguro
                .instance(new SimpleLoggerFactory(), new JSEHttpClient(),
                    Credential.sellerCredential(SELLER_EMAIL, SELLER_TOKEN), PagSeguroEnv.SANDBOX);

            //Permite a alteração do meio de pagamento atrelado ao pagamento do plano para as próximas cobranças
            pagSeguro.directPreApprovals().changePaymentMethod(
                new DirectPreApprovalChangingPaymentMethodBuilder()
                    .withPreApprovalCode(codeSignature) // código da assinatura
                    .withType(PreApprovalPaymentMethodType.CREDITCARD)
                    .withSenderRiskData(new PreApprovalSenderRiskDataBuilder()
                        .withIp("1.1.1.1")
                        .withHash("HASHCODE")
                    )
                    .withCreditCard(new PreApprovalCreditCardBuilder()
                        .withToken("683dbb354bc04d0d93f4d376f6bec636") // Codigo assinatura
                        .withHolder(new PreApprovalHolderBuilder()
                            .withName("JOSÉ Edicao")
                            .withBirthDate(new SimpleDateFormat("dd/MM/yyyy").parse("20/12/1990"))
                            .addDocument(new DocumentBuilder()
                                .withType(DocumentType.CPF)
                                .withValue("99999999999")
                            )
                            .withPhone(new PhoneBuilder()
                                .withAreaCode("99")
                                .withNumber("99999999")
                            )
                            .withBillingAddress(new AddressBuilder()
                                .withStreet("Av. PagSeguro")
                                .withNumber("9999")
                                .withComplement("99o andar")
                                .withDistrict("Jardim Internet")
                                .withCity("Cidade Exemplo")
                                .withState(State.SP)
                                .withCountry("BRA")
                                .withPostalCode("99999999")
                            )
                        )
                    )
            );
            System.out.println("Meio de pagamento alterado com sucesso!");

        } catch (Exception e) {
            e.getMessage();
        }
    }

    @Test
    public void changeStatusPreApproval() throws Throwable {

        try {
            final PagSeguro pagSeguro = PagSeguro
                .instance(new SimpleLoggerFactory(), new JSEHttpClient(),
                    Credential.sellerCredential(SELLER_EMAIL, SELLER_TOKEN), PagSeguroEnv.SANDBOX);

            // Alteração do status de adesão (assinatura) [ suspenção: "SUSPENDED" | reativação: "ACTIVE" ]
            pagSeguro.directPreApprovals().changeStatus(
                new DirectPreApprovalChangingStatusBuilder()
                    .withCode(codeSignature)
                    .withStatus(PreApprovalStatus.ACTIVE)
            );

            System.out.println("Alteracao de status de adesao realizado!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    public void editDirectPreApproval() {

        try {
            final PagSeguro pagSeguro = PagSeguro
                .instance(new SimpleLoggerFactory(), new JSEHttpClient(),
                    Credential.sellerCredential(SELLER_EMAIL, SELLER_TOKEN), PagSeguroEnv.SANDBOX);

            //Edição de Valor em Planos
            pagSeguro.directPreApprovals().edit(
                new DirectPreApprovalEditionBuilder()
                    .withCode("4E204143CDCD948774754FA07B5EE1D6")
                    .withAmountPerPayment("29.99")
                    .withUpdateSubscriptions(false)
            );

            System.out.println("Edição do valor do plano realizado!");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void discountPayment() throws Throwable {
        try{
            final PagSeguro pagSeguro = PagSeguro
                .instance(new SimpleLoggerFactory(), new JSEHttpClient(),
                    Credential.sellerCredential(SELLER_EMAIL, SELLER_TOKEN), PagSeguroEnv.SANDBOX);

            //Desconto na próxima cobrança
            pagSeguro.directPreApprovals().discount(
                new DirectPreApprovalDiscountBuilder()
                    .withCode("90CCCBEAACACA1EAA4B84FA84B46A00A") //código da assinatura
                    .withType(DiscountType.DISCOUNT_PERCENT)
                    .withValue("10.50")
            );

            System.out.println("Desconto realizado!");
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Test
    //cancela uma assinatura
    public void cancellationPreApproval() {

        try {

            final PagSeguro pagSeguro = PagSeguro
                .instance(new SimpleLoggerFactory(), new JSEHttpClient(),
                    Credential.sellerCredential(SELLER_EMAIL, SELLER_TOKEN), PagSeguroEnv.SANDBOX);

            // Cancelamento de assinaturas
            CancelledPreApproval cancelledPreApproval = pagSeguro.preApprovals().cancel(
                new PreApprovalCancellationBuilder().withCode("77E1C8EA5D5DBF3334A4AF8BA81EEDE0")
            );
            System.out.println(cancelledPreApproval.getTransactionStatus());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    //tambem cancela uma assinatura, n sei a diferença
    public void CancelDirectPreApproval() {

        try {
            final PagSeguro pagSeguro = PagSeguro
                .instance(new SimpleLoggerFactory(), new JSEHttpClient(),
                    Credential.sellerCredential(SELLER_EMAIL, SELLER_TOKEN), PagSeguroEnv.SANDBOX);

            // Cancelamento de adesão (assinatura)
            pagSeguro.directPreApprovals().cancel(
                new DirectPreApprovalCancellationBuilder()
                    .withCode("E29BDD45FDFD8A766419BFBE1E0C7D69")
            );

            System.out.println("Cancelamento de adesão realizado!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Consulta uma recorrência pelo código de adesao
    @Test
    public String searchDirectPreApprovalByAccessionCode() throws Throwable {

        try {
            final PagSeguro pagSeguro = PagSeguro.instance(
                new SimpleLoggerFactory(),
                new JSEHttpClient(),
                Credential.sellerCredential(SELLER_EMAIL, SELLER_TOKEN),
                PagSeguroEnv.SANDBOX
            );

            // Consulta uma recorrência pelo código
            SearchedDirectPreApprovalByAccessionCode searchedPreApproval = pagSeguro.directPreApprovals().searchByAccessionCode(
                new DirectPreApprovalSearchByAccessionCodeBuilder()
                    .withCode("90CCCBEAACACA1EAA4B84FA84B46A00A")
            );

            System.out.println("Consulta pelo codigo de adesão realizada!");
            return searchedPreApproval.getStatus();

        } catch (Exception e) {
            return e.getMessage();


        }
    }
}
