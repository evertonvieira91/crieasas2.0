import { Moment } from 'moment';

export interface IFinancialVariables {
    id?: number;
    closingDay?: Moment;
    billingDay?: Moment;
    percentageLevel1?: number;
    percentageLevel2?: number;
    percentageLevel3?: number;
    percentageLevel4?: number;
    percentageLevel5?: number;
    valueAdminFee?: number;
    percentageBonus?: number;
}

export class FinancialVariables implements IFinancialVariables {
    constructor(
        public id?: number,
        public closingDay?: Moment,
        public billingDay?: Moment,
        public percentageLevel1?: number,
        public percentageLevel2?: number,
        public percentageLevel3?: number,
        public percentageLevel4?: number,
        public percentageLevel5?: number,
        public valueAdminFee?: number,
        public percentageBonus?: number
    ) {}
}
