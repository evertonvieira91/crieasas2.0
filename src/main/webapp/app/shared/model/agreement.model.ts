import { Moment } from 'moment';

export interface IAgreement {
    id?: number;
    description?: string;
    urlAgreement?: string;
    createDate?: Moment;
    lastUpdate?: Moment;
}

export class Agreement implements IAgreement {
    constructor(
        public id?: number,
        public description?: string,
        public urlAgreement?: string,
        public createDate?: Moment,
        public lastUpdate?: Moment
    ) {}
}
