import { Moment } from 'moment';
import { IUser } from 'app/core/user/user.model';

export const enum StatusSignature {
    ACTIVE = 'ACTIVE',
    CANCELED = 'CANCELED',
    SUSPENDED = 'SUSPENDED',
    BLOCKED = 'BLOCKED'
}

export interface ISignature {
    id?: number;
    signatureHash?: string;
    dateBegin?: Moment;
    statusSignature?: StatusSignature;
    agreementId?: number;
    financialVariablesId?: number;
    planId?: number;
    users?: IUser[];
}

export class Signature implements ISignature {
    constructor(
        public id?: number,
        public signatureHash?: string,
        public dateBegin?: Moment,
        public statusSignature?: StatusSignature,
        public agreementId?: number,
        public financialVariablesId?: number,
        public planId?: number,
        public users?: IUser[]
    ) {}
}
