export interface IPlan {
    id?: number;
    name?: string;
    details?: string;
    pgPlanHash?: string;
    pgPlanCodeReference?: string;
    value?: number;
    billingPeriod?: string;
    membershipFee?: number;
}

export class Plan implements IPlan {
    constructor(
        public id?: number,
        public name?: string,
        public details?: string,
        public pgPlanHash?: string,
        public pgPlanCodeReference?: string,
        public value?: number,
        public billingPeriod?: string,
        public membershipFee?: number
    ) {}
}
