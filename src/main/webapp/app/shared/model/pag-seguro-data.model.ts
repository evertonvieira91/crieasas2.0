export interface IPagSeguroData {
    id?: number;
    sellerEmail?: string;
    sellerToken?: string;
    emailSandbox?: string;
    passSandbox?: string;
    appId?: string;
    appKey?: string;
}

export class PagSeguroData implements IPagSeguroData {
    constructor(
        public id?: number,
        public sellerEmail?: string,
        public sellerToken?: string,
        public emailSandbox?: string,
        public passSandbox?: string,
        public appId?: string,
        public appKey?: string
    ) {}
}
