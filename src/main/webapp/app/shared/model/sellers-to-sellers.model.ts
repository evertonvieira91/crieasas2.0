export interface ISellersToSellers {
    id?: number;
    sunId?: number;
    fatherId?: number;
}

export class SellersToSellers implements ISellersToSellers {
    constructor(public id?: number, public sunId?: number, public fatherId?: number) {}
}
