export interface IBank {
    id?: number;
    amountPro?: number;
    userLogin?: string;
    userId?: number;
}

export class Bank implements IBank {
    constructor(public id?: number, public amountPro?: number, public userLogin?: string, public userId?: number) {}
}
