export interface ISubDomain {
    id?: number;
    name?: string;
    signatureId?: number;
}

export class SubDomain implements ISubDomain {
    constructor(public id?: number, public name?: string, public signatureId?: number) {}
}
