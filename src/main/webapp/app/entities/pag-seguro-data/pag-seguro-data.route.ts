import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { PagSeguroData } from 'app/shared/model/pag-seguro-data.model';
import { PagSeguroDataService } from './pag-seguro-data.service';
import { PagSeguroDataComponent } from './pag-seguro-data.component';
import { PagSeguroDataDetailComponent } from './pag-seguro-data-detail.component';
import { PagSeguroDataUpdateComponent } from './pag-seguro-data-update.component';
import { PagSeguroDataDeletePopupComponent } from './pag-seguro-data-delete-dialog.component';
import { IPagSeguroData } from 'app/shared/model/pag-seguro-data.model';

@Injectable({ providedIn: 'root' })
export class PagSeguroDataResolve implements Resolve<IPagSeguroData> {
    constructor(private service: PagSeguroDataService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((pagSeguroData: HttpResponse<PagSeguroData>) => pagSeguroData.body));
        }
        return of(new PagSeguroData());
    }
}

export const pagSeguroDataRoute: Routes = [
    {
        path: 'pag-seguro-data',
        component: PagSeguroDataComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'crieAsasApp.pagSeguroData.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'pag-seguro-data/:id/view',
        component: PagSeguroDataDetailComponent,
        resolve: {
            pagSeguroData: PagSeguroDataResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'crieAsasApp.pagSeguroData.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'pag-seguro-data/new',
        component: PagSeguroDataUpdateComponent,
        resolve: {
            pagSeguroData: PagSeguroDataResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'crieAsasApp.pagSeguroData.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'pag-seguro-data/:id/edit',
        component: PagSeguroDataUpdateComponent,
        resolve: {
            pagSeguroData: PagSeguroDataResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'crieAsasApp.pagSeguroData.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const pagSeguroDataPopupRoute: Routes = [
    {
        path: 'pag-seguro-data/:id/delete',
        component: PagSeguroDataDeletePopupComponent,
        resolve: {
            pagSeguroData: PagSeguroDataResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'crieAsasApp.pagSeguroData.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
