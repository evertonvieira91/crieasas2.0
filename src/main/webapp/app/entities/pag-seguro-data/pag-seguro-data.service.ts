import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IPagSeguroData } from 'app/shared/model/pag-seguro-data.model';

type EntityResponseType = HttpResponse<IPagSeguroData>;
type EntityArrayResponseType = HttpResponse<IPagSeguroData[]>;

@Injectable({ providedIn: 'root' })
export class PagSeguroDataService {
    private resourceUrl = SERVER_API_URL + 'api/pag-seguro-data';

    constructor(private http: HttpClient) {}

    create(pagSeguroData: IPagSeguroData): Observable<EntityResponseType> {
        return this.http.post<IPagSeguroData>(this.resourceUrl, pagSeguroData, { observe: 'response' });
    }

    update(pagSeguroData: IPagSeguroData): Observable<EntityResponseType> {
        return this.http.put<IPagSeguroData>(this.resourceUrl, pagSeguroData, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IPagSeguroData>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IPagSeguroData[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
