import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IPagSeguroData } from 'app/shared/model/pag-seguro-data.model';
import { PagSeguroDataService } from './pag-seguro-data.service';

@Component({
    selector: 'jhi-pag-seguro-data-update',
    templateUrl: './pag-seguro-data-update.component.html'
})
export class PagSeguroDataUpdateComponent implements OnInit {
    private _pagSeguroData: IPagSeguroData;
    isSaving: boolean;

    constructor(private pagSeguroDataService: PagSeguroDataService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ pagSeguroData }) => {
            this.pagSeguroData = pagSeguroData;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.pagSeguroData.id !== undefined) {
            this.subscribeToSaveResponse(this.pagSeguroDataService.update(this.pagSeguroData));
        } else {
            this.subscribeToSaveResponse(this.pagSeguroDataService.create(this.pagSeguroData));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IPagSeguroData>>) {
        result.subscribe((res: HttpResponse<IPagSeguroData>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
    get pagSeguroData() {
        return this._pagSeguroData;
    }

    set pagSeguroData(pagSeguroData: IPagSeguroData) {
        this._pagSeguroData = pagSeguroData;
    }
}
