import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CrieAsasSharedModule } from 'app/shared';
import {
    PagSeguroDataComponent,
    PagSeguroDataDetailComponent,
    PagSeguroDataUpdateComponent,
    PagSeguroDataDeletePopupComponent,
    PagSeguroDataDeleteDialogComponent,
    pagSeguroDataRoute,
    pagSeguroDataPopupRoute
} from './';

const ENTITY_STATES = [...pagSeguroDataRoute, ...pagSeguroDataPopupRoute];

@NgModule({
    imports: [CrieAsasSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        PagSeguroDataComponent,
        PagSeguroDataDetailComponent,
        PagSeguroDataUpdateComponent,
        PagSeguroDataDeleteDialogComponent,
        PagSeguroDataDeletePopupComponent
    ],
    entryComponents: [
        PagSeguroDataComponent,
        PagSeguroDataUpdateComponent,
        PagSeguroDataDeleteDialogComponent,
        PagSeguroDataDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CrieAsasPagSeguroDataModule {}
