import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IPagSeguroData } from 'app/shared/model/pag-seguro-data.model';
import { PagSeguroDataService } from './pag-seguro-data.service';

@Component({
    selector: 'jhi-pag-seguro-data-delete-dialog',
    templateUrl: './pag-seguro-data-delete-dialog.component.html'
})
export class PagSeguroDataDeleteDialogComponent {
    pagSeguroData: IPagSeguroData;

    constructor(
        private pagSeguroDataService: PagSeguroDataService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.pagSeguroDataService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'pagSeguroDataListModification',
                content: 'Deleted an pagSeguroData'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-pag-seguro-data-delete-popup',
    template: ''
})
export class PagSeguroDataDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ pagSeguroData }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(PagSeguroDataDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.pagSeguroData = pagSeguroData;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
