import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IPagSeguroData } from 'app/shared/model/pag-seguro-data.model';

@Component({
    selector: 'jhi-pag-seguro-data-detail',
    templateUrl: './pag-seguro-data-detail.component.html'
})
export class PagSeguroDataDetailComponent implements OnInit {
    pagSeguroData: IPagSeguroData;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ pagSeguroData }) => {
            this.pagSeguroData = pagSeguroData;
        });
    }

    previousState() {
        window.history.back();
    }
}
