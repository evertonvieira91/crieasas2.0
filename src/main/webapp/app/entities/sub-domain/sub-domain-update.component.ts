import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { ISubDomain } from 'app/shared/model/sub-domain.model';
import { SubDomainService } from './sub-domain.service';
import { ISignature } from 'app/shared/model/signature.model';
import { SignatureService } from 'app/entities/signature';

@Component({
    selector: 'jhi-sub-domain-update',
    templateUrl: './sub-domain-update.component.html'
})
export class SubDomainUpdateComponent implements OnInit {
    private _subDomain: ISubDomain;
    isSaving: boolean;

    signatures: ISignature[];

    constructor(
        private jhiAlertService: JhiAlertService,
        private subDomainService: SubDomainService,
        private signatureService: SignatureService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ subDomain }) => {
            this.subDomain = subDomain;
        });
        this.signatureService.query({ filter: 'subdomain-is-null' }).subscribe(
            (res: HttpResponse<ISignature[]>) => {
                if (!this.subDomain.signatureId) {
                    this.signatures = res.body;
                } else {
                    this.signatureService.find(this.subDomain.signatureId).subscribe(
                        (subRes: HttpResponse<ISignature>) => {
                            this.signatures = [subRes.body].concat(res.body);
                        },
                        (subRes: HttpErrorResponse) => this.onError(subRes.message)
                    );
                }
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.subDomain.id !== undefined) {
            this.subscribeToSaveResponse(this.subDomainService.update(this.subDomain));
        } else {
            this.subscribeToSaveResponse(this.subDomainService.create(this.subDomain));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ISubDomain>>) {
        result.subscribe((res: HttpResponse<ISubDomain>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackSignatureById(index: number, item: ISignature) {
        return item.id;
    }
    get subDomain() {
        return this._subDomain;
    }

    set subDomain(subDomain: ISubDomain) {
        this._subDomain = subDomain;
    }
}
