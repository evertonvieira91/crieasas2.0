import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ISubDomain } from 'app/shared/model/sub-domain.model';

@Component({
    selector: 'jhi-sub-domain-detail',
    templateUrl: './sub-domain-detail.component.html'
})
export class SubDomainDetailComponent implements OnInit {
    subDomain: ISubDomain;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ subDomain }) => {
            this.subDomain = subDomain;
        });
    }

    previousState() {
        window.history.back();
    }
}
