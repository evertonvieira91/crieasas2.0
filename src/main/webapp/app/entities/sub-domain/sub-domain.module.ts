import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CrieAsasSharedModule } from 'app/shared';
import {
    SubDomainComponent,
    SubDomainDetailComponent,
    SubDomainUpdateComponent,
    SubDomainDeletePopupComponent,
    SubDomainDeleteDialogComponent,
    subDomainRoute,
    subDomainPopupRoute
} from './';

const ENTITY_STATES = [...subDomainRoute, ...subDomainPopupRoute];

@NgModule({
    imports: [CrieAsasSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        SubDomainComponent,
        SubDomainDetailComponent,
        SubDomainUpdateComponent,
        SubDomainDeleteDialogComponent,
        SubDomainDeletePopupComponent
    ],
    entryComponents: [SubDomainComponent, SubDomainUpdateComponent, SubDomainDeleteDialogComponent, SubDomainDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CrieAsasSubDomainModule {}
