import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ISubDomain } from 'app/shared/model/sub-domain.model';

type EntityResponseType = HttpResponse<ISubDomain>;
type EntityArrayResponseType = HttpResponse<ISubDomain[]>;

@Injectable({ providedIn: 'root' })
export class SubDomainService {
    private resourceUrl = SERVER_API_URL + 'api/sub-domains';

    constructor(private http: HttpClient) {}

    create(subDomain: ISubDomain): Observable<EntityResponseType> {
        return this.http.post<ISubDomain>(this.resourceUrl, subDomain, { observe: 'response' });
    }

    update(subDomain: ISubDomain): Observable<EntityResponseType> {
        return this.http.put<ISubDomain>(this.resourceUrl, subDomain, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ISubDomain>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<ISubDomain[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
