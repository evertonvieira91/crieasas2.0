import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { SubDomain } from 'app/shared/model/sub-domain.model';
import { SubDomainService } from './sub-domain.service';
import { SubDomainComponent } from './sub-domain.component';
import { SubDomainDetailComponent } from './sub-domain-detail.component';
import { SubDomainUpdateComponent } from './sub-domain-update.component';
import { SubDomainDeletePopupComponent } from './sub-domain-delete-dialog.component';
import { ISubDomain } from 'app/shared/model/sub-domain.model';

@Injectable({ providedIn: 'root' })
export class SubDomainResolve implements Resolve<ISubDomain> {
    constructor(private service: SubDomainService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((subDomain: HttpResponse<SubDomain>) => subDomain.body));
        }
        return of(new SubDomain());
    }
}

export const subDomainRoute: Routes = [
    {
        path: 'sub-domain',
        component: SubDomainComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'crieAsasApp.subDomain.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'sub-domain/:id/view',
        component: SubDomainDetailComponent,
        resolve: {
            subDomain: SubDomainResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'crieAsasApp.subDomain.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'sub-domain/new',
        component: SubDomainUpdateComponent,
        resolve: {
            subDomain: SubDomainResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'crieAsasApp.subDomain.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'sub-domain/:id/edit',
        component: SubDomainUpdateComponent,
        resolve: {
            subDomain: SubDomainResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'crieAsasApp.subDomain.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const subDomainPopupRoute: Routes = [
    {
        path: 'sub-domain/:id/delete',
        component: SubDomainDeletePopupComponent,
        resolve: {
            subDomain: SubDomainResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'crieAsasApp.subDomain.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
