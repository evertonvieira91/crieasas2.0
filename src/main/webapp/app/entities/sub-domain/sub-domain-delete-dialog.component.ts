import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ISubDomain } from 'app/shared/model/sub-domain.model';
import { SubDomainService } from './sub-domain.service';

@Component({
    selector: 'jhi-sub-domain-delete-dialog',
    templateUrl: './sub-domain-delete-dialog.component.html'
})
export class SubDomainDeleteDialogComponent {
    subDomain: ISubDomain;

    constructor(private subDomainService: SubDomainService, public activeModal: NgbActiveModal, private eventManager: JhiEventManager) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.subDomainService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'subDomainListModification',
                content: 'Deleted an subDomain'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-sub-domain-delete-popup',
    template: ''
})
export class SubDomainDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ subDomain }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(SubDomainDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
                this.ngbModalRef.componentInstance.subDomain = subDomain;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
