import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ISellersToSellers } from 'app/shared/model/sellers-to-sellers.model';

@Component({
    selector: 'jhi-sellers-to-sellers-detail',
    templateUrl: './sellers-to-sellers-detail.component.html'
})
export class SellersToSellersDetailComponent implements OnInit {
    sellersToSellers: ISellersToSellers;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ sellersToSellers }) => {
            this.sellersToSellers = sellersToSellers;
        });
    }

    previousState() {
        window.history.back();
    }
}
