import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ISellersToSellers } from 'app/shared/model/sellers-to-sellers.model';

type EntityResponseType = HttpResponse<ISellersToSellers>;
type EntityArrayResponseType = HttpResponse<ISellersToSellers[]>;

@Injectable({ providedIn: 'root' })
export class SellersToSellersService {
    private resourceUrl = SERVER_API_URL + 'api/sellers-to-sellers';

    constructor(private http: HttpClient) {}

    create(sellersToSellers: ISellersToSellers): Observable<EntityResponseType> {
        return this.http.post<ISellersToSellers>(this.resourceUrl, sellersToSellers, { observe: 'response' });
    }

    update(sellersToSellers: ISellersToSellers): Observable<EntityResponseType> {
        return this.http.put<ISellersToSellers>(this.resourceUrl, sellersToSellers, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ISellersToSellers>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<ISellersToSellers[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
