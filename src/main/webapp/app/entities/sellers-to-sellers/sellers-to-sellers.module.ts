import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CrieAsasSharedModule } from 'app/shared';
import { CrieAsasAdminModule } from 'app/admin/admin.module';
import {
    SellersToSellersComponent,
    SellersToSellersDetailComponent,
    SellersToSellersUpdateComponent,
    SellersToSellersDeletePopupComponent,
    SellersToSellersDeleteDialogComponent,
    sellersToSellersRoute,
    sellersToSellersPopupRoute
} from './';

const ENTITY_STATES = [...sellersToSellersRoute, ...sellersToSellersPopupRoute];

@NgModule({
    imports: [CrieAsasSharedModule, CrieAsasAdminModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        SellersToSellersComponent,
        SellersToSellersDetailComponent,
        SellersToSellersUpdateComponent,
        SellersToSellersDeleteDialogComponent,
        SellersToSellersDeletePopupComponent
    ],
    entryComponents: [
        SellersToSellersComponent,
        SellersToSellersUpdateComponent,
        SellersToSellersDeleteDialogComponent,
        SellersToSellersDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CrieAsasSellersToSellersModule {}
