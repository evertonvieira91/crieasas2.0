import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { ISellersToSellers } from 'app/shared/model/sellers-to-sellers.model';
import { SellersToSellersService } from './sellers-to-sellers.service';
import { IUser, UserService } from 'app/core';

@Component({
    selector: 'jhi-sellers-to-sellers-update',
    templateUrl: './sellers-to-sellers-update.component.html'
})
export class SellersToSellersUpdateComponent implements OnInit {
    private _sellersToSellers: ISellersToSellers;
    isSaving: boolean;

    users: IUser[];

    constructor(
        private jhiAlertService: JhiAlertService,
        private sellersToSellersService: SellersToSellersService,
        private userService: UserService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ sellersToSellers }) => {
            this.sellersToSellers = sellersToSellers;
        });
        this.userService.query().subscribe(
            (res: HttpResponse<IUser[]>) => {
                this.users = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.sellersToSellers.id !== undefined) {
            this.subscribeToSaveResponse(this.sellersToSellersService.update(this.sellersToSellers));
        } else {
            this.subscribeToSaveResponse(this.sellersToSellersService.create(this.sellersToSellers));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ISellersToSellers>>) {
        result.subscribe((res: HttpResponse<ISellersToSellers>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackUserById(index: number, item: IUser) {
        return item.id;
    }
    get sellersToSellers() {
        return this._sellersToSellers;
    }

    set sellersToSellers(sellersToSellers: ISellersToSellers) {
        this._sellersToSellers = sellersToSellers;
    }
}
