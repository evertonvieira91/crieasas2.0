import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ISellersToSellers } from 'app/shared/model/sellers-to-sellers.model';
import { SellersToSellersService } from './sellers-to-sellers.service';

@Component({
    selector: 'jhi-sellers-to-sellers-delete-dialog',
    templateUrl: './sellers-to-sellers-delete-dialog.component.html'
})
export class SellersToSellersDeleteDialogComponent {
    sellersToSellers: ISellersToSellers;

    constructor(
        private sellersToSellersService: SellersToSellersService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.sellersToSellersService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'sellersToSellersListModification',
                content: 'Deleted an sellersToSellers'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-sellers-to-sellers-delete-popup',
    template: ''
})
export class SellersToSellersDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ sellersToSellers }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(SellersToSellersDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.sellersToSellers = sellersToSellers;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
