import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { SellersToSellers } from 'app/shared/model/sellers-to-sellers.model';
import { SellersToSellersService } from './sellers-to-sellers.service';
import { SellersToSellersComponent } from './sellers-to-sellers.component';
import { SellersToSellersDetailComponent } from './sellers-to-sellers-detail.component';
import { SellersToSellersUpdateComponent } from './sellers-to-sellers-update.component';
import { SellersToSellersDeletePopupComponent } from './sellers-to-sellers-delete-dialog.component';
import { ISellersToSellers } from 'app/shared/model/sellers-to-sellers.model';

@Injectable({ providedIn: 'root' })
export class SellersToSellersResolve implements Resolve<ISellersToSellers> {
    constructor(private service: SellersToSellersService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((sellersToSellers: HttpResponse<SellersToSellers>) => sellersToSellers.body));
        }
        return of(new SellersToSellers());
    }
}

export const sellersToSellersRoute: Routes = [
    {
        path: 'sellers-to-sellers',
        component: SellersToSellersComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'crieAsasApp.sellersToSellers.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'sellers-to-sellers/:id/view',
        component: SellersToSellersDetailComponent,
        resolve: {
            sellersToSellers: SellersToSellersResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'crieAsasApp.sellersToSellers.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'sellers-to-sellers/new',
        component: SellersToSellersUpdateComponent,
        resolve: {
            sellersToSellers: SellersToSellersResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'crieAsasApp.sellersToSellers.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'sellers-to-sellers/:id/edit',
        component: SellersToSellersUpdateComponent,
        resolve: {
            sellersToSellers: SellersToSellersResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'crieAsasApp.sellersToSellers.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const sellersToSellersPopupRoute: Routes = [
    {
        path: 'sellers-to-sellers/:id/delete',
        component: SellersToSellersDeletePopupComponent,
        resolve: {
            sellersToSellers: SellersToSellersResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'crieAsasApp.sellersToSellers.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
