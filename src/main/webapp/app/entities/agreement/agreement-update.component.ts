import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IAgreement } from 'app/shared/model/agreement.model';
import { AgreementService } from './agreement.service';

@Component({
    selector: 'jhi-agreement-update',
    templateUrl: './agreement-update.component.html'
})
export class AgreementUpdateComponent implements OnInit {
    private _agreement: IAgreement;
    isSaving: boolean;
    createDate: string;
    lastUpdate: string;

    constructor(private agreementService: AgreementService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ agreement }) => {
            this.agreement = agreement;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        this.agreement.createDate = moment(this.createDate, DATE_TIME_FORMAT);
        this.agreement.lastUpdate = moment(this.lastUpdate, DATE_TIME_FORMAT);
        if (this.agreement.id !== undefined) {
            this.subscribeToSaveResponse(this.agreementService.update(this.agreement));
        } else {
            this.subscribeToSaveResponse(this.agreementService.create(this.agreement));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IAgreement>>) {
        result.subscribe((res: HttpResponse<IAgreement>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
    get agreement() {
        return this._agreement;
    }

    set agreement(agreement: IAgreement) {
        this._agreement = agreement;
        this.createDate = moment(agreement.createDate).format(DATE_TIME_FORMAT);
        this.lastUpdate = moment(agreement.lastUpdate).format(DATE_TIME_FORMAT);
    }
}
