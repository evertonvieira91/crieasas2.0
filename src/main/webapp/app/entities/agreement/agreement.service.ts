import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IAgreement } from 'app/shared/model/agreement.model';

type EntityResponseType = HttpResponse<IAgreement>;
type EntityArrayResponseType = HttpResponse<IAgreement[]>;

@Injectable({ providedIn: 'root' })
export class AgreementService {
    private resourceUrl = SERVER_API_URL + 'api/agreements';

    constructor(private http: HttpClient) {}

    create(agreement: IAgreement): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(agreement);
        return this.http
            .post<IAgreement>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(agreement: IAgreement): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(agreement);
        return this.http
            .put<IAgreement>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<IAgreement>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IAgreement[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    private convertDateFromClient(agreement: IAgreement): IAgreement {
        const copy: IAgreement = Object.assign({}, agreement, {
            createDate: agreement.createDate != null && agreement.createDate.isValid() ? agreement.createDate.toJSON() : null,
            lastUpdate: agreement.lastUpdate != null && agreement.lastUpdate.isValid() ? agreement.lastUpdate.toJSON() : null
        });
        return copy;
    }

    private convertDateFromServer(res: EntityResponseType): EntityResponseType {
        res.body.createDate = res.body.createDate != null ? moment(res.body.createDate) : null;
        res.body.lastUpdate = res.body.lastUpdate != null ? moment(res.body.lastUpdate) : null;
        return res;
    }

    private convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        res.body.forEach((agreement: IAgreement) => {
            agreement.createDate = agreement.createDate != null ? moment(agreement.createDate) : null;
            agreement.lastUpdate = agreement.lastUpdate != null ? moment(agreement.lastUpdate) : null;
        });
        return res;
    }
}
