import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';

import { ISignature } from 'app/shared/model/signature.model';
import { SignatureService } from './signature.service';
import { IAgreement } from 'app/shared/model/agreement.model';
import { AgreementService } from 'app/entities/agreement';
import { IFinancialVariables } from 'app/shared/model/financial-variables.model';
import { FinancialVariablesService } from 'app/entities/financial-variables';
import { IPlan } from 'app/shared/model/plan.model';
import { PlanService } from 'app/entities/plan';
import { IUser, UserService } from 'app/core';

@Component({
    selector: 'jhi-signature-update',
    templateUrl: './signature-update.component.html'
})
export class SignatureUpdateComponent implements OnInit {
    private _signature: ISignature;
    isSaving: boolean;

    agreements: IAgreement[];

    financialvariables: IFinancialVariables[];

    plans: IPlan[];

    users: IUser[];
    dateBegin: string;

    constructor(
        private jhiAlertService: JhiAlertService,
        private signatureService: SignatureService,
        private agreementService: AgreementService,
        private financialVariablesService: FinancialVariablesService,
        private planService: PlanService,
        private userService: UserService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ signature }) => {
            this.signature = signature;
        });
        this.agreementService.query({ filter: 'signature-is-null' }).subscribe(
            (res: HttpResponse<IAgreement[]>) => {
                if (!this.signature.agreementId) {
                    this.agreements = res.body;
                } else {
                    this.agreementService.find(this.signature.agreementId).subscribe(
                        (subRes: HttpResponse<IAgreement>) => {
                            this.agreements = [subRes.body].concat(res.body);
                        },
                        (subRes: HttpErrorResponse) => this.onError(subRes.message)
                    );
                }
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.financialVariablesService.query({ filter: 'signature-is-null' }).subscribe(
            (res: HttpResponse<IFinancialVariables[]>) => {
                if (!this.signature.financialVariablesId) {
                    this.financialvariables = res.body;
                } else {
                    this.financialVariablesService.find(this.signature.financialVariablesId).subscribe(
                        (subRes: HttpResponse<IFinancialVariables>) => {
                            this.financialvariables = [subRes.body].concat(res.body);
                        },
                        (subRes: HttpErrorResponse) => this.onError(subRes.message)
                    );
                }
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.planService.query().subscribe(
            (res: HttpResponse<IPlan[]>) => {
                this.plans = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.userService.query().subscribe(
            (res: HttpResponse<IUser[]>) => {
                this.users = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        this.signature.dateBegin = moment(this.dateBegin, DATE_TIME_FORMAT);
        if (this.signature.id !== undefined) {
            this.subscribeToSaveResponse(this.signatureService.update(this.signature));
        } else {
            this.subscribeToSaveResponse(this.signatureService.create(this.signature));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ISignature>>) {
        result.subscribe((res: HttpResponse<ISignature>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackAgreementById(index: number, item: IAgreement) {
        return item.id;
    }

    trackFinancialVariablesById(index: number, item: IFinancialVariables) {
        return item.id;
    }

    trackPlanById(index: number, item: IPlan) {
        return item.id;
    }

    trackUserById(index: number, item: IUser) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
    get signature() {
        return this._signature;
    }

    set signature(signature: ISignature) {
        this._signature = signature;
        this.dateBegin = moment(signature.dateBegin).format(DATE_TIME_FORMAT);
    }
}
