import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CrieAsasSharedModule } from 'app/shared';
import { CrieAsasAdminModule } from 'app/admin/admin.module';
import {
    SignatureComponent,
    SignatureDetailComponent,
    SignatureUpdateComponent,
    SignatureDeletePopupComponent,
    SignatureDeleteDialogComponent,
    signatureRoute,
    signaturePopupRoute
} from './';

const ENTITY_STATES = [...signatureRoute, ...signaturePopupRoute];

@NgModule({
    imports: [CrieAsasSharedModule, CrieAsasAdminModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        SignatureComponent,
        SignatureDetailComponent,
        SignatureUpdateComponent,
        SignatureDeleteDialogComponent,
        SignatureDeletePopupComponent
    ],
    entryComponents: [SignatureComponent, SignatureUpdateComponent, SignatureDeleteDialogComponent, SignatureDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CrieAsasSignatureModule {}
