import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ISignature } from 'app/shared/model/signature.model';

type EntityResponseType = HttpResponse<ISignature>;
type EntityArrayResponseType = HttpResponse<ISignature[]>;

@Injectable({ providedIn: 'root' })
export class SignatureService {
    private resourceUrl = SERVER_API_URL + 'api/signatures';

    constructor(private http: HttpClient) {}

    create(signature: ISignature): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(signature);
        return this.http
            .post<ISignature>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(signature: ISignature): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(signature);
        return this.http
            .put<ISignature>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<ISignature>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<ISignature[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    private convertDateFromClient(signature: ISignature): ISignature {
        const copy: ISignature = Object.assign({}, signature, {
            dateBegin: signature.dateBegin != null && signature.dateBegin.isValid() ? signature.dateBegin.toJSON() : null
        });
        return copy;
    }

    private convertDateFromServer(res: EntityResponseType): EntityResponseType {
        res.body.dateBegin = res.body.dateBegin != null ? moment(res.body.dateBegin) : null;
        return res;
    }

    private convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        res.body.forEach((signature: ISignature) => {
            signature.dateBegin = signature.dateBegin != null ? moment(signature.dateBegin) : null;
        });
        return res;
    }
}
