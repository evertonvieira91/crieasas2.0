import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IFinancialVariables } from 'app/shared/model/financial-variables.model';
import { FinancialVariablesService } from './financial-variables.service';

@Component({
    selector: 'jhi-financial-variables-update',
    templateUrl: './financial-variables-update.component.html'
})
export class FinancialVariablesUpdateComponent implements OnInit {
    private _financialVariables: IFinancialVariables;
    isSaving: boolean;
    closingDay: string;
    billingDay: string;

    constructor(private financialVariablesService: FinancialVariablesService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ financialVariables }) => {
            this.financialVariables = financialVariables;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        this.financialVariables.closingDay = moment(this.closingDay, DATE_TIME_FORMAT);
        this.financialVariables.billingDay = moment(this.billingDay, DATE_TIME_FORMAT);
        if (this.financialVariables.id !== undefined) {
            this.subscribeToSaveResponse(this.financialVariablesService.update(this.financialVariables));
        } else {
            this.subscribeToSaveResponse(this.financialVariablesService.create(this.financialVariables));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IFinancialVariables>>) {
        result.subscribe((res: HttpResponse<IFinancialVariables>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
    get financialVariables() {
        return this._financialVariables;
    }

    set financialVariables(financialVariables: IFinancialVariables) {
        this._financialVariables = financialVariables;
        this.closingDay = moment(financialVariables.closingDay).format(DATE_TIME_FORMAT);
        this.billingDay = moment(financialVariables.billingDay).format(DATE_TIME_FORMAT);
    }
}
