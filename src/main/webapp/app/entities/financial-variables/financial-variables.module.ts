import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CrieAsasSharedModule } from 'app/shared';
import {
    FinancialVariablesComponent,
    FinancialVariablesDetailComponent,
    FinancialVariablesUpdateComponent,
    FinancialVariablesDeletePopupComponent,
    FinancialVariablesDeleteDialogComponent,
    financialVariablesRoute,
    financialVariablesPopupRoute
} from './';

const ENTITY_STATES = [...financialVariablesRoute, ...financialVariablesPopupRoute];

@NgModule({
    imports: [CrieAsasSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        FinancialVariablesComponent,
        FinancialVariablesDetailComponent,
        FinancialVariablesUpdateComponent,
        FinancialVariablesDeleteDialogComponent,
        FinancialVariablesDeletePopupComponent
    ],
    entryComponents: [
        FinancialVariablesComponent,
        FinancialVariablesUpdateComponent,
        FinancialVariablesDeleteDialogComponent,
        FinancialVariablesDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CrieAsasFinancialVariablesModule {}
