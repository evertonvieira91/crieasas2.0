import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IFinancialVariables } from 'app/shared/model/financial-variables.model';

type EntityResponseType = HttpResponse<IFinancialVariables>;
type EntityArrayResponseType = HttpResponse<IFinancialVariables[]>;

@Injectable({ providedIn: 'root' })
export class FinancialVariablesService {
    private resourceUrl = SERVER_API_URL + 'api/financial-variables';

    constructor(private http: HttpClient) {}

    create(financialVariables: IFinancialVariables): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(financialVariables);
        return this.http
            .post<IFinancialVariables>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(financialVariables: IFinancialVariables): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(financialVariables);
        return this.http
            .put<IFinancialVariables>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<IFinancialVariables>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IFinancialVariables[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    private convertDateFromClient(financialVariables: IFinancialVariables): IFinancialVariables {
        const copy: IFinancialVariables = Object.assign({}, financialVariables, {
            closingDay:
                financialVariables.closingDay != null && financialVariables.closingDay.isValid()
                    ? financialVariables.closingDay.toJSON()
                    : null,
            billingDay:
                financialVariables.billingDay != null && financialVariables.billingDay.isValid()
                    ? financialVariables.billingDay.toJSON()
                    : null
        });
        return copy;
    }

    private convertDateFromServer(res: EntityResponseType): EntityResponseType {
        res.body.closingDay = res.body.closingDay != null ? moment(res.body.closingDay) : null;
        res.body.billingDay = res.body.billingDay != null ? moment(res.body.billingDay) : null;
        return res;
    }

    private convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        res.body.forEach((financialVariables: IFinancialVariables) => {
            financialVariables.closingDay = financialVariables.closingDay != null ? moment(financialVariables.closingDay) : null;
            financialVariables.billingDay = financialVariables.billingDay != null ? moment(financialVariables.billingDay) : null;
        });
        return res;
    }
}
