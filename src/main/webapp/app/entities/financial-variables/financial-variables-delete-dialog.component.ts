import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IFinancialVariables } from 'app/shared/model/financial-variables.model';
import { FinancialVariablesService } from './financial-variables.service';

@Component({
    selector: 'jhi-financial-variables-delete-dialog',
    templateUrl: './financial-variables-delete-dialog.component.html'
})
export class FinancialVariablesDeleteDialogComponent {
    financialVariables: IFinancialVariables;

    constructor(
        private financialVariablesService: FinancialVariablesService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.financialVariablesService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'financialVariablesListModification',
                content: 'Deleted an financialVariables'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-financial-variables-delete-popup',
    template: ''
})
export class FinancialVariablesDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ financialVariables }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(FinancialVariablesDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.financialVariables = financialVariables;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
