import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IFinancialVariables } from 'app/shared/model/financial-variables.model';

@Component({
    selector: 'jhi-financial-variables-detail',
    templateUrl: './financial-variables-detail.component.html'
})
export class FinancialVariablesDetailComponent implements OnInit {
    financialVariables: IFinancialVariables;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ financialVariables }) => {
            this.financialVariables = financialVariables;
        });
    }

    previousState() {
        window.history.back();
    }
}
