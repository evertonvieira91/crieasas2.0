import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { FinancialVariables } from 'app/shared/model/financial-variables.model';
import { FinancialVariablesService } from './financial-variables.service';
import { FinancialVariablesComponent } from './financial-variables.component';
import { FinancialVariablesDetailComponent } from './financial-variables-detail.component';
import { FinancialVariablesUpdateComponent } from './financial-variables-update.component';
import { FinancialVariablesDeletePopupComponent } from './financial-variables-delete-dialog.component';
import { IFinancialVariables } from 'app/shared/model/financial-variables.model';

@Injectable({ providedIn: 'root' })
export class FinancialVariablesResolve implements Resolve<IFinancialVariables> {
    constructor(private service: FinancialVariablesService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((financialVariables: HttpResponse<FinancialVariables>) => financialVariables.body));
        }
        return of(new FinancialVariables());
    }
}

export const financialVariablesRoute: Routes = [
    {
        path: 'financial-variables',
        component: FinancialVariablesComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'crieAsasApp.financialVariables.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'financial-variables/:id/view',
        component: FinancialVariablesDetailComponent,
        resolve: {
            financialVariables: FinancialVariablesResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'crieAsasApp.financialVariables.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'financial-variables/new',
        component: FinancialVariablesUpdateComponent,
        resolve: {
            financialVariables: FinancialVariablesResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'crieAsasApp.financialVariables.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'financial-variables/:id/edit',
        component: FinancialVariablesUpdateComponent,
        resolve: {
            financialVariables: FinancialVariablesResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'crieAsasApp.financialVariables.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const financialVariablesPopupRoute: Routes = [
    {
        path: 'financial-variables/:id/delete',
        component: FinancialVariablesDeletePopupComponent,
        resolve: {
            financialVariables: FinancialVariablesResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'crieAsasApp.financialVariables.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
