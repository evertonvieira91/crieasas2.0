import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { CrieAsasSellersToSellersModule } from './sellers-to-sellers/sellers-to-sellers.module';
import { CrieAsasPlanModule } from './plan/plan.module';
import { CrieAsasPagSeguroDataModule } from './pag-seguro-data/pag-seguro-data.module';
import { CrieAsasSignatureModule } from './signature/signature.module';
import { CrieAsasAgreementModule } from './agreement/agreement.module';
import { CrieAsasSubDomainModule } from './sub-domain/sub-domain.module';
import { CrieAsasFinancialVariablesModule } from './financial-variables/financial-variables.module';
import { CrieAsasBankModule } from './bank/bank.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    // prettier-ignore
    imports: [
        CrieAsasSellersToSellersModule,
        CrieAsasPlanModule,
        CrieAsasPagSeguroDataModule,
        CrieAsasSignatureModule,
        CrieAsasAgreementModule,
        CrieAsasSubDomainModule,
        CrieAsasFinancialVariablesModule,
        CrieAsasBankModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CrieAsasEntityModule {}
