package br.com.crieasas.app.service;

public interface PagSeguroTransaction {


    // Direct Payment

    public void createDirectPaymentWithCredtCard() throws Throwable;


    // Seach Transaction

    public void searchTransactionByCode() throws Throwable;

    public void searchTransactionByNotificationCode() throws Throwable;

    public void searchTransactions() throws Throwable;


    // Request Refund

    public boolean requestRefundPartial();

    public boolean requestRefundTotal();

}
