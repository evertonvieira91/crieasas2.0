package br.com.crieasas.app.service.mapper;

import br.com.crieasas.app.domain.*;
import br.com.crieasas.app.service.dto.AgreementDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Agreement and its DTO AgreementDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface AgreementMapper extends EntityMapper<AgreementDTO, Agreement> {



    default Agreement fromId(Long id) {
        if (id == null) {
            return null;
        }
        Agreement agreement = new Agreement();
        agreement.setId(id);
        return agreement;
    }
}
