package br.com.crieasas.app.service.dto;

public class PagSeguroDTO {


    private static final String EMAIL_SANDBOX = "v73959826874906233219@sandbox.pagseguro.com.br";
    private static final String SENHA_SANDBOX = "7Pt0R554y7l086g3";
    private static final String COMPRADOR_USER = "c05766986688922869240@sandbox.pagseguro.com.br";
    private static final String COMPRADOR_SENHA = "cF3680h4f562LD6c";
    private static final String appId = "app8130750727";
    private static final String appKey = "1D3C421C8E8E728CC4858FA81BE5BE6E";


    //Dani
    private static final String SELLER_EMAIL = "daniela.spaiva@yahoo.com";
    private static final String SELLER_TOKEN = "64E63302E4944E71B36F27FAC79241DB";
    private static final String codePlan = "C4A2F18CFAFA3A1444D6EFA7E646AD7F";
    private static final String codeSignature = "D3DECF3E2C2C004EE42A7FB369459829";

    //Everton
//    private static final String SELLER_EMAIL = "evertonvieira91@gmail.com";
//    private static final String SELLER_TOKEN = "C47036D7015D40FAA8538931C123A9FA";
//    private String codePlan = "";

    //Shibil
//    private static final String SELLER_EMAIL = "socramteix@gmail.com";
//    private static final String SELLER_TOKEN = "A85CD1F4DECA453F9A23DD7AAB342569";
//    private String codePlan = "";


    public static String getSellerEmail() {
        return SELLER_EMAIL;
    }

    public static String getSellerToken() {
        return SELLER_TOKEN;
    }

    public static String getEmailSandbox() {
        return EMAIL_SANDBOX;
    }

    public static String getSenhaSandbox() {
        return SENHA_SANDBOX;
    }

    public static String getCompradorUser() {
        return COMPRADOR_USER;
    }

    public static String getCompradorSenha() {
        return COMPRADOR_SENHA;
    }

    public static String getAppId() {
        return appId;
    }

    public static String getAppKey() {
        return appKey;
    }

    public static String getCodePlan() {
        return codePlan;
    }

    public static String getCodeSignature() {
        return codeSignature;
    }
}
