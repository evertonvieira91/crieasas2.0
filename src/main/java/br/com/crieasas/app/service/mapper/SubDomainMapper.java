package br.com.crieasas.app.service.mapper;

import br.com.crieasas.app.domain.*;
import br.com.crieasas.app.service.dto.SubDomainDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity SubDomain and its DTO SubDomainDTO.
 */
@Mapper(componentModel = "spring", uses = {SignatureMapper.class})
public interface SubDomainMapper extends EntityMapper<SubDomainDTO, SubDomain> {

    @Mapping(source = "signature.id", target = "signatureId")
    SubDomainDTO toDto(SubDomain subDomain);

    @Mapping(source = "signatureId", target = "signature")
    SubDomain toEntity(SubDomainDTO subDomainDTO);

    default SubDomain fromId(Long id) {
        if (id == null) {
            return null;
        }
        SubDomain subDomain = new SubDomain();
        subDomain.setId(id);
        return subDomain;
    }
}
