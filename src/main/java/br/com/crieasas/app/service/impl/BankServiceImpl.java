package br.com.crieasas.app.service.impl;

import br.com.crieasas.app.domain.*;
import br.com.crieasas.app.repository.SellersToSellersRepository;
import br.com.crieasas.app.repository.SignatureRepository;
import br.com.crieasas.app.repository.UserRepository;
import br.com.crieasas.app.service.BankService;
import br.com.crieasas.app.repository.BankRepository;
import br.com.crieasas.app.service.dto.BankDTO;
import br.com.crieasas.app.service.dto.UserDTO;
import br.com.crieasas.app.service.mapper.BankMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.Optional;
/**
 * Service Implementation for managing Bank.
 */
@Service
@Transactional
public class BankServiceImpl implements BankService {

    private final Logger log = LoggerFactory.getLogger(BankServiceImpl.class);

    private final BankMapper bankMapper;

    @Autowired
    private SellersToSellersRepository sellersToSellersRepository;

    private final BankRepository bankRepository;

    @Autowired
    private SignatureRepository signatureRepository;

    @Autowired
    private UserRepository userRepository;

    public BankServiceImpl(BankRepository bankRepository, BankMapper bankMapper) {
        this.bankRepository = bankRepository;
        this.bankMapper = bankMapper;
    }

    /**
     * Save a bank.
     *
     * @param bankDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public BankDTO save(BankDTO bankDTO) {
        log.debug("Request to save Bank : {}", bankDTO);
        Bank bank = bankMapper.toEntity(bankDTO);
        bank = bankRepository.save(bank);
        return bankMapper.toDto(bank);
    }

    /**
     * Get all the banks.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<BankDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Banks");
        return bankRepository.findAll(pageable)
            .map(bankMapper::toDto);
    }


    /**
     * Get one bank by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<BankDTO> findOne(Long id) {
        log.debug("Request to get Bank : {}", id);
        return bankRepository.findById(id)
            .map(bankMapper::toDto);
    }

    /**
     * Delete the bank by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Bank : {}", id);
        bankRepository.deleteById(id);
    }

    public void depositMembershipFee(Long sonId){
        SellersToSellers sellersToSellers = sellersToSellersRepository.findBySunId(sonId);
        User father = sellersToSellers.getFather();
        //pega a assinatura do filho que está entrando pra pagar o pai
        Signature signature = signatureRepository.findByUserId(sonId);
        Plan plan = signature.getPlan();
        FinancialVariables financialVariables = signature.getFinancialVariables();
        Bank bank = bankRepository.findByUser(father);
        //o valor 100 seria pra transformar em porcentagem
        bank.setAmountPro(bank.getAmountPro()+plan.getMembershipFee()*(100-financialVariables.getValueAdminFee())/100);
        bankRepository.save(bank);
    }

    public void depositCommission(Long sonId){

        SellersToSellers sellersToSellers = sellersToSellersRepository.findBySunId(sonId);
        Signature signature = signatureRepository.findByUserId(sonId);
        Plan plan = signature.getPlan();
        FinancialVariables financialVariables = signature.getFinancialVariables();
        Bank bank;
        User father;

        //verifica se possui pai
        if(sellersToSellers != null && sellersToSellers.getFather() != null) {
            //pega o pai de quem acabou de entrar
            father = sellersToSellers.getFather();
            //pega o banco do pai
            bank = bankRepository.findByUser(father);
            //deposita no pai
            bank.setAmountPro(
                //pega o valor que possui no saldo
                bank.getAmountPro() +
                    //pega a porcentagem do level 1
                    (financialVariables.getPercentageLevel1() *
                        //multiplica pela mensalidade dividindo por 100 pois o valor da porc. vem em %
                        plan.getValue())/100);
            bankRepository.save(bank);
            sellersToSellers = sellersToSellersRepository.findBySunId(father.getId());
            //deposita no trial do avô
            if(sellersToSellers != null && sellersToSellers.getFather() != null) {
                father = sellersToSellers.getFather();
                bank = bankRepository.findByUser(father);
                bank.setAmountPro( bank.getAmountPro() + (financialVariables.getPercentageLevel2() *
                    plan.getValue())/100);
                bankRepository.save(bank);
                sellersToSellers = sellersToSellersRepository.findBySunId(father.getId());
                //deposita no trial do bisavô
                if(sellersToSellers != null && sellersToSellers.getFather() != null) {
                    father = sellersToSellers.getFather();
                    bank = bankRepository.findByUser(father);
                    bank.setAmountPro( bank.getAmountPro() + (financialVariables.getPercentageLevel3() *
                        plan.getValue())/100);
                    bankRepository.save(bank);
                    sellersToSellers = sellersToSellersRepository.findBySunId(father.getId());
                    //deposita no trial do trisavô
                    if(sellersToSellers != null && sellersToSellers.getFather() != null) {
                        father = sellersToSellers.getFather();
                        bank = bankRepository.findByUser(father);
                        bank.setAmountPro( bank.getAmountPro() + (financialVariables.getPercentageLevel4() *
                            plan.getValue())/100);
                        bankRepository.save(bank);
                        sellersToSellers = sellersToSellersRepository.findBySunId(father.getId());
                        //deposita no trial do tetravô
                        if(sellersToSellers != null && sellersToSellers.getFather() != null) {
                            father = sellersToSellers.getFather();
                            bank = bankRepository.findByUser(father);
                            bank.setAmountPro( bank.getAmountPro() + (financialVariables.getPercentageLevel5() *
                                plan.getValue())/100);
                            bankRepository.save(bank);
                        }
                    }
                }
            }
        }
    }
}
