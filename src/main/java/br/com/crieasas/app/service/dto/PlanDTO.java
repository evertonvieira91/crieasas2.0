package br.com.crieasas.app.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Plan entity.
 */
public class PlanDTO implements Serializable {

    private Long id;

    private String name;

    private String details;

    private String pgPlanHash;

    private String pgPlanCodeReference;

    private Float value;

    private String billingPeriod;

    private Float membershipFee;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getPgPlanHash() {
        return pgPlanHash;
    }

    public void setPgPlanHash(String pgPlanHash) {
        this.pgPlanHash = pgPlanHash;
    }

    public String getPgPlanCodeReference() {
        return pgPlanCodeReference;
    }

    public void setPgPlanCodeReference(String pgPlanCodeReference) {
        this.pgPlanCodeReference = pgPlanCodeReference;
    }

    public Float getValue() {
        return value;
    }

    public void setValue(Float value) {
        this.value = value;
    }

    public String getBillingPeriod() {
        return billingPeriod;
    }

    public void setBillingPeriod(String billingPeriod) {
        this.billingPeriod = billingPeriod;
    }

    public Float getMembershipFee() {
        return membershipFee;
    }

    public void setMembershipFee(Float membershipFee) {
        this.membershipFee = membershipFee;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PlanDTO planDTO = (PlanDTO) o;
        if (planDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), planDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PlanDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", details='" + getDetails() + "'" +
            ", pgPlanHash='" + getPgPlanHash() + "'" +
            ", pgPlanCodeReference='" + getPgPlanCodeReference() + "'" +
            ", value=" + getValue() +
            ", billingPeriod='" + getBillingPeriod() + "'" +
            ", membershipFee=" + getMembershipFee() +
            "}";
    }
}
