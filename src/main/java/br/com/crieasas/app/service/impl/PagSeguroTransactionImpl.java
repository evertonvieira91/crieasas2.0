package br.com.crieasas.app.service.impl;

import br.com.crieasas.app.service.PagSeguroTransaction;
import br.com.crieasas.app.service.dto.PagSeguroDTO;
import br.com.uol.pagseguro.api.PagSeguro;
import br.com.uol.pagseguro.api.PagSeguroEnv;
import br.com.uol.pagseguro.api.credential.Credential;
import br.com.uol.pagseguro.api.transaction.search.TransactionDetail;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;

@Service
@Transactional
public class PagSeguroTransactionImpl implements PagSeguroTransaction {

    private String sellerEmail = PagSeguroDTO.getSellerEmail();
    private String sellerToken = PagSeguroDTO.getSellerToken();

    //codigo da assinatura
    private String codeSignature = "C4A2F18CFAFA3A1444D6EFA7E646AD7F";

    @Override
    public void createDirectPaymentWithCredtCard() throws Throwable {

    }

    // Estorno Parcial
    @Override
    public boolean requestRefundPartial() {
        try {

            final PagSeguro pagSeguro = PagSeguro.instance(Credential.sellerCredential(sellerEmail,
                sellerToken), PagSeguroEnv.SANDBOX);

            //codigo da assinatura
            pagSeguro.transactions().refundByCode(codeSignature, new BigDecimal(10.90));

            return true;

        } catch (Exception e) {
            return false;
        }
    }

    // Estorno Total
    @Override
    public boolean requestRefundTotal() {
        try {

            final PagSeguro pagSeguro = PagSeguro.instance(Credential.sellerCredential(sellerEmail,
                sellerToken), PagSeguroEnv.SANDBOX);

            //codigo da assinatura
            pagSeguro.transactions().refundByCode(codeSignature);

            return true;

        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public void searchTransactionByCode() throws Throwable {
        try {
            final PagSeguro pagSeguro = PagSeguro.instance(Credential.sellerCredential(sellerEmail,
                sellerToken), PagSeguroEnv.SANDBOX);

            //TransactionDetail transaction = pagSeguro.transactions().search().byCode("C418ED");
            TransactionDetail transaction = pagSeguro.transactions().search().byNotificationCode("BCAF2C2AEEC6483288050822DB51F998");
            System.out.println(transaction);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void searchTransactionByNotificationCode() throws Throwable {

    }

    @Override
    public void searchTransactions() throws Throwable {

    }
}
