package br.com.crieasas.app.service.dto;

import java.time.Instant;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import br.com.crieasas.app.domain.enumeration.StatusSignature;

/**
 * A DTO for the Signature entity.
 */
public class SignatureDTO implements Serializable {

    private Long id;

    private String signatureHash;

    private Instant dateBegin;

    private StatusSignature statusSignature;

    private Long agreementId;

    private Long financialVariablesId;

    private Long planId;

    private Set<UserDTO> users = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSignatureHash() {
        return signatureHash;
    }

    public void setSignatureHash(String signatureHash) {
        this.signatureHash = signatureHash;
    }

    public Instant getDateBegin() {
        return dateBegin;
    }

    public void setDateBegin(Instant dateBegin) {
        this.dateBegin = dateBegin;
    }

    public StatusSignature getStatusSignature() {
        return statusSignature;
    }

    public void setStatusSignature(StatusSignature statusSignature) {
        this.statusSignature = statusSignature;
    }

    public Long getAgreementId() {
        return agreementId;
    }

    public void setAgreementId(Long agreementId) {
        this.agreementId = agreementId;
    }

    public Long getFinancialVariablesId() {
        return financialVariablesId;
    }

    public void setFinancialVariablesId(Long financialVariablesId) {
        this.financialVariablesId = financialVariablesId;
    }

    public Long getPlanId() {
        return planId;
    }

    public void setPlanId(Long planId) {
        this.planId = planId;
    }

    public Set<UserDTO> getUsers() {
        return users;
    }

    public void setUsers(Set<UserDTO> users) {
        this.users = users;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SignatureDTO signatureDTO = (SignatureDTO) o;
        if (signatureDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), signatureDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SignatureDTO{" +
            "id=" + getId() +
            ", signatureHash='" + getSignatureHash() + "'" +
            ", dateBegin='" + getDateBegin() + "'" +
            ", statusSignature='" + getStatusSignature() + "'" +
            ", agreement=" + getAgreementId() +
            ", financialVariables=" + getFinancialVariablesId() +
            ", plan=" + getPlanId() +
            "}";
    }
}
