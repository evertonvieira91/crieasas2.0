package br.com.crieasas.app.service;

import br.com.crieasas.app.service.dto.SubDomainDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing SubDomain.
 */
public interface SubDomainService {

    /**
     * Save a subDomain.
     *
     * @param subDomainDTO the entity to save
     * @return the persisted entity
     */
    SubDomainDTO save(SubDomainDTO subDomainDTO);

    /**
     * Get all the subDomains.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<SubDomainDTO> findAll(Pageable pageable);


    /**
     * Get the "id" subDomain.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<SubDomainDTO> findOne(Long id);

    /**
     * Delete the "id" subDomain.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
