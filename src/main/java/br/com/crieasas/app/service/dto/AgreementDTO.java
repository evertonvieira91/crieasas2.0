package br.com.crieasas.app.service.dto;

import java.time.Instant;
import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Agreement entity.
 */
public class AgreementDTO implements Serializable {

    private Long id;

    private String description;

    private String urlAgreement;

    private Instant createDate;

    private ZonedDateTime lastUpdate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrlAgreement() {
        return urlAgreement;
    }

    public void setUrlAgreement(String urlAgreement) {
        this.urlAgreement = urlAgreement;
    }

    public Instant getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Instant createDate) {
        this.createDate = createDate;
    }

    public ZonedDateTime getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(ZonedDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AgreementDTO agreementDTO = (AgreementDTO) o;
        if (agreementDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), agreementDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AgreementDTO{" +
            "id=" + getId() +
            ", description='" + getDescription() + "'" +
            ", urlAgreement='" + getUrlAgreement() + "'" +
            ", createDate='" + getCreateDate() + "'" +
            ", lastUpdate='" + getLastUpdate() + "'" +
            "}";
    }
}
