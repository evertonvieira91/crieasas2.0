package br.com.crieasas.app.service.mapper;

import br.com.crieasas.app.domain.*;
import br.com.crieasas.app.service.dto.PagSeguroDataDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity PagSeguroData and its DTO PagSeguroDataDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PagSeguroDataMapper extends EntityMapper<PagSeguroDataDTO, PagSeguroData> {



    default PagSeguroData fromId(Long id) {
        if (id == null) {
            return null;
        }
        PagSeguroData pagSeguroData = new PagSeguroData();
        pagSeguroData.setId(id);
        return pagSeguroData;
    }
}
