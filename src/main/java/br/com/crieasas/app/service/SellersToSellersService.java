package br.com.crieasas.app.service;

import br.com.crieasas.app.service.dto.SellersToSellersDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing SellersToSellers.
 */
public interface SellersToSellersService {

    /**
     * Save a sellersToSellers.
     *
     * @param sellersToSellersDTO the entity to save
     * @return the persisted entity
     */
    SellersToSellersDTO save(SellersToSellersDTO sellersToSellersDTO);

    /**
     * Get all the sellersToSellers.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<SellersToSellersDTO> findAll(Pageable pageable);


    /**
     * Get the "id" sellersToSellers.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<SellersToSellersDTO> findOne(Long id);

    /**
     * Delete the "id" sellersToSellers.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
