package br.com.crieasas.app.service.impl;

import br.com.crieasas.app.service.SubDomainService;
import br.com.crieasas.app.domain.SubDomain;
import br.com.crieasas.app.repository.SubDomainRepository;
import br.com.crieasas.app.service.dto.SubDomainDTO;
import br.com.crieasas.app.service.mapper.SubDomainMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.Optional;
/**
 * Service Implementation for managing SubDomain.
 */
@Service
@Transactional
public class SubDomainServiceImpl implements SubDomainService {

    private final Logger log = LoggerFactory.getLogger(SubDomainServiceImpl.class);

    private final SubDomainRepository subDomainRepository;

    private final SubDomainMapper subDomainMapper;

    public SubDomainServiceImpl(SubDomainRepository subDomainRepository, SubDomainMapper subDomainMapper) {
        this.subDomainRepository = subDomainRepository;
        this.subDomainMapper = subDomainMapper;
    }

    /**
     * Save a subDomain.
     *
     * @param subDomainDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public SubDomainDTO save(SubDomainDTO subDomainDTO) {
        log.debug("Request to save SubDomain : {}", subDomainDTO);
        SubDomain subDomain = subDomainMapper.toEntity(subDomainDTO);
        subDomain = subDomainRepository.save(subDomain);
        return subDomainMapper.toDto(subDomain);
    }

    /**
     * Get all the subDomains.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<SubDomainDTO> findAll(Pageable pageable) {
        log.debug("Request to get all SubDomains");
        return subDomainRepository.findAll(pageable)
            .map(subDomainMapper::toDto);
    }


    /**
     * Get one subDomain by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<SubDomainDTO> findOne(Long id) {
        log.debug("Request to get SubDomain : {}", id);
        return subDomainRepository.findById(id)
            .map(subDomainMapper::toDto);
    }

    /**
     * Delete the subDomain by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete SubDomain : {}", id);
        subDomainRepository.deleteById(id);
    }
}
