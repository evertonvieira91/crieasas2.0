package br.com.crieasas.app.service.impl;

import br.com.crieasas.app.service.SellersToSellersService;
import br.com.crieasas.app.domain.SellersToSellers;
import br.com.crieasas.app.repository.SellersToSellersRepository;
import br.com.crieasas.app.service.dto.SellersToSellersDTO;
import br.com.crieasas.app.service.mapper.SellersToSellersMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.Optional;
/**
 * Service Implementation for managing SellersToSellers.
 */
@Service
@Transactional
public class SellersToSellersServiceImpl implements SellersToSellersService {

    private final Logger log = LoggerFactory.getLogger(SellersToSellersServiceImpl.class);

    private final SellersToSellersRepository sellersToSellersRepository;

    private final SellersToSellersMapper sellersToSellersMapper;

    public SellersToSellersServiceImpl(SellersToSellersRepository sellersToSellersRepository, SellersToSellersMapper sellersToSellersMapper) {
        this.sellersToSellersRepository = sellersToSellersRepository;
        this.sellersToSellersMapper = sellersToSellersMapper;
    }

    /**
     * Save a sellersToSellers.
     *
     * @param sellersToSellersDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public SellersToSellersDTO save(SellersToSellersDTO sellersToSellersDTO) {
        log.debug("Request to save SellersToSellers : {}", sellersToSellersDTO);
        SellersToSellers sellersToSellers = sellersToSellersMapper.toEntity(sellersToSellersDTO);
        sellersToSellers = sellersToSellersRepository.save(sellersToSellers);
        return sellersToSellersMapper.toDto(sellersToSellers);
    }

    /**
     * Get all the sellersToSellers.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<SellersToSellersDTO> findAll(Pageable pageable) {
        log.debug("Request to get all SellersToSellers");
        return sellersToSellersRepository.findAll(pageable)
            .map(sellersToSellersMapper::toDto);
    }


    /**
     * Get one sellersToSellers by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<SellersToSellersDTO> findOne(Long id) {
        log.debug("Request to get SellersToSellers : {}", id);
        return sellersToSellersRepository.findById(id)
            .map(sellersToSellersMapper::toDto);
    }

    /**
     * Delete the sellersToSellers by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete SellersToSellers : {}", id);
        sellersToSellersRepository.deleteById(id);
    }
}
