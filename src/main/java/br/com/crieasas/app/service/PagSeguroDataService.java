package br.com.crieasas.app.service;

import br.com.crieasas.app.service.dto.PagSeguroDataDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing PagSeguroData.
 */
public interface PagSeguroDataService {

    /**
     * Save a pagSeguroData.
     *
     * @param pagSeguroDataDTO the entity to save
     * @return the persisted entity
     */
    PagSeguroDataDTO save(PagSeguroDataDTO pagSeguroDataDTO);

    /**
     * Get all the pagSeguroData.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<PagSeguroDataDTO> findAll(Pageable pageable);


    /**
     * Get the "id" pagSeguroData.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<PagSeguroDataDTO> findOne(Long id);

    /**
     * Delete the "id" pagSeguroData.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
