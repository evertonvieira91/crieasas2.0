package br.com.crieasas.app.service.mapper;

import br.com.crieasas.app.domain.*;
import br.com.crieasas.app.service.dto.FinancialVariablesDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity FinancialVariables and its DTO FinancialVariablesDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface FinancialVariablesMapper extends EntityMapper<FinancialVariablesDTO, FinancialVariables> {



    default FinancialVariables fromId(Long id) {
        if (id == null) {
            return null;
        }
        FinancialVariables financialVariables = new FinancialVariables();
        financialVariables.setId(id);
        return financialVariables;
    }
}
