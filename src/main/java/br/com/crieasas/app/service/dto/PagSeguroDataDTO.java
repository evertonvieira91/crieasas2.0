package br.com.crieasas.app.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the PagSeguroData entity.
 */
public class PagSeguroDataDTO implements Serializable {

    private Long id;

    private String sellerEmail;

    private String sellerToken;

    private String emailSandbox;

    private String passSandbox;

    private String appId;

    private String appKey;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSellerEmail() {
        return sellerEmail;
    }

    public void setSellerEmail(String sellerEmail) {
        this.sellerEmail = sellerEmail;
    }

    public String getSellerToken() {
        return sellerToken;
    }

    public void setSellerToken(String sellerToken) {
        this.sellerToken = sellerToken;
    }

    public String getEmailSandbox() {
        return emailSandbox;
    }

    public void setEmailSandbox(String emailSandbox) {
        this.emailSandbox = emailSandbox;
    }

    public String getPassSandbox() {
        return passSandbox;
    }

    public void setPassSandbox(String passSandbox) {
        this.passSandbox = passSandbox;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PagSeguroDataDTO pagSeguroDataDTO = (PagSeguroDataDTO) o;
        if (pagSeguroDataDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), pagSeguroDataDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PagSeguroDataDTO{" +
            "id=" + getId() +
            ", sellerEmail='" + getSellerEmail() + "'" +
            ", sellerToken='" + getSellerToken() + "'" +
            ", emailSandbox='" + getEmailSandbox() + "'" +
            ", passSandbox='" + getPassSandbox() + "'" +
            ", appId='" + getAppId() + "'" +
            ", appKey='" + getAppKey() + "'" +
            "}";
    }
}
