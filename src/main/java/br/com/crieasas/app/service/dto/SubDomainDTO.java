package br.com.crieasas.app.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the SubDomain entity.
 */
public class SubDomainDTO implements Serializable {

    private Long id;

    private String name;

    private Long signatureId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getSignatureId() {
        return signatureId;
    }

    public void setSignatureId(Long signatureId) {
        this.signatureId = signatureId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SubDomainDTO subDomainDTO = (SubDomainDTO) o;
        if (subDomainDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), subDomainDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SubDomainDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", signature=" + getSignatureId() +
            "}";
    }
}
