package br.com.crieasas.app.service;

import br.com.crieasas.app.service.dto.FinancialVariablesDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing FinancialVariables.
 */
public interface FinancialVariablesService {

    /**
     * Save a financialVariables.
     *
     * @param financialVariablesDTO the entity to save
     * @return the persisted entity
     */
    FinancialVariablesDTO save(FinancialVariablesDTO financialVariablesDTO);

    /**
     * Get all the financialVariables.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<FinancialVariablesDTO> findAll(Pageable pageable);


    /**
     * Get the "id" financialVariables.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<FinancialVariablesDTO> findOne(Long id);

    /**
     * Delete the "id" financialVariables.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
