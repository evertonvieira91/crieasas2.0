package br.com.crieasas.app.service.impl;

import br.com.crieasas.app.service.PagSeguroService;
import br.com.crieasas.app.service.dto.PagSeguroDTO;
import br.com.uol.pagseguro.api.PagSeguro;
import br.com.uol.pagseguro.api.PagSeguroEnv;
import br.com.uol.pagseguro.api.common.domain.DataList;
import br.com.uol.pagseguro.api.common.domain.builder.*;
import br.com.uol.pagseguro.api.common.domain.enums.*;
import br.com.uol.pagseguro.api.credential.Credential;
import br.com.uol.pagseguro.api.direct.preapproval.*;
import br.com.uol.pagseguro.api.http.JSEHttpClient;
import br.com.uol.pagseguro.api.session.CreatedSession;
import br.com.uol.pagseguro.api.transaction.search.TransactionDetail;
import br.com.uol.pagseguro.api.utils.logging.SimpleLoggerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.xml.bind.DatatypeConverter;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;

@Service
@Transactional
public class PagSeguroServiceImpl implements PagSeguroService {
    private final Logger log = LoggerFactory.getLogger(PagSeguroServiceImpl.class);

    private String sellerEmail = PagSeguroDTO.getSellerEmail();
    private String sellerToken = PagSeguroDTO.getSellerToken();
    private String codePlan = PagSeguroDTO.getCodePlan();
    private String codeSignature = PagSeguroDTO.getCodeSignature();


    @Override
    public void createAutomaticPlan() throws Throwable {

        try {

            final PagSeguro pagSeguro = PagSeguro
                .instance(new SimpleLoggerFactory(), new JSEHttpClient(),
                    Credential.sellerCredential(sellerEmail, sellerToken), PagSeguroEnv.SANDBOX);

            RegisteredDirectPreApprovalRequest registeredPreApproval = pagSeguro.directPreApprovals().register(
                new DirectPreApprovalRequestRegistrationBuilder()
                    .withRedirectURL("http://www.seusite.com.br/assinatura-concluidaa")
                    .withReference("XXXXXX")
                    .withReviewURL("http://lojamodelo.com.br/revisar")
                    .withMaxUses(500)

                    .withPreApproval(new PreApprovalRequestBuilder()
                        .withName("Assinatura da Revista Vegana")
                        .withCharge(Charge.AUTO)
                        .withPeriod(Period.MONTHLY)
                        .withDetails("Assinatura mensal de Revista")
                        .withAmountPerPayment(new BigDecimal(100.00))
                        .withExpiration(new ExpirationBuilder()
                            .withValue(1)
                            .withUnit(Unit.YEARS))
                    )

            );
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
            System.out.println(sdf.format(registeredPreApproval.getPreApprovalDate()));
            System.out.println(registeredPreApproval.getPreApprovalCode());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //cria uma sessão de pagamento para retornar o token necessario para o front
    //seller session (CreateDirectPreApprovalSellerSession.java)
    @Override
    public String criaSessaoPagamento() throws Throwable {

        final PagSeguro pagSeguro = PagSeguro
            .instance(new SimpleLoggerFactory(), new JSEHttpClient(), Credential.sellerCredential(sellerEmail,
                sellerToken), PagSeguroEnv.SANDBOX);

        try {

            // Criacao de sessao de seller
            CreatedSession createdSession = pagSeguro.sessions().createDirectPreApproval();
            return createdSession.getId();
        } catch (Exception e) {
            //e.printStackTrace();
            return e.getMessage();
        }

    }

    //Foi testado porem a veracidade do resultado não pode ser obtida, pois não podemos ver o cartão do usuario
    @Override
    public boolean changePaymentMethodPreApproval() {
        try {
            final PagSeguro pagSeguro = PagSeguro
                .instance(new SimpleLoggerFactory(), new JSEHttpClient(),
                    Credential.sellerCredential(sellerEmail, sellerToken), PagSeguroEnv.SANDBOX);

            //Permite a alteração do meio de pagamento atrelado ao pagamento do plano para as próximas cobranças
            pagSeguro.directPreApprovals().changePaymentMethod(
                new DirectPreApprovalChangingPaymentMethodBuilder()
                    .withPreApprovalCode("69C22B7B424257E884529F8498B422D9") // código da assinatura
                    .withType(PreApprovalPaymentMethodType.CREDITCARD)
                    .withSenderRiskData(new PreApprovalSenderRiskDataBuilder()
                        .withIp("1.1.1.1")
                        .withHash("HASHCODE")
                    )
                    .withCreditCard(new PreApprovalCreditCardBuilder()
                        .withToken("268321c768384e6f9c3b3e124f09cb71") // Codigo assinatura
                        .withHolder(new PreApprovalHolderBuilder()
                            .withName("JOSÉ Edicao")
                            .withBirthDate(new SimpleDateFormat("dd/MM/yyyy").parse("20/12/1990"))
                            .addDocument(new DocumentBuilder()
                                .withType(DocumentType.CPF)
                                .withValue("99999999999")
                            )
                            .withPhone(new PhoneBuilder()
                                .withAreaCode("99")
                                .withNumber("99999999")
                            )
                            .withBillingAddress(new AddressBuilder()
                                .withStreet("Av. PagSeguro")
                                .withNumber("9999")
                                .withComplement("99o andar")
                                .withDistrict("Jardim Internet")
                                .withCity("Cidade Exemplo")
                                .withState(State.SP)
                                .withCountry("BRA")
                                .withPostalCode("99999999")
                            )
                        )
                    )
            );
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public boolean changeStatusPreApproval() {

        try {
            final PagSeguro pagSeguro = PagSeguro
                .instance(new SimpleLoggerFactory(), new JSEHttpClient(),
                    Credential.sellerCredential(sellerEmail, sellerToken), PagSeguroEnv.SANDBOX);

            // Alteração do status de adesão (assinatura) [ suspenção: "SUSPENDED" | reativação: "ACTIVE" ]
            pagSeguro.directPreApprovals().changeStatus(
                new DirectPreApprovalChangingStatusBuilder()
                    .withCode(codeSignature)
                    .withStatus(PreApprovalStatus.ACTIVE)
            );

            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public boolean editValuePreApproval() {
        try {
            final PagSeguro pagSeguro = PagSeguro
                .instance(new SimpleLoggerFactory(), new JSEHttpClient(),
                    Credential.sellerCredential(sellerEmail, sellerToken), PagSeguroEnv.SANDBOX);

            //Edição de Valor em Planos
            pagSeguro.directPreApprovals().edit(
                new DirectPreApprovalEditionBuilder()
                    .withCode(codePlan) //codigo do plano
                    .withAmountPerPayment("59.99")
                    .withUpdateSubscriptions(false)
            );

            return true;
        } catch (Exception e) {
            return false;
        }
    }


    @Override
    public void accedeDirectPreApprovalPlan() throws Throwable {
        //teste de porra nnenhuma
        try {

            final PagSeguro pagSeguro = PagSeguro
                .instance(new SimpleLoggerFactory(), new JSEHttpClient(),
                    Credential.sellerCredential(sellerEmail, sellerToken), PagSeguroEnv.SANDBOX);

            // Aderindo ao plano FFAC8AE62424AC5884C90F8DAAE2F21A
            AccededDirectPreApproval accededDirectPreApproval = pagSeguro.directPreApprovals().accede(
                new DirectPreApprovalAccessionBuilder()
                    .withPlan(codePlan)
                    .withReference("XXXXXX")
                    .withSender(new SenderBuilder()
                        .withName("José Comprador")
                        .withEmail("senderemail@sandbox.pagseguro.com.br")
                        .withIp("1.1.1.1")
                        .withHash("HASHHERE")
                        .withPhone(new PhoneBuilder()
                            .withAreaCode("99")
                            .withNumber("99999999")
                        )
                        .withAddress(new AddressBuilder()
                            .withStreet("Av. PagSeguro")
                            .withNumber("9999")
                            .withComplement("99o andar")
                            .withDistrict("Jardim Internet")
                            .withCity("Cidade Exemplo")
                            .withState(State.SP)
                            .withCountry("BRA")
                            .withPostalCode("99999999")
                        )
                        .addDocument(new DocumentBuilder()
                            .withType(DocumentType.CPF)
                            .withValue("99999999999")
                        )
                    )

                    .withPaymentMethod(new PreApprovalPaymentMethodBuilder()
                        .withType(PreApprovalPaymentMethodType.CREDITCARD)
                        .withCreditCard(new PreApprovalCreditCardBuilder()
                            .withToken("268321c768384e6f9c3b3e124f09cb71")
                            .withHolder(new PreApprovalHolderBuilder()
                                .withName("JOSÉ COMPRADOR")
                                .withBirthDate(new SimpleDateFormat("dd/MM/yyyy").parse("20/12/1990"))
                                .addDocument(new DocumentBuilder()
                                    .withType(DocumentType.CPF)
                                    .withValue("99999999999")
                                )
                                .withPhone(new PhoneBuilder()
                                    .withAreaCode("99")
                                    .withNumber("99999999")
                                )
                                .withBillingAddress(new AddressBuilder()
                                    .withStreet("Av. PagSeguro")
                                    .withNumber("9999")
                                    .withComplement("99o andar")
                                    .withDistrict("Jardim Internet")
                                    .withCity("Cidade Exemplo")
                                    .withState(State.SP)
                                    .withCountry("BRA")
                                    .withPostalCode("99999999")
                                )
                            )
                        )
                    )
            );

            System.out.println(accededDirectPreApproval.getPreApprovalCode());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Consulta uma recorrência pelo código de adesao
    @Override
    public String searchPlanByAccessionCode() throws Throwable{
    //teste
        try{
            final PagSeguro pagSeguro = PagSeguro.instance(
                new SimpleLoggerFactory(),
                new JSEHttpClient(),
                Credential.sellerCredential(sellerEmail, sellerToken),
                PagSeguroEnv.SANDBOX
            );

            // Consulta uma recorrência pelo código
            SearchedDirectPreApprovalByAccessionCode searchedPreApproval = pagSeguro.directPreApprovals().searchByAccessionCode(
                new DirectPreApprovalSearchByAccessionCodeBuilder()
                    .withCode("90CCCBEAACACA1EAA4B84FA84B46A00A")
            );

            System.out.println("Consulta pelo codigo de adesão realizada!");
            return searchedPreApproval.getStatus();

        }catch (Exception e){
            return e.getMessage();
        }
    }

    @Override
    public DataList<? extends DirectPreApprovalData> searchPlanByDateInterval() {
        try {

            final PagSeguro pagSeguro = PagSeguro.instance(
                Credential.sellerCredential(sellerEmail, sellerToken),
                PagSeguroEnv.SANDBOX
            );

            // Listando as recorrências criadas dentro do intervalo de datas
            final DataList<? extends DirectPreApprovalData> directPreApprovalByDateInterval = pagSeguro.directPreApprovals()
                .listDirectPreApprovalByDateInterval(new DirectPreApprovalByDateIntervalListBuilder()
                        .withDateRange(
                            new DateRangeBuilder().between(
                                DatatypeConverter.parseDateTime("2018-08-01T00:00:00.000-03:00").getTime(),
                                DatatypeConverter.parseDateTime("2018-08-15T15:56:00.000-03:00").getTime()
                            )
                        )
                    //.withPage(1) //(opcional) página na qual se quer observar os resultados
                    //.withMaxPageResults(1) //(opcional) Número máximo de registros por página
                    //.withStatus(DirectPreApprovalStatus.CANCELLED_BY_RECEIVER) //(opcional) Status em que se encontra a recorrência.
                    //.withCode("C08984179E9EDF3DD4023F87B71DE349") // (opcional) código do plano
                    //.withSenderEmail("senderemail@sandbox.pagseguro.com.br") // (opcional) E-mail do Comprador
                );

            return directPreApprovalByDateInterval;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void searchPlanByDayInterval() {
        try {

            final PagSeguro pagSeguro = PagSeguro.instance(
                Credential.sellerCredential(sellerEmail, sellerToken),
                PagSeguroEnv.SANDBOX
            );

            // Listando as recorrências  que tiveram algum tipo de notificação dentro de um intervalo de dias
            final DataList<? extends DirectPreApprovalData> directPreApprovalByDayInterval = pagSeguro.directPreApprovals()
                .listDirectPreApprovalByDayInterval(new DirectPreApprovalByDayIntervalListBuilder()
                        .withInterval(20) // (obrigatório) quantidade de dias de intervalo (máx: 30)
                    //.withPage(1) //(opcional) página na qual se quer observar os resultados
                    //.withMaxPageResults(1) //(opcional) Número máximo de registros por página
                );

            System.out.println(directPreApprovalByDayInterval);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void SearchPlanByNotificationCode() throws Throwable{

        try{
            final PagSeguro pagSeguro = PagSeguro.instance(
                new SimpleLoggerFactory(),
                new JSEHttpClient(),
                Credential.sellerCredential(sellerEmail, sellerToken),
                PagSeguroEnv.SANDBOX
            );

            // Consulta pelo código de adesão (assinatura)
            SearchedDirectPreApprovalByNotificationCode searchedPreApproval = pagSeguro.directPreApprovals().searchByNotificationCode(
                new DirectPreApprovalSearchByNotificationCodeBuilder()
                    .withCode("855E73E89494F1D994B05FBD5EEC12B8")
            );

            System.out.println("Consulta pelo codigo de notificacao realizada!");
            System.out.println(searchedPreApproval);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void discountPayment() throws Throwable {
        try{
            final PagSeguro pagSeguro = PagSeguro
                .instance(new SimpleLoggerFactory(), new JSEHttpClient(),
                    Credential.sellerCredential(sellerEmail, sellerToken), PagSeguroEnv.SANDBOX);

            //Desconto na próxima cobrança
            pagSeguro.directPreApprovals().discount(
                new DirectPreApprovalDiscountBuilder()
                    .withCode("855E73E89494F1D994B05FBD5EEC12B8") //código da assinatura
                    .withType(DiscountType.DISCOUNT_PERCENT)
                    .withValue("10.50")
            );

            System.out.println("Desconto realizado!");
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public DataList<? extends PaymentOrder> listDirectPaymentOrders() throws Throwable {
        try {

            final PagSeguro pagSeguro = PagSeguro.instance(Credential.sellerCredential(sellerEmail,
                sellerToken), PagSeguroEnv.SANDBOX);

            // Listando as ordens de pagamento da assinatura de código 09CA1D3141415C74441C6FA8E0B3ED14
            final DataList<? extends PaymentOrder> paymentOrders = pagSeguro.directPreApprovals()
                .listPaymentOrders(new DirectPreApprovalPaymentOrdersListBuilder()
                        .withCode("855E73E89494F1D994B05FBD5EEC12B8") // código da assinatura
                    //.withStatus(PaymentOrderStatus.PROCESSANDO) //(opcional) status em que se encontra a ordem de pagamento.
                    //.withPage(1) //(opcional) página na qual se quer observar os resultados
                    //.withMaxPageResults(3) //(opcional) Número máximo de registros por página
                );

           return paymentOrders;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }






}
