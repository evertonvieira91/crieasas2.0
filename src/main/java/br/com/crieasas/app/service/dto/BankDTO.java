package br.com.crieasas.app.service.dto;

import br.com.crieasas.app.domain.Bank;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Bank entity.
 */
public class BankDTO implements Serializable {

    private Long id;

    private Float amountPro;

    private Long userId;

    public BankDTO(Long id, Float amountPro, Long userId, String userLogin) {
        this.id = id;
        this.amountPro = amountPro;
        this.userId = userId;
        this.userLogin = userLogin;
    }
    public BankDTO(Bank bank) {
        this.id = bank.getId();
        this.amountPro = bank.getAmountPro();
        this.userId = bank.getUser().getId();
        this.userLogin = bank.getUser().getLogin();
    }
    //public BankDTO() {}

    private String userLogin;

    public BankDTO() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getAmountPro() {
        return amountPro;
    }

    public void setAmountPro(Float amountPro) {
        this.amountPro = amountPro;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BankDTO bankDTO = (BankDTO) o;
        if (bankDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), bankDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "BankDTO{" +
            "id=" + getId() +
            ", amountPro=" + getAmountPro() +
            ", user=" + getUserId() +
            ", user='" + getUserLogin() + "'" +
            "}";
    }
}
