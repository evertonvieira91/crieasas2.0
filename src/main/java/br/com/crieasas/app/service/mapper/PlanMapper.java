package br.com.crieasas.app.service.mapper;

import br.com.crieasas.app.domain.*;
import br.com.crieasas.app.service.dto.PlanDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Plan and its DTO PlanDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PlanMapper extends EntityMapper<PlanDTO, Plan> {



    default Plan fromId(Long id) {
        if (id == null) {
            return null;
        }
        Plan plan = new Plan();
        plan.setId(id);
        return plan;
    }
}
