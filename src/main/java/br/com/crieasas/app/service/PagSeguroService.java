package br.com.crieasas.app.service;

import br.com.uol.pagseguro.api.common.domain.DataList;
import br.com.uol.pagseguro.api.direct.preapproval.DirectPreApprovalData;
import br.com.uol.pagseguro.api.direct.preapproval.PaymentOrder;

public interface PagSeguroService {
    /*
     * Primeiro passo para criar uma adesão(Assinatura) com pagamento recorrente
     * Cria um plano automáico com cobrança mensais de R$ 100,00 por 1 ano
     * Retorno: código do plano criado;
     * O código será usado para fazer adesão ao plano
    */
    public void createAutomaticPlan() throws Throwable;

    public String criaSessaoPagamento() throws Throwable;

    public void accedeDirectPreApprovalPlan() throws Throwable;

    public boolean changePaymentMethodPreApproval();

    public boolean changeStatusPreApproval();

    public boolean editValuePreApproval();

    public String searchPlanByAccessionCode() throws Throwable;

    public DataList<? extends DirectPreApprovalData> searchPlanByDateInterval() throws Throwable;

    public void searchPlanByDayInterval() throws Throwable;

    public void SearchPlanByNotificationCode() throws Throwable;

    public void discountPayment() throws Throwable;

    public DataList<? extends PaymentOrder> listDirectPaymentOrders() throws Throwable;


}
