package br.com.crieasas.app.service.impl;

import br.com.crieasas.app.service.FinancialVariablesService;
import br.com.crieasas.app.domain.FinancialVariables;
import br.com.crieasas.app.repository.FinancialVariablesRepository;
import br.com.crieasas.app.service.dto.FinancialVariablesDTO;
import br.com.crieasas.app.service.mapper.FinancialVariablesMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.Optional;
/**
 * Service Implementation for managing FinancialVariables.
 */
@Service
@Transactional
public class FinancialVariablesServiceImpl implements FinancialVariablesService {

    private final Logger log = LoggerFactory.getLogger(FinancialVariablesServiceImpl.class);

    private final FinancialVariablesRepository financialVariablesRepository;

    private final FinancialVariablesMapper financialVariablesMapper;

    public FinancialVariablesServiceImpl(FinancialVariablesRepository financialVariablesRepository, FinancialVariablesMapper financialVariablesMapper) {
        this.financialVariablesRepository = financialVariablesRepository;
        this.financialVariablesMapper = financialVariablesMapper;
    }

    /**
     * Save a financialVariables.
     *
     * @param financialVariablesDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public FinancialVariablesDTO save(FinancialVariablesDTO financialVariablesDTO) {
        log.debug("Request to save FinancialVariables : {}", financialVariablesDTO);
        FinancialVariables financialVariables = financialVariablesMapper.toEntity(financialVariablesDTO);
        financialVariables = financialVariablesRepository.save(financialVariables);
        return financialVariablesMapper.toDto(financialVariables);
    }

    /**
     * Get all the financialVariables.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<FinancialVariablesDTO> findAll(Pageable pageable) {
        log.debug("Request to get all FinancialVariables");
        return financialVariablesRepository.findAll(pageable)
            .map(financialVariablesMapper::toDto);
    }


    /**
     * Get one financialVariables by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<FinancialVariablesDTO> findOne(Long id) {
        log.debug("Request to get FinancialVariables : {}", id);
        return financialVariablesRepository.findById(id)
            .map(financialVariablesMapper::toDto);
    }

    /**
     * Delete the financialVariables by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete FinancialVariables : {}", id);
        financialVariablesRepository.deleteById(id);
    }
}
