package br.com.crieasas.app.service.mapper;

import br.com.crieasas.app.domain.*;
import br.com.crieasas.app.service.dto.SellersToSellersDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity SellersToSellers and its DTO SellersToSellersDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class})
public interface SellersToSellersMapper extends EntityMapper<SellersToSellersDTO, SellersToSellers> {

    @Mapping(source = "sun.id", target = "sunId")
    @Mapping(source = "father.id", target = "fatherId")
    SellersToSellersDTO toDto(SellersToSellers sellersToSellers);

    @Mapping(source = "sunId", target = "sun")
    @Mapping(source = "fatherId", target = "father")
    SellersToSellers toEntity(SellersToSellersDTO sellersToSellersDTO);

    default SellersToSellers fromId(Long id) {
        if (id == null) {
            return null;
        }
        SellersToSellers sellersToSellers = new SellersToSellers();
        sellersToSellers.setId(id);
        return sellersToSellers;
    }
}
