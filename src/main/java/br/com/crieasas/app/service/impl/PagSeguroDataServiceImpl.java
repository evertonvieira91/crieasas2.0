package br.com.crieasas.app.service.impl;

import br.com.crieasas.app.service.PagSeguroDataService;
import br.com.crieasas.app.domain.PagSeguroData;
import br.com.crieasas.app.repository.PagSeguroDataRepository;
import br.com.crieasas.app.service.dto.PagSeguroDataDTO;
import br.com.crieasas.app.service.mapper.PagSeguroDataMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.Optional;
/**
 * Service Implementation for managing PagSeguroData.
 */
@Service
@Transactional
public class PagSeguroDataServiceImpl implements PagSeguroDataService {

    private final Logger log = LoggerFactory.getLogger(PagSeguroDataServiceImpl.class);

    private final PagSeguroDataRepository pagSeguroDataRepository;

    private final PagSeguroDataMapper pagSeguroDataMapper;

    public PagSeguroDataServiceImpl(PagSeguroDataRepository pagSeguroDataRepository, PagSeguroDataMapper pagSeguroDataMapper) {
        this.pagSeguroDataRepository = pagSeguroDataRepository;
        this.pagSeguroDataMapper = pagSeguroDataMapper;
    }

    /**
     * Save a pagSeguroData.
     *
     * @param pagSeguroDataDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public PagSeguroDataDTO save(PagSeguroDataDTO pagSeguroDataDTO) {
        log.debug("Request to save PagSeguroData : {}", pagSeguroDataDTO);
        PagSeguroData pagSeguroData = pagSeguroDataMapper.toEntity(pagSeguroDataDTO);
        pagSeguroData = pagSeguroDataRepository.save(pagSeguroData);
        return pagSeguroDataMapper.toDto(pagSeguroData);
    }

    /**
     * Get all the pagSeguroData.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PagSeguroDataDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PagSeguroData");
        return pagSeguroDataRepository.findAll(pageable)
            .map(pagSeguroDataMapper::toDto);
    }


    /**
     * Get one pagSeguroData by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<PagSeguroDataDTO> findOne(Long id) {
        log.debug("Request to get PagSeguroData : {}", id);
        return pagSeguroDataRepository.findById(id)
            .map(pagSeguroDataMapper::toDto);
    }

    /**
     * Delete the pagSeguroData by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete PagSeguroData : {}", id);
        pagSeguroDataRepository.deleteById(id);
    }
}
