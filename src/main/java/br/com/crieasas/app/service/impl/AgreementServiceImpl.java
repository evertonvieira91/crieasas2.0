package br.com.crieasas.app.service.impl;

import br.com.crieasas.app.service.AgreementService;
import br.com.crieasas.app.domain.Agreement;
import br.com.crieasas.app.repository.AgreementRepository;
import br.com.crieasas.app.service.dto.AgreementDTO;
import br.com.crieasas.app.service.mapper.AgreementMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.Optional;
/**
 * Service Implementation for managing Agreement.
 */
@Service
@Transactional
public class AgreementServiceImpl implements AgreementService {

    private final Logger log = LoggerFactory.getLogger(AgreementServiceImpl.class);

    private final AgreementRepository agreementRepository;

    private final AgreementMapper agreementMapper;

    public AgreementServiceImpl(AgreementRepository agreementRepository, AgreementMapper agreementMapper) {
        this.agreementRepository = agreementRepository;
        this.agreementMapper = agreementMapper;
    }

    /**
     * Save a agreement.
     *
     * @param agreementDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public AgreementDTO save(AgreementDTO agreementDTO) {
        log.debug("Request to save Agreement : {}", agreementDTO);
        Agreement agreement = agreementMapper.toEntity(agreementDTO);
        agreement = agreementRepository.save(agreement);
        return agreementMapper.toDto(agreement);
    }

    /**
     * Get all the agreements.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<AgreementDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Agreements");
        return agreementRepository.findAll(pageable)
            .map(agreementMapper::toDto);
    }


    /**
     * Get one agreement by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<AgreementDTO> findOne(Long id) {
        log.debug("Request to get Agreement : {}", id);
        return agreementRepository.findById(id)
            .map(agreementMapper::toDto);
    }

    /**
     * Delete the agreement by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Agreement : {}", id);
        agreementRepository.deleteById(id);
    }
}
