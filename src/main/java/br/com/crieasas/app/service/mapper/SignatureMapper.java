package br.com.crieasas.app.service.mapper;

import br.com.crieasas.app.domain.*;
import br.com.crieasas.app.service.dto.SignatureDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Signature and its DTO SignatureDTO.
 */
@Mapper(componentModel = "spring", uses = {AgreementMapper.class, FinancialVariablesMapper.class, PlanMapper.class, UserMapper.class})
public interface SignatureMapper extends EntityMapper<SignatureDTO, Signature> {

    @Mapping(source = "agreement.id", target = "agreementId")
    @Mapping(source = "financialVariables.id", target = "financialVariablesId")
    @Mapping(source = "plan.id", target = "planId")
    SignatureDTO toDto(Signature signature);

    @Mapping(source = "agreementId", target = "agreement")
    @Mapping(source = "financialVariablesId", target = "financialVariables")
    @Mapping(source = "planId", target = "plan")
    Signature toEntity(SignatureDTO signatureDTO);

    default Signature fromId(Long id) {
        if (id == null) {
            return null;
        }
        Signature signature = new Signature();
        signature.setId(id);
        return signature;
    }
}
