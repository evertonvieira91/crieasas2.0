package br.com.crieasas.app.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the FinancialVariables entity.
 */
public class FinancialVariablesDTO implements Serializable {

    private Long id;

    private ZonedDateTime closingDay;

    private ZonedDateTime billingDay;

    private Float percentageLevel1;

    private Float percentageLevel2;

    private Float percentageLevel3;

    private Float percentageLevel4;

    private Float percentageLevel5;

    private Float valueAdminFee;

    private Float percentageBonus;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getClosingDay() {
        return closingDay;
    }

    public void setClosingDay(ZonedDateTime closingDay) {
        this.closingDay = closingDay;
    }

    public ZonedDateTime getBillingDay() {
        return billingDay;
    }

    public void setBillingDay(ZonedDateTime billingDay) {
        this.billingDay = billingDay;
    }

    public Float getPercentageLevel1() {
        return percentageLevel1;
    }

    public void setPercentageLevel1(Float percentageLevel1) {
        this.percentageLevel1 = percentageLevel1;
    }

    public Float getPercentageLevel2() {
        return percentageLevel2;
    }

    public void setPercentageLevel2(Float percentageLevel2) {
        this.percentageLevel2 = percentageLevel2;
    }

    public Float getPercentageLevel3() {
        return percentageLevel3;
    }

    public void setPercentageLevel3(Float percentageLevel3) {
        this.percentageLevel3 = percentageLevel3;
    }

    public Float getPercentageLevel4() {
        return percentageLevel4;
    }

    public void setPercentageLevel4(Float percentageLevel4) {
        this.percentageLevel4 = percentageLevel4;
    }

    public Float getPercentageLevel5() {
        return percentageLevel5;
    }

    public void setPercentageLevel5(Float percentageLevel5) {
        this.percentageLevel5 = percentageLevel5;
    }

    public Float getValueAdminFee() {
        return valueAdminFee;
    }

    public void setValueAdminFee(Float valueAdminFee) {
        this.valueAdminFee = valueAdminFee;
    }

    public Float getPercentageBonus() {
        return percentageBonus;
    }

    public void setPercentageBonus(Float percentageBonus) {
        this.percentageBonus = percentageBonus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        FinancialVariablesDTO financialVariablesDTO = (FinancialVariablesDTO) o;
        if (financialVariablesDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), financialVariablesDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "FinancialVariablesDTO{" +
            "id=" + getId() +
            ", closingDay='" + getClosingDay() + "'" +
            ", billingDay='" + getBillingDay() + "'" +
            ", percentageLevel1=" + getPercentageLevel1() +
            ", percentageLevel2=" + getPercentageLevel2() +
            ", percentageLevel3=" + getPercentageLevel3() +
            ", percentageLevel4=" + getPercentageLevel4() +
            ", percentageLevel5=" + getPercentageLevel5() +
            ", valueAdminFee=" + getValueAdminFee() +
            ", percentageBonus=" + getPercentageBonus() +
            "}";
    }
}
