package br.com.crieasas.app.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the SellersToSellers entity.
 */
public class SellersToSellersDTO implements Serializable {

    private Long id;

    private Long sunId;

    private Long fatherId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSunId() { return sunId; }

    public void setSunId(Long userId) {
        this.sunId = userId;
    }

    public Long getFatherId() {
        return fatherId;
    }

    public void setFatherId(Long userId) {
        this.fatherId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SellersToSellersDTO sellersToSellersDTO = (SellersToSellersDTO) o;
        if (sellersToSellersDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), sellersToSellersDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SellersToSellersDTO{" +
            "id=" + getId() +
            ", sunId=" + getSunId() +
            ", fatherId=" + getFatherId() +
            "}";
    }
}
