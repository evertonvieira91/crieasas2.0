package br.com.crieasas.app.service;

import br.com.crieasas.app.service.dto.BankDTO;

import br.com.crieasas.app.service.dto.UserDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing Bank.
 */
public interface BankService {

    /**
     * Save a bank.
     *
     * @param bankDTO the entity to save
     * @return the persisted entity
     */
    BankDTO save(BankDTO bankDTO);

    /**
     * Get all the banks.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<BankDTO> findAll(Pageable pageable);


    /**
     * Get the "id" bank.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<BankDTO> findOne(Long id);

    /**
     * Delete the "id" bank.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    void depositCommission(Long id);

    void depositMembershipFee(Long sonId);
}
