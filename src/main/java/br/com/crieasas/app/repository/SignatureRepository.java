package br.com.crieasas.app.repository;

import br.com.crieasas.app.domain.Signature;
import br.com.crieasas.app.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Signature entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SignatureRepository extends JpaRepository<Signature, Long> {

    @Query(value = "select distinct signature from Signature signature left join fetch signature.users",
        countQuery = "select count(distinct signature) from Signature signature")
    Page<Signature> findAllWithEagerRelationships(Pageable pageable);

    @Query(value = "select distinct signature from Signature signature left join fetch signature.users")
    List<Signature> findAllWithEagerRelationships();

    @Query("select signature from Signature signature left join fetch signature.users where signature.id =:id")
    Optional<Signature> findOneWithEagerRelationships(@Param("id") Long id);

    @Query(value = "select * from signature LEFT JOIN signature_user ON signature_user.signatures_id = signature.id where signature_user.users_id = ?1", nativeQuery = true)
    //@Query("select signature from Signature signature LEFT JOIN fetch signature.users where signature.users.id = ?1")
    Signature findByUserId(Long id);


    //Signature findByUserId(User user);


}
