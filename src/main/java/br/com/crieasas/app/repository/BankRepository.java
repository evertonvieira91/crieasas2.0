package br.com.crieasas.app.repository;

import br.com.crieasas.app.domain.Bank;
import br.com.crieasas.app.domain.User;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Bank entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BankRepository extends JpaRepository<Bank, Long> {

    Bank findByUser(User user);

}
