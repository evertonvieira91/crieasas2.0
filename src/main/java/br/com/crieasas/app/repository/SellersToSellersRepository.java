package br.com.crieasas.app.repository;

import br.com.crieasas.app.domain.SellersToSellers;
import br.com.crieasas.app.domain.User;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the SellersToSellers entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SellersToSellersRepository extends JpaRepository<SellersToSellers, Long> {

    @Query("select sellers_to_sellers from SellersToSellers sellers_to_sellers where sellers_to_sellers.father.login = ?#{principal.username}")
    List<SellersToSellers> findByFatherIsCurrentUser();

    //@Query("select sellers_to_sellers from SellersToSellers sellers_to_sellers where sellers_to_sellers.sunId.id = :id")
    @Query(value = "select * from sellers_to_sellers where sellers_to_sellers.sun_id = ?1",nativeQuery = true)
    SellersToSellers findBySunId(Long id);



}
