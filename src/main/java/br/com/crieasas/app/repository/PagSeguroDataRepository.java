package br.com.crieasas.app.repository;

import br.com.crieasas.app.domain.PagSeguroData;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the PagSeguroData entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PagSeguroDataRepository extends JpaRepository<PagSeguroData, Long> {

}
