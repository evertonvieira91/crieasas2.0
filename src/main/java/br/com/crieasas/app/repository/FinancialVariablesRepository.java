package br.com.crieasas.app.repository;

import br.com.crieasas.app.domain.FinancialVariables;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the FinancialVariables entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FinancialVariablesRepository extends JpaRepository<FinancialVariables, Long> {

}
