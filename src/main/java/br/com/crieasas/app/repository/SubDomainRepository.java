package br.com.crieasas.app.repository;

import br.com.crieasas.app.domain.SubDomain;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the SubDomain entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SubDomainRepository extends JpaRepository<SubDomain, Long> {

}
