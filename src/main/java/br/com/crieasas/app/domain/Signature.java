package br.com.crieasas.app.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import br.com.crieasas.app.domain.enumeration.StatusSignature;

/**
 * A Signature.
 */
@Entity
@Table(name = "signature")
public class Signature implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "signature_hash")
    private String signatureHash;

    @Column(name = "date_begin")
    private Instant dateBegin;

    @Enumerated(EnumType.STRING)
    @Column(name = "status_signature")
    private StatusSignature statusSignature;

    @OneToOne
    @JoinColumn(unique = true)
    private Agreement agreement;

    @OneToOne
    @JoinColumn(unique = true)
    private FinancialVariables financialVariables;

    @ManyToOne
    @JsonIgnoreProperties("")
    private Plan plan;

    @ManyToMany
    @JoinTable(name = "signature_user",
               joinColumns = @JoinColumn(name = "signatures_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "users_id", referencedColumnName = "id"))
    private Set<User> users = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSignatureHash() {
        return signatureHash;
    }

    public Signature signatureHash(String signatureHash) {
        this.signatureHash = signatureHash;
        return this;
    }

    public void setSignatureHash(String signatureHash) {
        this.signatureHash = signatureHash;
    }

    public Instant getDateBegin() {
        return dateBegin;
    }

    public Signature dateBegin(Instant dateBegin) {
        this.dateBegin = dateBegin;
        return this;
    }

    public void setDateBegin(Instant dateBegin) {
        this.dateBegin = dateBegin;
    }

    public StatusSignature getStatusSignature() {
        return statusSignature;
    }

    public Signature statusSignature(StatusSignature statusSignature) {
        this.statusSignature = statusSignature;
        return this;
    }

    public void setStatusSignature(StatusSignature statusSignature) {
        this.statusSignature = statusSignature;
    }

    public Agreement getAgreement() {
        return agreement;
    }

    public Signature agreement(Agreement agreement) {
        this.agreement = agreement;
        return this;
    }

    public void setAgreement(Agreement agreement) {
        this.agreement = agreement;
    }

    public FinancialVariables getFinancialVariables() {
        return financialVariables;
    }

    public Signature financialVariables(FinancialVariables financialVariables) {
        this.financialVariables = financialVariables;
        return this;
    }

    public void setFinancialVariables(FinancialVariables financialVariables) {
        this.financialVariables = financialVariables;
    }

    public Plan getPlan() {
        return plan;
    }

    public Signature plan(Plan plan) {
        this.plan = plan;
        return this;
    }

    public void setPlan(Plan plan) {
        this.plan = plan;
    }

    public Set<User> getUsers() {
        return users;
    }

    public Signature users(Set<User> users) {
        this.users = users;
        return this;
    }

    public Signature addUser(User user) {
        this.users.add(user);
        return this;
    }

    public Signature removeUser(User user) {
        this.users.remove(user);
        return this;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Signature signature = (Signature) o;
        if (signature.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), signature.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Signature{" +
            "id=" + getId() +
            ", signatureHash='" + getSignatureHash() + "'" +
            ", dateBegin='" + getDateBegin() + "'" +
            ", statusSignature='" + getStatusSignature() + "'" +
            "}";
    }
}
