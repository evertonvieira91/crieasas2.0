package br.com.crieasas.app.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A Plan.
 */
@Entity
@Table(name = "plan")
public class Plan implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "details")
    private String details;

    @Column(name = "pg_plan_hash")
    private String pgPlanHash;

    @Column(name = "pg_plan_code_reference")
    private String pgPlanCodeReference;

    @Column(name = "jhi_value")
    private Float value;

    @Column(name = "billing_period")
    private String billingPeriod;

    @Column(name = "membership_fee")
    private Float membershipFee;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Plan name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetails() {
        return details;
    }

    public Plan details(String details) {
        this.details = details;
        return this;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getPgPlanHash() {
        return pgPlanHash;
    }

    public Plan pgPlanHash(String pgPlanHash) {
        this.pgPlanHash = pgPlanHash;
        return this;
    }

    public void setPgPlanHash(String pgPlanHash) {
        this.pgPlanHash = pgPlanHash;
    }

    public String getPgPlanCodeReference() {
        return pgPlanCodeReference;
    }

    public Plan pgPlanCodeReference(String pgPlanCodeReference) {
        this.pgPlanCodeReference = pgPlanCodeReference;
        return this;
    }

    public void setPgPlanCodeReference(String pgPlanCodeReference) {
        this.pgPlanCodeReference = pgPlanCodeReference;
    }

    public Float getValue() {
        return value;
    }

    public Plan value(Float value) {
        this.value = value;
        return this;
    }

    public void setValue(Float value) {
        this.value = value;
    }

    public String getBillingPeriod() {
        return billingPeriod;
    }

    public Plan billingPeriod(String billingPeriod) {
        this.billingPeriod = billingPeriod;
        return this;
    }

    public void setBillingPeriod(String billingPeriod) {
        this.billingPeriod = billingPeriod;
    }

    public Float getMembershipFee() {
        return membershipFee;
    }

    public Plan membershipFee(Float membershipFee) {
        this.membershipFee = membershipFee;
        return this;
    }

    public void setMembershipFee(Float membershipFee) {
        this.membershipFee = membershipFee;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Plan plan = (Plan) o;
        if (plan.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), plan.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Plan{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", details='" + getDetails() + "'" +
            ", pgPlanHash='" + getPgPlanHash() + "'" +
            ", pgPlanCodeReference='" + getPgPlanCodeReference() + "'" +
            ", value=" + getValue() +
            ", billingPeriod='" + getBillingPeriod() + "'" +
            ", membershipFee=" + getMembershipFee() +
            "}";
    }
}
