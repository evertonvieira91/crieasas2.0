package br.com.crieasas.app.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A FinancialVariables.
 */
@Entity
@Table(name = "financial_variables")
public class FinancialVariables implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "closing_day")
    private ZonedDateTime closingDay;

    @Column(name = "billing_day")
    private ZonedDateTime billingDay;

    @Column(name = "percentage_level_1")
    private Float percentageLevel1;

    @Column(name = "percentage_level_2")
    private Float percentageLevel2;

    @Column(name = "percentage_level_3")
    private Float percentageLevel3;

    @Column(name = "percentage_level_4")
    private Float percentageLevel4;

    @Column(name = "percentage_level_5")
    private Float percentageLevel5;

    @Column(name = "value_admin_fee")
    private Float valueAdminFee;

    @Column(name = "percentage_bonus")
    private Float percentageBonus;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getClosingDay() {
        return closingDay;
    }

    public FinancialVariables closingDay(ZonedDateTime closingDay) {
        this.closingDay = closingDay;
        return this;
    }

    public void setClosingDay(ZonedDateTime closingDay) {
        this.closingDay = closingDay;
    }

    public ZonedDateTime getBillingDay() {
        return billingDay;
    }

    public FinancialVariables billingDay(ZonedDateTime billingDay) {
        this.billingDay = billingDay;
        return this;
    }

    public void setBillingDay(ZonedDateTime billingDay) {
        this.billingDay = billingDay;
    }

    public Float getPercentageLevel1() {
        return percentageLevel1;
    }

    public FinancialVariables percentageLevel1(Float percentageLevel1) {
        this.percentageLevel1 = percentageLevel1;
        return this;
    }

    public void setPercentageLevel1(Float percentageLevel1) {
        this.percentageLevel1 = percentageLevel1;
    }

    public Float getPercentageLevel2() {
        return percentageLevel2;
    }

    public FinancialVariables percentageLevel2(Float percentageLevel2) {
        this.percentageLevel2 = percentageLevel2;
        return this;
    }

    public void setPercentageLevel2(Float percentageLevel2) {
        this.percentageLevel2 = percentageLevel2;
    }

    public Float getPercentageLevel3() {
        return percentageLevel3;
    }

    public FinancialVariables percentageLevel3(Float percentageLevel3) {
        this.percentageLevel3 = percentageLevel3;
        return this;
    }

    public void setPercentageLevel3(Float percentageLevel3) {
        this.percentageLevel3 = percentageLevel3;
    }

    public Float getPercentageLevel4() {
        return percentageLevel4;
    }

    public FinancialVariables percentageLevel4(Float percentageLevel4) {
        this.percentageLevel4 = percentageLevel4;
        return this;
    }

    public void setPercentageLevel4(Float percentageLevel4) {
        this.percentageLevel4 = percentageLevel4;
    }

    public Float getPercentageLevel5() {
        return percentageLevel5;
    }

    public FinancialVariables percentageLevel5(Float percentageLevel5) {
        this.percentageLevel5 = percentageLevel5;
        return this;
    }

    public void setPercentageLevel5(Float percentageLevel5) {
        this.percentageLevel5 = percentageLevel5;
    }

    public Float getValueAdminFee() {
        return valueAdminFee;
    }

    public FinancialVariables valueAdminFee(Float valueAdminFee) {
        this.valueAdminFee = valueAdminFee;
        return this;
    }

    public void setValueAdminFee(Float valueAdminFee) {
        this.valueAdminFee = valueAdminFee;
    }

    public Float getPercentageBonus() {
        return percentageBonus;
    }

    public FinancialVariables percentageBonus(Float percentageBonus) {
        this.percentageBonus = percentageBonus;
        return this;
    }

    public void setPercentageBonus(Float percentageBonus) {
        this.percentageBonus = percentageBonus;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        FinancialVariables financialVariables = (FinancialVariables) o;
        if (financialVariables.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), financialVariables.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "FinancialVariables{" +
            "id=" + getId() +
            ", closingDay='" + getClosingDay() + "'" +
            ", billingDay='" + getBillingDay() + "'" +
            ", percentageLevel1=" + getPercentageLevel1() +
            ", percentageLevel2=" + getPercentageLevel2() +
            ", percentageLevel3=" + getPercentageLevel3() +
            ", percentageLevel4=" + getPercentageLevel4() +
            ", percentageLevel5=" + getPercentageLevel5() +
            ", valueAdminFee=" + getValueAdminFee() +
            ", percentageBonus=" + getPercentageBonus() +
            "}";
    }
}
