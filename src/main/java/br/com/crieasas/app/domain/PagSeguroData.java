package br.com.crieasas.app.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A PagSeguroData.
 */
@Entity
@Table(name = "pag_seguro_data")
public class PagSeguroData implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "seller_email")
    private String sellerEmail;

    @Column(name = "seller_token")
    private String sellerToken;

    @Column(name = "email_sandbox")
    private String emailSandbox;

    @Column(name = "pass_sandbox")
    private String passSandbox;

    @Column(name = "app_id")
    private String appId;

    @Column(name = "app_key")
    private String appKey;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSellerEmail() {
        return sellerEmail;
    }

    public PagSeguroData sellerEmail(String sellerEmail) {
        this.sellerEmail = sellerEmail;
        return this;
    }

    public void setSellerEmail(String sellerEmail) {
        this.sellerEmail = sellerEmail;
    }

    public String getSellerToken() {
        return sellerToken;
    }

    public PagSeguroData sellerToken(String sellerToken) {
        this.sellerToken = sellerToken;
        return this;
    }

    public void setSellerToken(String sellerToken) {
        this.sellerToken = sellerToken;
    }

    public String getEmailSandbox() {
        return emailSandbox;
    }

    public PagSeguroData emailSandbox(String emailSandbox) {
        this.emailSandbox = emailSandbox;
        return this;
    }

    public void setEmailSandbox(String emailSandbox) {
        this.emailSandbox = emailSandbox;
    }

    public String getPassSandbox() {
        return passSandbox;
    }

    public PagSeguroData passSandbox(String passSandbox) {
        this.passSandbox = passSandbox;
        return this;
    }

    public void setPassSandbox(String passSandbox) {
        this.passSandbox = passSandbox;
    }

    public String getAppId() {
        return appId;
    }

    public PagSeguroData appId(String appId) {
        this.appId = appId;
        return this;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAppKey() {
        return appKey;
    }

    public PagSeguroData appKey(String appKey) {
        this.appKey = appKey;
        return this;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PagSeguroData pagSeguroData = (PagSeguroData) o;
        if (pagSeguroData.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), pagSeguroData.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PagSeguroData{" +
            "id=" + getId() +
            ", sellerEmail='" + getSellerEmail() + "'" +
            ", sellerToken='" + getSellerToken() + "'" +
            ", emailSandbox='" + getEmailSandbox() + "'" +
            ", passSandbox='" + getPassSandbox() + "'" +
            ", appId='" + getAppId() + "'" +
            ", appKey='" + getAppKey() + "'" +
            "}";
    }
}
