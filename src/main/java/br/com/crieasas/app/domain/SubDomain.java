package br.com.crieasas.app.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A SubDomain.
 */
@Entity
@Table(name = "sub_domain")
public class SubDomain implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @OneToOne
    @JoinColumn(unique = true)
    private Signature signature;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public SubDomain name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Signature getSignature() {
        return signature;
    }

    public SubDomain signature(Signature signature) {
        this.signature = signature;
        return this;
    }

    public void setSignature(Signature signature) {
        this.signature = signature;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SubDomain subDomain = (SubDomain) o;
        if (subDomain.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), subDomain.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SubDomain{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            "}";
    }
}
