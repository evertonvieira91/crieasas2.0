package br.com.crieasas.app.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A SellersToSellers.
 */
@Entity
@Table(name = "sellers_to_sellers")
public class SellersToSellers implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @JoinColumn(unique = true)
    private User sun;

    @ManyToOne
    @JsonIgnoreProperties("")
    private User father;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getSun() {
        return sun;
    }

    public SellersToSellers sun(User user) {
        this.sun = user;
        return this;
    }

    public void setSun(User user) {
        this.sun = user;
    }

    public User getFather() {
        return father;
    }

    public SellersToSellers father(User user) {
        this.father = user;
        return this;
    }

    public void setFather(User user) {
        this.father = user;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SellersToSellers sellersToSellers = (SellersToSellers) o;
        if (sellersToSellers.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), sellersToSellers.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SellersToSellers{" +
            "id=" + getId() +
            "}";
    }
}
