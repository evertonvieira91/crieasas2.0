package br.com.crieasas.app.domain.enumeration;

/**
 * The StatusSignature enumeration.
 */
public enum StatusSignature {
    ACTIVE, CANCELED, SUSPENDED, BLOCKED
}
