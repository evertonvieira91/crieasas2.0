package br.com.crieasas.app.web.rest;

import com.codahale.metrics.annotation.Timed;
import br.com.crieasas.app.service.PagSeguroDataService;
import br.com.crieasas.app.web.rest.errors.BadRequestAlertException;
import br.com.crieasas.app.web.rest.util.HeaderUtil;
import br.com.crieasas.app.web.rest.util.PaginationUtil;
import br.com.crieasas.app.service.dto.PagSeguroDataDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing PagSeguroData.
 */
@RestController
@RequestMapping("/api")
public class PagSeguroDataResource {

    private final Logger log = LoggerFactory.getLogger(PagSeguroDataResource.class);

    private static final String ENTITY_NAME = "pagSeguroData";

    private final PagSeguroDataService pagSeguroDataService;

    public PagSeguroDataResource(PagSeguroDataService pagSeguroDataService) {
        this.pagSeguroDataService = pagSeguroDataService;
    }

    /**
     * POST  /pag-seguro-data : Create a new pagSeguroData.
     *
     * @param pagSeguroDataDTO the pagSeguroDataDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new pagSeguroDataDTO, or with status 400 (Bad Request) if the pagSeguroData has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/pag-seguro-data")
    @Timed
    public ResponseEntity<PagSeguroDataDTO> createPagSeguroData(@RequestBody PagSeguroDataDTO pagSeguroDataDTO) throws URISyntaxException {
        log.debug("REST request to save PagSeguroData : {}", pagSeguroDataDTO);
        if (pagSeguroDataDTO.getId() != null) {
            throw new BadRequestAlertException("A new pagSeguroData cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PagSeguroDataDTO result = pagSeguroDataService.save(pagSeguroDataDTO);
        return ResponseEntity.created(new URI("/api/pag-seguro-data/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /pag-seguro-data : Updates an existing pagSeguroData.
     *
     * @param pagSeguroDataDTO the pagSeguroDataDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated pagSeguroDataDTO,
     * or with status 400 (Bad Request) if the pagSeguroDataDTO is not valid,
     * or with status 500 (Internal Server Error) if the pagSeguroDataDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/pag-seguro-data")
    @Timed
    public ResponseEntity<PagSeguroDataDTO> updatePagSeguroData(@RequestBody PagSeguroDataDTO pagSeguroDataDTO) throws URISyntaxException {
        log.debug("REST request to update PagSeguroData : {}", pagSeguroDataDTO);
        if (pagSeguroDataDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        PagSeguroDataDTO result = pagSeguroDataService.save(pagSeguroDataDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, pagSeguroDataDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /pag-seguro-data : get all the pagSeguroData.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of pagSeguroData in body
     */
    @GetMapping("/pag-seguro-data")
    @Timed
    public ResponseEntity<List<PagSeguroDataDTO>> getAllPagSeguroData(Pageable pageable) {
        log.debug("REST request to get a page of PagSeguroData");
        Page<PagSeguroDataDTO> page = pagSeguroDataService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/pag-seguro-data");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /pag-seguro-data/:id : get the "id" pagSeguroData.
     *
     * @param id the id of the pagSeguroDataDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the pagSeguroDataDTO, or with status 404 (Not Found)
     */
    @GetMapping("/pag-seguro-data/{id}")
    @Timed
    public ResponseEntity<PagSeguroDataDTO> getPagSeguroData(@PathVariable Long id) {
        log.debug("REST request to get PagSeguroData : {}", id);
        Optional<PagSeguroDataDTO> pagSeguroDataDTO = pagSeguroDataService.findOne(id);
        return ResponseUtil.wrapOrNotFound(pagSeguroDataDTO);
    }

    /**
     * DELETE  /pag-seguro-data/:id : delete the "id" pagSeguroData.
     *
     * @param id the id of the pagSeguroDataDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/pag-seguro-data/{id}")
    @Timed
    public ResponseEntity<Void> deletePagSeguroData(@PathVariable Long id) {
        log.debug("REST request to delete PagSeguroData : {}", id);
        pagSeguroDataService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
