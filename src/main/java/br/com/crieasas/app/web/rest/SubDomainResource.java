package br.com.crieasas.app.web.rest;

import com.codahale.metrics.annotation.Timed;
import br.com.crieasas.app.service.SubDomainService;
import br.com.crieasas.app.web.rest.errors.BadRequestAlertException;
import br.com.crieasas.app.web.rest.util.HeaderUtil;
import br.com.crieasas.app.web.rest.util.PaginationUtil;
import br.com.crieasas.app.service.dto.SubDomainDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing SubDomain.
 */
@RestController
@RequestMapping("/api")
public class SubDomainResource {

    private final Logger log = LoggerFactory.getLogger(SubDomainResource.class);

    private static final String ENTITY_NAME = "subDomain";

    private final SubDomainService subDomainService;

    public SubDomainResource(SubDomainService subDomainService) {
        this.subDomainService = subDomainService;
    }

    /**
     * POST  /sub-domains : Create a new subDomain.
     *
     * @param subDomainDTO the subDomainDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new subDomainDTO, or with status 400 (Bad Request) if the subDomain has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/sub-domains")
    @Timed
    public ResponseEntity<SubDomainDTO> createSubDomain(@RequestBody SubDomainDTO subDomainDTO) throws URISyntaxException {
        log.debug("REST request to save SubDomain : {}", subDomainDTO);
        if (subDomainDTO.getId() != null) {
            throw new BadRequestAlertException("A new subDomain cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SubDomainDTO result = subDomainService.save(subDomainDTO);
        return ResponseEntity.created(new URI("/api/sub-domains/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /sub-domains : Updates an existing subDomain.
     *
     * @param subDomainDTO the subDomainDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated subDomainDTO,
     * or with status 400 (Bad Request) if the subDomainDTO is not valid,
     * or with status 500 (Internal Server Error) if the subDomainDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/sub-domains")
    @Timed
    public ResponseEntity<SubDomainDTO> updateSubDomain(@RequestBody SubDomainDTO subDomainDTO) throws URISyntaxException {
        log.debug("REST request to update SubDomain : {}", subDomainDTO);
        if (subDomainDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        SubDomainDTO result = subDomainService.save(subDomainDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, subDomainDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /sub-domains : get all the subDomains.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of subDomains in body
     */
    @GetMapping("/sub-domains")
    @Timed
    public ResponseEntity<List<SubDomainDTO>> getAllSubDomains(Pageable pageable) {
        log.debug("REST request to get a page of SubDomains");
        Page<SubDomainDTO> page = subDomainService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/sub-domains");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /sub-domains/:id : get the "id" subDomain.
     *
     * @param id the id of the subDomainDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the subDomainDTO, or with status 404 (Not Found)
     */
    @GetMapping("/sub-domains/{id}")
    @Timed
    public ResponseEntity<SubDomainDTO> getSubDomain(@PathVariable Long id) {
        log.debug("REST request to get SubDomain : {}", id);
        Optional<SubDomainDTO> subDomainDTO = subDomainService.findOne(id);
        return ResponseUtil.wrapOrNotFound(subDomainDTO);
    }

    /**
     * DELETE  /sub-domains/:id : delete the "id" subDomain.
     *
     * @param id the id of the subDomainDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/sub-domains/{id}")
    @Timed
    public ResponseEntity<Void> deleteSubDomain(@PathVariable Long id) {
        log.debug("REST request to delete SubDomain : {}", id);
        subDomainService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
