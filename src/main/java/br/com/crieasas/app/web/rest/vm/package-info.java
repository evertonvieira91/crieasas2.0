/**
 * View Models used by Spring MVC REST controllers.
 */
package br.com.crieasas.app.web.rest.vm;
