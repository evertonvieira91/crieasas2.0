package br.com.crieasas.app.web.rest;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Hasha {

    private String hash;

    public Hasha(){}
    @JsonCreator
    public Hasha(@JsonProperty("hash") String hash){
        this.hash=hash;
    }
}
