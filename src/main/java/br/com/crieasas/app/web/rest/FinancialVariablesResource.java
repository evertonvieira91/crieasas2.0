package br.com.crieasas.app.web.rest;

import com.codahale.metrics.annotation.Timed;
import br.com.crieasas.app.service.FinancialVariablesService;
import br.com.crieasas.app.web.rest.errors.BadRequestAlertException;
import br.com.crieasas.app.web.rest.util.HeaderUtil;
import br.com.crieasas.app.web.rest.util.PaginationUtil;
import br.com.crieasas.app.service.dto.FinancialVariablesDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing FinancialVariables.
 */
@RestController
@RequestMapping("/api")
public class FinancialVariablesResource {

    private final Logger log = LoggerFactory.getLogger(FinancialVariablesResource.class);

    private static final String ENTITY_NAME = "financialVariables";

    private final FinancialVariablesService financialVariablesService;

    public FinancialVariablesResource(FinancialVariablesService financialVariablesService) {
        this.financialVariablesService = financialVariablesService;
    }

    /**
     * POST  /financial-variables : Create a new financialVariables.
     *
     * @param financialVariablesDTO the financialVariablesDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new financialVariablesDTO, or with status 400 (Bad Request) if the financialVariables has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/financial-variables")
    @Timed
    public ResponseEntity<FinancialVariablesDTO> createFinancialVariables(@RequestBody FinancialVariablesDTO financialVariablesDTO) throws URISyntaxException {
        log.debug("REST request to save FinancialVariables : {}", financialVariablesDTO);
        if (financialVariablesDTO.getId() != null) {
            throw new BadRequestAlertException("A new financialVariables cannot already have an ID", ENTITY_NAME, "idexists");
        }
        FinancialVariablesDTO result = financialVariablesService.save(financialVariablesDTO);
        return ResponseEntity.created(new URI("/api/financial-variables/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /financial-variables : Updates an existing financialVariables.
     *
     * @param financialVariablesDTO the financialVariablesDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated financialVariablesDTO,
     * or with status 400 (Bad Request) if the financialVariablesDTO is not valid,
     * or with status 500 (Internal Server Error) if the financialVariablesDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/financial-variables")
    @Timed
    public ResponseEntity<FinancialVariablesDTO> updateFinancialVariables(@RequestBody FinancialVariablesDTO financialVariablesDTO) throws URISyntaxException {
        log.debug("REST request to update FinancialVariables : {}", financialVariablesDTO);
        if (financialVariablesDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        FinancialVariablesDTO result = financialVariablesService.save(financialVariablesDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, financialVariablesDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /financial-variables : get all the financialVariables.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of financialVariables in body
     */
    @GetMapping("/financial-variables")
    @Timed
    public ResponseEntity<List<FinancialVariablesDTO>> getAllFinancialVariables(Pageable pageable) {
        log.debug("REST request to get a page of FinancialVariables");
        Page<FinancialVariablesDTO> page = financialVariablesService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/financial-variables");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /financial-variables/:id : get the "id" financialVariables.
     *
     * @param id the id of the financialVariablesDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the financialVariablesDTO, or with status 404 (Not Found)
     */
    @GetMapping("/financial-variables/{id}")
    @Timed
    public ResponseEntity<FinancialVariablesDTO> getFinancialVariables(@PathVariable Long id) {
        log.debug("REST request to get FinancialVariables : {}", id);
        Optional<FinancialVariablesDTO> financialVariablesDTO = financialVariablesService.findOne(id);
        return ResponseUtil.wrapOrNotFound(financialVariablesDTO);
    }

    /**
     * DELETE  /financial-variables/:id : delete the "id" financialVariables.
     *
     * @param id the id of the financialVariablesDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/financial-variables/{id}")
    @Timed
    public ResponseEntity<Void> deleteFinancialVariables(@PathVariable Long id) {
        log.debug("REST request to delete FinancialVariables : {}", id);
        financialVariablesService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
