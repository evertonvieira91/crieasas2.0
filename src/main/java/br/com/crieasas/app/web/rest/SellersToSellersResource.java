package br.com.crieasas.app.web.rest;

import com.codahale.metrics.annotation.Timed;
import br.com.crieasas.app.service.SellersToSellersService;
import br.com.crieasas.app.web.rest.errors.BadRequestAlertException;
import br.com.crieasas.app.web.rest.util.HeaderUtil;
import br.com.crieasas.app.web.rest.util.PaginationUtil;
import br.com.crieasas.app.service.dto.SellersToSellersDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing SellersToSellers.
 */
@RestController
@RequestMapping("/api")
public class SellersToSellersResource {

    private final Logger log = LoggerFactory.getLogger(SellersToSellersResource.class);

    private static final String ENTITY_NAME = "sellersToSellers";

    private final SellersToSellersService sellersToSellersService;

    public SellersToSellersResource(SellersToSellersService sellersToSellersService) {
        this.sellersToSellersService = sellersToSellersService;
    }

    /**
     * POST  /sellers-to-sellers : Create a new sellersToSellers.
     *
     * @param sellersToSellersDTO the sellersToSellersDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new sellersToSellersDTO, or with status 400 (Bad Request) if the sellersToSellers has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/sellers-to-sellers")
    @Timed
    public ResponseEntity<SellersToSellersDTO> createSellersToSellers(@RequestBody SellersToSellersDTO sellersToSellersDTO) throws URISyntaxException {
        log.debug("REST request to save SellersToSellers : {}", sellersToSellersDTO);
        if (sellersToSellersDTO.getId() != null) {
            throw new BadRequestAlertException("A new sellersToSellers cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SellersToSellersDTO result = sellersToSellersService.save(sellersToSellersDTO);
        return ResponseEntity.created(new URI("/api/sellers-to-sellers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /sellers-to-sellers : Updates an existing sellersToSellers.
     *
     * @param sellersToSellersDTO the sellersToSellersDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated sellersToSellersDTO,
     * or with status 400 (Bad Request) if the sellersToSellersDTO is not valid,
     * or with status 500 (Internal Server Error) if the sellersToSellersDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/sellers-to-sellers")
    @Timed
    public ResponseEntity<SellersToSellersDTO> updateSellersToSellers(@RequestBody SellersToSellersDTO sellersToSellersDTO) throws URISyntaxException {
        log.debug("REST request to update SellersToSellers : {}", sellersToSellersDTO);
        if (sellersToSellersDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        SellersToSellersDTO result = sellersToSellersService.save(sellersToSellersDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, sellersToSellersDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /sellers-to-sellers : get all the sellersToSellers.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of sellersToSellers in body
     */
    @GetMapping("/sellers-to-sellers")
    @Timed
    public ResponseEntity<List<SellersToSellersDTO>> getAllSellersToSellers(Pageable pageable) {
        log.debug("REST request to get a page of SellersToSellers");
        Page<SellersToSellersDTO> page = sellersToSellersService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/sellers-to-sellers");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /sellers-to-sellers/:id : get the "id" sellersToSellers.
     *
     * @param id the id of the sellersToSellersDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the sellersToSellersDTO, or with status 404 (Not Found)
     */
    @GetMapping("/sellers-to-sellers/{id}")
    @Timed
    public ResponseEntity<SellersToSellersDTO> getSellersToSellers(@PathVariable Long id) {
        log.debug("REST request to get SellersToSellers : {}", id);
        Optional<SellersToSellersDTO> sellersToSellersDTO = sellersToSellersService.findOne(id);
        return ResponseUtil.wrapOrNotFound(sellersToSellersDTO);
    }

    /**
     * DELETE  /sellers-to-sellers/:id : delete the "id" sellersToSellers.
     *
     * @param id the id of the sellersToSellersDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/sellers-to-sellers/{id}")
    @Timed
    public ResponseEntity<Void> deleteSellersToSellers(@PathVariable Long id) {
        log.debug("REST request to delete SellersToSellers : {}", id);
        sellersToSellersService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
