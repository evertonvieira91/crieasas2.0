package br.com.crieasas.app.web.rest;

import br.com.crieasas.app.service.PagSeguroService;
import br.com.crieasas.app.service.PagSeguroTransaction;
import br.com.uol.pagseguro.api.common.domain.DataList;
import br.com.uol.pagseguro.api.direct.preapproval.PaymentOrder;
import br.com.uol.pagseguro.api.direct.preapproval.DirectPreApprovalData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Value;

import javax.validation.Valid;

/**
 * PagSeguroResource controller
 */
@RestController
@RequestMapping("/api/pag-seguro")
public class PagSeguroResource {

    private final Logger log = LoggerFactory.getLogger(PagSeguroResource.class);
    private PagSeguroService pagSeguroService;
    private PagSeguroTransaction pagSeguroTransaction;
    /**
    * GET pagseguro
    */

    public PagSeguroResource(PagSeguroService pagSeguroService,PagSeguroTransaction pagSeguroTransaction){
        this.pagSeguroService = pagSeguroService;
        this.pagSeguroTransaction = pagSeguroTransaction;
    }


    @GetMapping("/pag-seguro")
    public String pagseguro(int id) {
        return "pagseguro"+id;
    }

    @GetMapping("/createPlan")
    public void createPlan() throws Throwable { pagSeguroService.createAutomaticPlan(); }

    @GetMapping("/planRegister")
    public void preAppovalRegister() throws Throwable { pagSeguroService.accedeDirectPreApprovalPlan(); }

    @PutMapping("/createSession")
    public String createSession() throws Throwable { return pagSeguroService.criaSessaoPagamento(); }

    @GetMapping("/searchPlanByAccessionCode")
    public String searchPlanByAccessionCode() throws Throwable { return pagSeguroService.searchPlanByAccessionCode(); }

    @GetMapping("/searchPlanByDateInterval")
    public DataList<? extends DirectPreApprovalData> searchPlanByDateInterval() throws Throwable { return pagSeguroService.searchPlanByDateInterval(); }

    @GetMapping("/searchPlanByDayInterval")
    public void searchPlanByDayInterval() throws Throwable { pagSeguroService.searchPlanByDayInterval(); }

    @GetMapping("/SearchPlanByNotificationCode")
    public void SearchPlanByNotificationCode() throws Throwable { pagSeguroService.SearchPlanByNotificationCode(); }

    @PutMapping("/discountPayment")
    public void discountPayment() throws Throwable { pagSeguroService.discountPayment(); }

    @GetMapping("/listDirectPaymentOrders")
    public DataList<? extends PaymentOrder> listDirectPaymentOrders() throws Throwable { return pagSeguroService.listDirectPaymentOrders(); }

    @GetMapping("/changePaymentMethod")
    public boolean changePaymentMethod(){return pagSeguroService.changePaymentMethodPreApproval(); }

    @GetMapping("/changeStatusPlan")
    public boolean changeStatusPreApproval(){return pagSeguroService.changeStatusPreApproval(); }

    @GetMapping("/editValuePlan")
    public boolean editValuePreApproval(){return pagSeguroService.editValuePreApproval();}



    // Transaction

    @GetMapping("/requestRefundPartial")
    public boolean requestRefundPartial(){return pagSeguroTransaction.requestRefundPartial();}

    @GetMapping("/requestRefundTotal")
    public boolean requestRefundTotal(){return pagSeguroTransaction.requestRefundTotal();}

    @GetMapping("/searchTransactionByCode")
    public void searchTransactionByCode() throws Throwable {pagSeguroTransaction.searchTransactionByCode();}

    //Notification
    @RequestMapping(value = "/notifications",method = RequestMethod.GET)
    public String notificationReceiveGet(){
        return "teste";
    }


    @RequestMapping(value = "/notifications",method = RequestMethod.POST)
    public String notificationReceivePost(){
        return "teste post";
    }


}
